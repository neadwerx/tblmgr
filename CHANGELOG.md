Changelog
=========

# 1.0

 - Reorganized code
 - Improved documentation
 - Updater process is now crash safe
 - Moved common code to service/lib/
 - Refresher process will now handle refreshing of tables with dependent objects
 - Added running as non-superuser a non-fatal event (we only need this for PID file)

# 0.9

 - Added flat SQL file for installation targets where an extension is not appropriate (AWS RDS)
 - Relaxed trigger validation on pg_restore - validation function handles specific SQL states which may otherwise prevent extension tables from being populated

# 0.8.1

 - Fixed issue where set-comparison table created by updater process caused replication lag on high throughput systems.

# 0.8

 - Added dependency for hstore
 - Updater process now retries queries that have been canceled or terminated.
 - Added debug views
 - Modified comparison logic to avoid interference with long-running pg_dumps

# 0.7.5

Incremental update improving interlock between updater and refresher process.
 - Refresher process now reliably handles database crashes and conflicts with other refreshes

# 0.7

 - Partial refactor of trigger management
 - Introduced asynchronous refresher process with segmented, non-overlapping creation to avoid interference with existing tables

# 0.6

 - Minor improvements to index and refresh management functions
 - Improved indexing of queue table
 - Improved speed of queue peeking prior to udpate

# 0.5.5

 - Support for SMP added (experimental)
 - Prototype per-table processes implemented using both POSIX form and Coro. (this effort has been sidelined)

# 0.5

Complete refactor
 - Prototype semaphore to prevent flushing of update queue while a full refresh is in progress.
 - Standardized table and function names to avoid confusion
 - Improved queue management function
 - Improved speed of queue peek

# 0.4

 - Improved refresher process and trigger managment

# 0.3

 - Improved index management
 - Added manual refresh function for cache tables.
 - Added function to validate trigger states

# 0.2

 - Added object dependencies
 - Refined queue population from trigger masquerading sources

# 0.1

Initial Release
