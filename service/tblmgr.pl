#!/usr/bin/perl

use utf8;
use strict;
use warnings;

use Params::Validate qw( :all );
use Carp;
use Readonly;
use Perl6::Export::Attrs;
use English qw( -no_match_vars );

use DBI;
use JSON;
use IO::Select;
use IO::Handle;
use Getopt::Std;
use Time::HiRes qw( gettimeofday tv_interval );
use POSIX qw( strftime setsid :sys_wait_h );
use Cwd qw( abs_path );
use FindBin;

use lib "$FindBin::Bin/lib";
use lib "$FindBin::Bin/service/lib/";
use tblmgr qw( :updater );

Readonly::Scalar my $LISTEN_CHANNEL       => 'cache_update_request';
Readonly::Scalar my $MAX_QUERY_RETRY      => 3;
Readonly::Scalar my $SQL_STATE_ADMIN_TERM => '57P01';
Readonly::Scalar my $SQL_STATE_CANCELED   => '57014';
Readonly::Scalar my $QUERY_RETRY_ENABLED  => 1;

$OUTPUT_AUTOFLUSH = 1;
my $pid_file;

sub remove_pid_and_quit()
{
    if( defined $pid_file and -e $pid_file )
    {
        unlink( $pid_file );
    }

    exit 1;
}

sub check_extension($)
{
    my( $handle ) = @_;
    my $query  = <<END_SQL;
    SELECT n.oid
      FROM pg_namespace n
     WHERE n.nspname = 'tblmgr'
END_SQL

    my $sth = $handle->prepare( $query );

    unless( $sth->execute() )
    {
        update( "Failed to run extension check query" );
        remove_pid_and_quit();
    }

    if( $sth->rows() > 0 )
    {
        $sth->finish();
        return;
    }

    $sth->finish();
    update( "Tblmgr does not seem to be installed on this system" );
    remove_pid_and_quit();
}

sub _check_tblmgr_running($)
{
    # Check that only one instance of tblmgr is running on a given database. This prevents issues.
    my( $handle ) = @_;

    my $check_query = <<'END_SQL';
    SELECT pg_try_advisory_lock(
               c.oid::BIGINT
           ) AS lock_acquired
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = 'tblmgr'
     WHERE c.relname::VARCHAR = 'tb_cache_update_request'
       AND c.relkind = 'r'
END_SQL

    my $sth = $handle->prepare( $check_query );
    $sth->execute();

    my $row = $sth->fetchrow_hashref();
    my $result = $row->{lock_acquired};

    if(
            defined $result
        and (
                $result eq 't'
             or $result eq 'T'
             or (
                     $result =~ m/^\d+$/
                 and $result == 1
                )
            )
      )
    {
        return 0;
    }

    return 1;
}

sub was_db_dropped()
{
    my $pghandle;
    my $dbname = $conn_map->{dbname};

    my $pgconn = $conn_map->{conn_string};
    $pgconn =~ s/dbname=${dbname}/dbname=postgres/;
    $pghandle = DBI->connect( $pgconn, 'postgres', undef );

    unless( $pghandle )
    {
        update( "TBLMGR: Failed to check $dbname existence, resuming backoff" );
        return;
    }

    # Used to exit the process in cases where the DB was dropped after we've started
    my $query = <<END_SQL;
    SELECT datname
      FROM pg_database
     WHERE datname = ?
END_SQL

    my $sth = $pghandle->prepare( $query );
    $sth->bind_param( 1, $conn_map->{dbname} );

    unless( $sth->execute() )
    {
        update( 'Failed to verify DB existence' );
        return;
    }

    if( $sth->rows() > 0 )
    {
        $sth->finish();
        return;
    }

    $sth->finish();
    $pghandle->disconnect();
    update( 'DB seems to have been dropped. Exiting.' );
    remove_pid_and_quit();
}

sub try_query($)
{
    my ( $query ) = @_;
    my $sth;
    my $retry_counter     = 0;
    my $last_backoff_time = 0;
    my $last_sql_state    = '';

    # Try query simplifies query execution and restores database handle if it has been
    #  disconnected.

    RETRY:
    until (
        ( ( $sth = $handle->prepare( $query ) ) and $sth->execute() )
            or ($QUERY_RETRY_ENABLED
            and $retry_counter >= $MAX_QUERY_RETRY )
        )
    {
        my $query_state = $handle->state;

        if(    ( $query_state eq $SQL_STATE_ADMIN_TERM )
            or ( $query_state eq $SQL_STATE_CANCELED ) )
        {
            # Catch when a query is terminated or canceled by administrator.
            # If this is the case, we will defer updates by a random amount of time
            # and retry.
            $retry_counter++;

            if( $query_state eq $SQL_STATE_CANCELED )
            {
                carp( 'TBLMGR: Query canceled by administrator. Retrying...' );
            }
            elsif( $query_state eq $SQL_STATE_ADMIN_TERM )
            {
                carp( 'TBLMGR: Query terminated by administrator. Retrying...' );
            }

            my $random_backoff = rand();

            $last_backoff_time = $last_backoff_time + $random_backoff;

            sleep( $last_backoff_time );

            $last_sql_state = $query_state;

            # We've detected an SQL state indicating that the administrator
            # has terminated or canceled our query. We need to re-run the query
            # in order to maintain a consistent state for the cache_tables that
            # are being updated
            goto RETRY;
        }

        if( $handle->ping() > 0 )
        {
            return $sth;
        }

        # Attempt to avoid memory leak by deleteing globally-scoped objects
        undef $sel;
        undef $handle;
        undef $file_descriptor;
        my $sleep_backoff = 1;
        my $try = 0;
        until ( $handle and $handle->ping() > 0 )
        {
            $try++;
            if( $_verbose or $DEBUG )
            {
                update( 'DB connection is down!' );
            }

            sleep $sleep_backoff;
            $handle = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef );
            $sleep_backoff += int( rand( 2**$try - 1 ) );

            if( $try > 5 )
            {
                was_db_dropped();
            }
        }

        if( $_verbose or $DEBUG )
        {
            update( 'Reconnected to database' );
        }

        if( _check_tblmgr_running( $handle ) )
        {
            my $message = 'Tblmgr is already running and active on this database';
            update( $message ) unless( $no_daemonize );
            warn( "$message\n" ) if( $no_daemonize );
            exit 127;
        }

        $handle->do( "SET application_name = 'tblmgr updater process'" );
        $handle->do( "LISTEN $LISTEN_CHANNEL" );

        $file_descriptor = $handle->func( 'getfd' );

        $sel = IO::Select->new( $file_descriptor );
    }

    if(     $retry_counter >= $MAX_QUERY_RETRY
        and $last_sql_state ne '' )
    {
        carp( "TBLMGR: Query failed after $MAX_QUERY_RETRY tries." );
    }

    return $sth;
}

sub sighup_handler()
{
    # Load configuration changes by reading in modified GUC variables
    my $query = <<SQL;
    SELECT $_schema.fn_get_config( 'enable_retry' )::INTEGER AS enable_retry
SQL
    my $sth = try_query( $query );

    if( $sth )
    {
        my $row = $sth->fetchrow_hashref();
        my $val = $row->{enable_retry};

        if( defined $val
            and $val != $QUERY_RETRY_ENABLED )
        {
            update( "TBLMGR: Loaded new setting for enable_retry: '$val'" );
            $QUERY_RETRY_ENABLED = $val;
        }
    }

    return;
}

sub get_pk_column_name($)
{
    my ( $table_name ) = @_;

    my $pk_col;

    my $q = <<SQL;
    SELECT pga.attname AS pk_col
      FROM pg_index pgi
INNER JOIN pg_class pgc
        ON pgc.oid = pgi.indrelid
       AND pgc.oid = '${table_name}'::REGCLASS
INNER JOIN pg_attribute pga
        ON pga.attrelid = pgc.oid
       AND pga.attnum = ANY( pgi.indkey )
     WHERE pgi.indisprimary
SQL
    my $sth = try_query( $q );
    my $row = $sth->fetchrow_hashref();

    $pk_col = $row->{'pk_col'};

    return $pk_col;
}

sub get_table_columns($;$)
{
    my ( $table_name, $exclude_columns ) = @_;

    my $exclude = '';

    if(     defined $exclude_columns
        and ref( $exclude_columns ) eq 'ARRAY'
        and scalar( @$exclude_columns ) > 0 )
    {
        $exclude = "AND pga.attname NOT IN( '" . join( "','", @$exclude_columns ) . "' )";
    }

    my @return_columns;

    my $query = <<SQL;
    SELECT pga.attname
      FROM pg_attribute pga
INNER JOIN pg_class pgc
        ON pgc.oid = pga.attrelid
       AND pgc.relname = '$table_name'
     WHERE pga.attnum > 0
           $exclude
  ORDER BY pga.attnum
SQL

    if( $DEBUG )
    {
        update( $query );
    }

    my $sth = try_query( $query );

    while( my $row = $sth->fetchrow_hashref() )
    {
        push( @return_columns, $row->{attname} );
    }

    return @return_columns;
}

sub refresher_is_active()
{
    # Check for advisory locks held by refreshes (tblmgr_refresher)
    my $lock_check_q = <<SQL;
    SELECT pid,
           objid
      FROM pg_locks
     WHERE locktype = 'advisory'
       AND mode = 'ExclusiveLock'
       AND classid = '$_schema.tb_cache_table'::REGCLASS::OID
SQL

    my $sth = try_query( $lock_check_q );

    if( $sth->rows() > 0 )
    {
        return 1;
    }

    return 0;
}

sub _validate_temp_table_definition($$$)
{
    my ( $temp_table_name, $generation_query, $group_by ) = @_;

    # Checks that the comparison temp table exists, and that the definition is correct and up-to-date
    my $set_comparison_table_check_q = <<SQL;
    SELECT c.relname::VARCHAR
      FROM pg_class c
     WHERE c.relkind = 'r'
       AND c.relname::VARCHAR = '$temp_table_name'
       AND c.relpersistence = 'u'
SQL

    my $set_comparison_table_check_sth = try_query( $set_comparison_table_check_q );

    if( $set_comparison_table_check_sth->rows() == 0 )
    {
        return; # this will escape to the drop/create secion of caller
    }

    my $catalog_check_q = <<SQL;
    SELECT a.attname::VARCHAR AS column_name,
           t.typname::VARCHAR AS type
      FROM pg_class c
INNER JOIN pg_attribute a
        ON a.attrelid = c.oid
       AND a.attnum > 0
INNER JOIN pg_type t
        ON t.oid = a.atttypid
     WHERE c.relname::VARCHAR = '$temp_table_name'
SQL
    my $catalog_check_sth = try_query( $catalog_check_q );

    unless( $catalog_check_sth )
    {
        return;
    }

    $generation_query .= ' AND FALSE' . $group_by;

    my $query_sth = try_query( $generation_query );

    unless( $query_sth )
    {
        return;
    }

    my @query_keys  = @{ $query_sth->{NAME} };
    my @query_types = @{ $query_sth->{pg_type} };

    if( scalar( @query_keys ) != $catalog_check_sth->rows() )
    {
        return;
    }

    my $catalog_keys        = $catalog_check_sth->fetchall_hashref( 'column_name' );
    my $validated_key_count = 0;

    #TODO: Probably a cleaner way to do this
    foreach my $query_key ( @query_keys )
    {
        my $type = shift @query_types;

        unless( defined( $catalog_keys->{$query_key} ) and $catalog_keys->{$query_key}->{type} eq $type )
        {
            if( $type eq 'unknown' )
            {    # Workaround for strange types - hstore shows as unknown in driver output
                $validated_key_count++;
                next;
            }

            return;
        }

        $validated_key_count++;
    }

    if( $validated_key_count < scalar( keys %$catalog_keys ) )
    {
        return;
    }

    return 1;
}

sub process_ct_updates($$)
{
    my ( $cache_table_info, $table_keys ) = @_;

    my $update_start      = [gettimeofday()];
    my $pk_cache_table    = $cache_table_info->{'pk_cache_table'};
    my $unique_expression = $cache_table_info->{'unique_expression'};
    my $generation_query  = $cache_table_info->{'generation_query'};
    my $cache_table       = $cache_table_info->{'cache_table'};
    my $schema            = $cache_table_info->{'schema'};
    my $group_by          = $cache_table_info->{'group_by_expression'};
    my $table_columns     = $cache_table_info->{'table_columns'};
    $table_columns = decode_json( $table_columns );

    if( $_verbose )
    {
        update( "Processing updates for $cache_table" );
    }

    if( $_timing )
    {
        update( "Cache table $cache_table service timings:" );
    }

    my @generation_where_entries;
    my @ct_where_entries;

    my $additional_key_count = 0;
    my $primary_key_count    = 0;
    my $table_count          = 0;

    while( my ( $table_name, $pk_hash ) = each %$table_keys )
    {
        my $pk_column = $pk_hash->{pk_col};
        my $pk_values = $pk_hash->{pk_vals};
        my $key_infos = $pk_hash->{keys};
        $table_count++;

        foreach my $schema_name ( keys %$table_columns )
        {
            my $tables = $table_columns->{$schema_name};
            while( my ( $table_alias, $colname_mapping ) = each %{ $tables->{$table_name} } )
            {
                my $pk_col_alias = $colname_mapping->{$pk_column};
                my $where_entry  = "${table_alias}.${pk_column} IN( " . join( ',', @$pk_values ) . ' ) ';

                push( @generation_where_entries, $where_entry );

                if( $pk_col_alias )
                {
                    push( @ct_where_entries, "vw.$pk_col_alias IN( " . join( ',', @$pk_values ) . ' ) ' );
                    $primary_key_count += scalar( @$pk_values );
                }
                else
                {
                    # Use key_infos as a backup. This is generated as a fallback
                    #  by the fn_queue_cache_update_request functions
                    #  but is primarily used for updating masqueraded columns
                    foreach my $key_info ( @$key_infos )
                    {
                        my @ct_where_sub_entries;

                        while( my ( $unique_column, $value ) = each %$key_info )
                        {
                            push( @ct_where_sub_entries, "vw.${unique_column}::TEXT = ${value}::TEXT" );
                        }

                        push( @ct_where_entries, join( ' AND ', @ct_where_sub_entries ) );
                        $additional_key_count++;
                    }
                }
            }
        }
    }

    my @columns = get_table_columns( $cache_table, $unique_expression );

    my $json_processed      = [gettimeofday()];
    my $json_parse_overhead = tv_interval( $update_start, $json_processed );

    if( $_timing )
    {
        update( " - JSON Parse: $json_parse_overhead" );
    }

    my $join              = join( ' AND ', map { "tt.$_ = vw.$_" } @$unique_expression );
    my $table_unique      = join( ', ',    map { "vw.$_" } @$unique_expression );
    my $left_join_where_t = join( ' AND ', map { "tt.$_ IS NULL" } @$unique_expression );
    my $left_join_where_v = join( ' AND ', map { "vw.$_ IS NULL" } @$unique_expression );
    my $update            = join( ', ',    map { "$_ = tt.$_" } @columns );
    my $where = join( ' ) OR ( ', @ct_where_entries );

    # Handle the case where:
    #  - we update raw tuple values
    #  - Update of tuple causes exclusion from set
    #      ( filter by unqiue expression and key updated )
    #  - update causes tuple to be included in set

    $generation_query .= ' AND ( ( ' . join( ' ) or ( ', @generation_where_entries ) . ' ) ) ' . $group_by;

    # Update as per XERP-9762
    #  The set comparison table is persistent and merely gets truncated on use
    #
    #  MODIFICATION: Running a large Select within the DDL prototype is not good. While the UNLOGGED table data is not WAL'd
    #  to standbys, the DDL is. If the DDL takes a long time to replicate because of the generation_query, we can hold up
    #  replication despite our best intentions.
    my $comparison_table_name = "tt_${cache_table}_compare";

    # Check for existance of the compari
    unless(
        _validate_temp_table_definition(
            $comparison_table_name,
            $cache_table_info->{generation_query},
            $cache_table_info->{group_by_expression}
        )
        )
    {
        try_query( "DROP TABLE IF EXISTS ${comparison_table_name}" );
        my $create_q = <<SQL;
            CREATE UNLOGGED TABLE ${comparison_table_name} AS
            (
                $cache_table_info->{generation_query}
                  AND FALSE
                $cache_table_info->{group_by_expression}
            )
SQL
        try_query( $create_q );

        my $unique_columns = join( ',', @$unique_expression );
        my $index_query    = <<SQL;
        CREATE INDEX ix_${comparison_table_name}
                  ON ${comparison_table_name}(
                        $unique_columns
                                             )
SQL
        try_query( $index_query );
    }

    my $populate_query = <<SQL;
    INSERT INTO ${comparison_table_name}
                $generation_query
SQL

    try_query( $populate_query );

    my $temp_table_finished = [gettimeofday()];
    my $temp_table_overhead = tv_interval( $json_processed, $temp_table_finished );

    if( $_timing )
    {
        update( " - Temp table: $temp_table_overhead" );
    }

    if( $_verbose )
    {
        update( "  UPDATE: Temp table created for $cache_table" );
    }

    my $update_query = <<SQL;
        UPDATE ${schema}.${cache_table} vw
           SET $update
          FROM ${comparison_table_name} tt
         WHERE $join
SQL

    if( $DEBUG )
    {
        update( $update_query );
    }

    try_query( $update_query );

    my $update_finished = [gettimeofday()];
    my $update_duration = tv_interval( $temp_table_finished, $update_finished );

    if( $_timing )
    {
        update( " - UPDATE:     $update_duration" );
    }

    if( $_verbose )
    {
        update( "  UPDATE: Existing rows updated for $cache_table" );
    }

    my $remove_query = <<SQL;
    WITH tt_rows_removed AS
    (
        SELECT $table_unique
          FROM ${schema}.${cache_table} vw
     LEFT JOIN ${comparison_table_name} tt
            ON $join
         WHERE ( $where )
           AND $left_join_where_t
    )
        DELETE FROM ${schema}.${cache_table} vw
         USING tt_rows_removed tt
         WHERE $join
SQL

    if( $DEBUG )
    {
        update( $remove_query );
    }

    try_query( $remove_query );

    my $delete_finished = [gettimeofday()];
    my $delete_duration = tv_interval( $update_finished, $delete_finished );

    if( $_timing )
    {
        update( " - DELETE:     $delete_duration" );
    }

    if( $_verbose )
    {
        update( "  UPDATE: Old rows removed for $cache_table" );
    }

    my $insert_query = <<SQL;
    WITH tt_rows_insert AS
    (
        SELECT tt.*
          FROM ${comparison_table_name} tt
     LEFT JOIN ${schema}.${cache_table} vw
            ON $join
         WHERE $left_join_where_v
    )
    INSERT INTO ${schema}.${cache_table}
         SELECT *
           FROM tt_rows_insert
SQL

    if( $DEBUG )
    {
        update( $insert_query );
    }

    try_query( $insert_query );

    my $insert_finished = [gettimeofday()];
    my $insert_duration = tv_interval( $delete_finished, $insert_finished );

    if( $_timing )
    {
        update( " - INSERT:     $insert_duration" );
    }

    if( $_verbose )
    {
        update( "  UPDATE: New rows added for $cache_table" );
    }

    try_query( "DELETE FROM ${comparison_table_name}" );

    # Create detailed stats entry
    my $JSON = <<JSON;
    {
        "json_parse":$json_parse_overhead,
        "temp_table":$temp_table_overhead,
        "update":$update_duration,
        "delete":$delete_duration,
        "insert":$insert_duration,
        "additional_key_count":$additional_key_count,
        "primary_key_count":$primary_key_count,
        "table_count":$table_count
    }
JSON

    my $stats_q = <<SQL;
    INSERT INTO $_schema.tb_cache_table_statistic
                (
                    cache_table,
                    additional_stats
                )
         VALUES
                (
                    $pk_cache_table,
                    '${JSON}'::JSON
                );
SQL

    try_query( $stats_q );
    return;
}

sub flush_cache_requests_by_table_keys($)
{
    my ( $table_keys ) = @_;
    my $table_flush_start = [gettimeofday()];

    my $flush_entry_q = <<SQL;
    DELETE FROM $_schema.tb_cache_update_request
SQL

    if( refresher_is_active() )
    {
        # If an advisory lock has occurred, we want to refresh our
        # (possibly soon to be replaced) cache table, but also
        # leave these requests for processing once the refresh completes.
        # Due to xid min/max we would miss these updates otherwise
        $flush_entry_q .= <<SQL;
         WHERE disable_flush IS FALSE
           AND created < (
                            SELECT MIN( created )
                              FROM $_schema.tb_cache_update_request
                             WHERE disable_flush IS TRUE
                         )
           AND
SQL

        if( $_verbose )
        {
            update( 'fn_refresh_cache_table is running, some cache_update_requests will not be flushed' );
        }
    }
    else
    {
        $flush_entry_q .= <<SQL;
         WHERE
SQL
    }

    $flush_entry_q .= ' full_refresh_request IS FALSE AND ';
    my @table_entries;
    my $key_count = 0;

    foreach my $table_name ( keys %$table_keys )
    {
        my $primary_keys      = $table_keys->{$table_name}->{pk_vals};
        my $created           = $table_keys->{$table_name}->{created};
        my $table_flush_entry = " ( table_name = '$table_name' ";

        if( scalar( @$primary_keys ) > 0 )
        {
            $table_flush_entry .= 'AND primary_key IN( ' . join( ',', @$primary_keys ) . ' )';
            $key_count += scalar( @$primary_keys );
        }

        $table_flush_entry .= " AND created <= '$created'::TIMESTAMP ";
        $table_flush_entry .= ' )';

        push( @table_entries, $table_flush_entry );
    }

    $flush_entry_q .= join( ' OR ', @table_entries );

    if( $DEBUG )
    {
        update( $flush_entry_q );
    }

    try_query( $flush_entry_q );

    my $table_flush_duration = tv_interval( $table_flush_start, [gettimeofday()] );

    if( $_timing or $_verbose )
    {
        update( "$key_count keys flushed in $table_flush_duration seconds" );
    }

    return;
}

sub process_update_requests()
{
    # Get a list of cache tables and all the updates that correspond to them.
    #  The first two CTEs union the arrays of key_info's keys and fn_get_table_pk_col( table_name )
    #  from tb_cache_update_request
    #
    #  The last query joins that with the trigger_columns on column and table to determine what
    #  cache_tables are impacted by this update.
    #
    #  the created field is maxed and carried through so that we can accurately clear requests
    #  that have been serviced. This query should execute in ~700ms
    my $queue_peek_start = [gettimeofday()];
    my $requests_q       = <<SQL;
    WITH tt_latest_requests AS
    (
           SELECT DISTINCT ON( tt.table_name, tt.primary_key )
                  tt.table_name,
                  tt.primary_key,
                  ( array_agg( tt.key_info ORDER BY tt.created DESC ) )[1] AS key_info,
                  MAX( tt.created ) AS created,
                  MIN( tt.created ) AS oldest_created,
                  array_agg( DISTINCT jok.key )::VARCHAR[] AS keys,
                  COUNT( DISTINCT cache_update_request ) AS keynum
             FROM $_schema.tb_cache_update_request tt
LEFT JOIN LATERAL (
                      SELECT key,
                             tt.table_name,
                             tt.primary_key
                        FROM json_object_keys( tt.key_info ) key
                  ) jok
               ON jok.table_name = tt.table_name
              AND jok.primary_key = tt.primary_key
            WHERE tt.full_refresh_request IS FALSE
         GROUP BY tt.table_name,
                  tt.primary_key
    ),
    tt_distinct_column_names AS
    (
           SELECT tt.table_name,
                  tt.primary_key,
                  tt.key_info,
                  unnest( CASE WHEN ARRAY[ a.attname::VARCHAR ]::VARCHAR[] <@ tt.keys
                       THEN tt.keys
                       ELSE array_append( tt.keys, a.attname::VARCHAR )
                        END
                  ) AS column_name,
                  tt.keynum,
                  tt.created,
                  tt.oldest_created
             FROM tt_latest_requests tt
       INNER JOIN pg_class c
               ON c.relname::VARCHAR = tt.table_name
              AND c.relkind = 'r'
       INNER JOIN pg_attribute a
               ON a.attrelid = c.oid
              AND a.attnum > 0
       INNER JOIN pg_constraint cn
               ON cn.contype = 'p'
              AND cn.conrelid = c.oid
              AND cn.conkey[1] = a.attnum
     ),
     tt_cache_table_updates AS
     (
         SELECT ct.table_name AS cache_table,
                ct.cache_table AS pk_cache_table,
                ct.unique_expression,
                ct.generation_query,
                ct.table_columns,
                ct.schema,
                COALESCE( ' GROUP BY ' || array_to_string( ct.group_by_expression, ', ' ), '' ) AS group_by_expression,
                tt.table_name,
                SUM( tt.keynum ) AS keynum,
                array_agg( tt.primary_key ORDER BY tt.primary_key ) AS primary_keys,
                array_agg( tt.key_info    ORDER BY tt.primary_key ) AS key_infos,
                MAX( tt.created ) AS created,
                MIN( tt.oldest_created ) AS oldest_created
           FROM $_schema.tb_cache_table ct
     INNER JOIN $_schema.tb_cache_table_trigger_column cttc
             ON cttc.cache_table = ct.cache_table
     INNER JOIN $_schema.tb_trigger_column tc
             ON tc.trigger_column = cttc.trigger_column
     INNER JOIN tt_distinct_column_names tt
             ON tt.table_name = tc.table_name
            AND tt.column_name = tc.column_name
       GROUP BY ct.cache_table,
                tt.table_name
    )
         SELECT cache_table,
                pk_cache_table,
                schema,
                unique_expression,
                generation_query,
                group_by_expression,
                table_columns,
                keynum,
                oldest_created,
                ( '{"' || table_name || '":{'
                || '"pk_col":"'   || $_schema.fn_get_table_pk_col( table_name ) || '",'
                || '"pk_vals":['  || array_to_string( primary_keys, ','  ) || '],'
                || '"keys":['     || array_to_string( key_infos,    ','  ) || '],'
                || '"created":"' || created || '"}}' )::JSON AS table_keys
           FROM tt_cache_table_updates
SQL

    if( $DEBUG )
    {
        update( $requests_q );
    }

    my $requests_sth = try_query( $requests_q );

    my $queue_peek_time = tv_interval( $queue_peek_start, [gettimeofday()] );

    if( $_timing )
    {
        update( "queue peek time: $queue_peek_time" );
    }

    # Thread here
    while( my $request = $requests_sth->fetchrow_hashref() )
    {
        my $ct_service_start = [gettimeofday()];
        my $cache_table      = $request->{cache_table};
        my $table_keys       = decode_json( $request->{table_keys} );

        unless( $table_keys )
        {
            update( "TBLMGR ERROR: Failed to decode json for cache table $cache_table" );
            next;
        }

        my $table_count = scalar keys %$table_keys;

        if( $_verbose )
        {
            update( "Processing updates to $cache_table from $table_count base tables." );
        }

        my $cache_table_info = {
            cache_table         => $cache_table,
            pk_cache_table      => $request->{pk_cache_table},
            group_by_expression => $request->{group_by_expression},
            generation_query    => $request->{generation_query},
            unique_expression   => $request->{unique_expression},
            table_columns       => $request->{table_columns},
            schema              => $request->{schema},
        };

        process_ct_updates( $cache_table_info, $table_keys );

        if( $_verbose )
        {
            update( 'Flushing serviced cache update requests' );
        }

        # Assume the child thread will finish its work
        flush_cache_requests_by_table_keys( $table_keys );

        # Store stats
        my $key_count      = $request->{keynum};
        my $oldest_created = $request->{oldest_created};

        my $duration = tv_interval( $ct_service_start, [gettimeofday()] );

        my @table_stats;

        if( $_timing )
        {
            update( "$cache_table service time: $duration" );
        }

        foreach my $table_name ( keys %$table_keys )
        {
            my $pk_vals_array     = $table_keys->{$table_name}->{pk_vals};
            my $table_stats_entry = "\"$table_name\":" . scalar( @{$pk_vals_array} );

            push( @table_stats, $table_stats_entry );
        }

        my $tables_array = join( ',', @table_stats );

        my $JSON = <<JSON;
        {
            "tables":{$tables_array},
            "oldest_request":"$oldest_created",
            "peek_time":$queue_peek_time
        }
JSON
        my $stat_query = <<SQL;
        INSERT INTO $_schema.tb_cache_table_statistic
                    (
                        cache_table,
                        partial_refresh_keys,
                        partial_refresh_duration,
                        additional_stats
                    )
             SELECT cache_table,
                    '$key_count'::INTEGER AS partial_refresh_keys,
                    ( $duration * '1 second'::INTERVAL ) AS partial_refresh_duration,
                    '${JSON}'::JSON AS additional_stats
               FROM $_schema.tb_cache_table
              WHERE table_name = '$cache_table'
SQL

        try_query( $stat_query );

        if( $_verbose )
        {
            update( "Service statistics and timing updated for $cache_table" );
        }
    }

    my $check_remaining_requests_q = <<SQL;
    SELECT COUNT( * ) AS count
      FROM $_schema.tb_cache_update_request
SQL

    my $check_count_sth = try_query( $check_remaining_requests_q );

    my $check_count_row = $check_count_sth->fetchrow_hashref();
    my $count           = $check_count_row->{count};

    if(     $count
        and $count =~ /^\d?$/
        and $count > 0 )
    {
        # Check to see if we have outstanding requests, if so we will need to process them,
        #  but only if the refresher is not active. If the refresher is active, we ignore requests
        #  as these are not pruned until the cache table refresh has been completed.
        if( refresher_is_active() )
        {
            return;
        }

        # indicate to the caller that they should re-execute this subroutine
        # Deep recursion is not acceptable here because memory usage gets out of control
        # for high TPS installations
        return 1;
    }

    return;
}

sub usage($)
{
    my ( $msg ) = @_;
    print "$msg\n" if( $msg );
    my $usage = <<USAGE;
    Usage:
    $0
     -d <database_name>
     -U <username>
     -h <hostname>
     [ -p port ]
USAGE
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0\n";
    print " -d dbname\n";
    print " -U username\n";
    print " -h host\n";
    print " [ -p port ]\n";
    print " [ -P pid_file (default /var/run/tblmgr.db_name.pid)]\n";
    print " [ -s tblmgr_schema (default: tblmgr)\n";
    print " [ -D ( do not daemonize ) ]\n";
    print " [ -V ( verbose ) ]\n";
    print " [ -T ( show timings ) ]\n";
    remove_pid_and_quit();
}

sub _fork_refresher
{
    my @original_argv = @_;

    # Fork refresher process
    my $child_pid = 0;
    my $curr_path = abs_path( $0 );
    $curr_path =~ s/tblmgr\/.*$//;
    $curr_path .= 'tblmgr/';
    print( "Running from '$curr_path'\n" );
    # handle calling either from tblmgr root or within service directory
    my $refresher_path = abs_path( "${curr_path}service/tblmgr_refresher.pl" );
    my $alternate_path = abs_path( "${curr_path}tblmgr_refresher.pl" );

    if(
           ( defined $alternate_path and -e $alternate_path )
        and not ( defined $refresher_path or -e $refresher_path )
      )
    {
        $refresher_path = $alternate_path;
        print "$refresher_path\n";
    }

    unless( defined $refresher_path and -e $refresher_path )
    {
        update( 'TBLMGR ERROR: Could not locate refresher service' );
        remove_pid_and_quit();
    }

    my $check_pid = fork();

    if( defined( $check_pid ) and $check_pid == 0 )
    {
        $child_pid = $check_pid;
    }
    elsif( defined $check_pid and $check_pid > 0 )
    {
        my $command_string = "${refresher_path} @original_argv";
        system( $command_string );
        exit 0;
    }
    else
    {
        update( "TBLMGR ERROR: Failed to fork and start refresher service" );
        remove_pid_and_quit();
    }

    return $child_pid;
}

sub _crash_recovery_handler()
{
    my $check_q = <<"END_SQL";
    SELECT n.nspname::VARCHAR || '.' || c.relname::VARCHAR AS table_name
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.nspname::VARCHAR LIKE '%pg_temp%'
       AND n.oid = c.relnamespace
INNER JOIN $_schema.tb_cache_table ct
        ON ct.table_name = regexp_replace( regexp_replace( c.relname::VARCHAR, '^tt_', '' ), '_compare\$', '' )
END_SQL

    my $sth = try_query( $check_q );

    while( my $row = $sth->fetchrow_hashref() )
    {
        my $fully_qualified_table_name = $row->{table_name};
        try_query( "DROP TABLE $fully_qualified_table_name" );
    }

    return;
}

our ( $opt_D, $opt_d, $opt_U, $opt_h, $opt_p, $opt_P, $opt_V, $opt_T, $opt_s );
my @original_argv = @ARGV;

usage( 'Invalid arguments' ) unless( getopts( 'd:U:h:p:P:s:DVT' ) );

my $dbname = $opt_d;
my $host   = $opt_h;
my $user   = $opt_U;
my $port   = $opt_p;
$no_daemonize = $opt_D;
$pid_file = $opt_P;
$_schema  = $opt_s if( $opt_s );
$_verbose = $opt_V if( $opt_V );
$_timing  = $opt_T if( $opt_T );

$port = 5432 unless( defined $port );
usage( 'Invalid port' ) if( defined $port and ( $port !~ /^\d+$/ or $port < 1 or $port > 65536 ) );
usage( 'Invalid dbname' )   unless( defined $dbname and length( $dbname ) > 0 );
usage( 'Invalid username' ) unless( defined $user   and length( $user ) > 0 );
usage( 'Invalid hostname' ) unless( defined $host   and length( $host ) > 0 );

unless( $no_daemonize )
{
    print "Daemonizing\n";
    daemonize();
}

setup_status_file();

# Setup PID file to avoid race processes
my $pid = $$;

unless( defined $pid_file and length( $pid_file ) > 0 )
{
    $pid_file = $ENV{HOME} . "/tblmgr.$dbname.pid";

    if( $EFFECTIVE_USER_ID == 0 )
    {
        $pid_file = "/var/run/tblmgr.$dbname.pid";
    }
}

my $conn_string = "dbi:Pg:dbname=${dbname};host=${host};port=${port}";
$conn_map->{dbname}      = $dbname;
$conn_map->{conn_string} = $conn_string;
$conn_map->{user}        = $user;

$handle = DBI->connect( $conn_string, $user, undef );

croak( 'Could not connect to the database' ) unless( $handle );

# Pre-flight checks
if( _check_tblmgr_running( $handle ) )
{
    my $message = 'Tblmgr is already running and active on this database';
    update( $message ) unless( $no_daemonize );
    warn( "$message\n" ) if( $no_daemonize );
    exit 127;
}

print "Opening pid file with $pid, $pid_file\n";
manage_pid_file( $pid, $pid_file );

check_extension( $handle );

my $child_pid   = _fork_refresher( @original_argv );
$handle->do( "SET application_name = 'tblmgr updater process'" );
sighup_handler();

# Handle cases where PostgreSQL server crashed and did not clean up
#  our temp tables, orphaning them in the old pg_temp* namespace
_crash_recovery_handler();

# Bind the HUP handler to re-read configuration
$SIG{HUP} = \&sighup_handler;
$SIG{INT} = \&remove_pid_and_quit;

if( $_verbose )
{
    update( 'STARTUP: Handlers bound' );
    update( 'STARTUP: Checking for cache update requests' );
}

process_update_requests();

if( $_verbose )
{
    update( "STARTUP: Listening on channel '$LISTEN_CHANNEL'" );
}

$handle->do( "LISTEN $LISTEN_CHANNEL" );
$file_descriptor = $handle->func( 'getfd' );
$sel             = IO::Select->new( $file_descriptor );

# Inner loop timing variables
my ( $update_process_start, $update_finished, $update_duration );
my ( $time_since_last_notify, $last_notify_time );

while( 1 )
{
    update( 'Pre WAIT for NOTIFY' ) if( $DEBUG );

    $last_notify_time = [gettimeofday()];

    update( "Waiting for NOTIFY on '$LISTEN_CHANNEL'" ) if( $_verbose );

    $sel->can_read;    # Blocks and waits
    my $notify = $handle->func( 'pg_notifies' );

    update( 'Checking notify' ) if( $DEBUG or $_verbose );

    $time_since_last_notify = tv_interval( $last_notify_time, [gettimeofday()] );

    update( "Time since last notify: $time_since_last_notify" ) if( $_timing );

    if( $notify )
    {
        my ( $relname, $pid ) = @$notify;

        if( $relname eq $LISTEN_CHANNEL )
        {
            $update_process_start = [gettimeofday()];
            my $rerun_needed = 1;
            while( $rerun_needed )
            {
                # Curtail memory usage / deep recursion by having
                #  process_update_requests return 1 if it needs
                #  to be re-executed (outstanding requests present),
                #  otherwise return undef
                $rerun_needed = process_update_requests();
            }

            if( $_timing or $_verbose )
            {
                $update_finished = [gettimeofday()];
                $update_duration = tv_interval( $update_process_start, $update_finished );

                update( "Time to process last NOTIFY: $update_duration seconds" );
            }
        }
    }
    else
    {
        try_query( 'SELECT 1' );
    }

    sleep( 0.25 );
}

waitpid( $child_pid, 0 );

exit 0;
