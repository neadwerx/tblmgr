package tblmgr;

use strict;
use warnings;

use Carp;
use Readonly;
use Perl6::Export::Attrs;
use English qw( -no_match_vars );
use Params::Validate qw( :all );

use DBI;
use JSON;
use IO::Select;
use IO::Handle;
use Sys::Syslog;
use Getopt::Std;
use Time::HiRes qw( gettimeofday tv_interval );
use POSIX qw( strftime setsid :sys_wait_h );
use Data::Dumper;
use IO::Interactive qw( is_interactive );
use Cwd qw( abs_path );

Readonly our $DEBUG :Export( :MANDATORY ) => 0;

# Global variables used by both the updater and refresher process
our $_verbose :Export( :MANDATORY ) = 0;
our $_timing  :Export( :MANDATORY ) = 0;
our $_schema  :Export( :MANDATORY ) = 'tblmgr';

our $handle          :Export( :MANDATORY ) = undef;
our $conn_map        :Export( :MANDATORY ) = undef;
our $sel             :Export( :MANDATORY ) = undef;
our $file_descriptor :Export( :MANDATORY ) = undef;
our $no_daemonize    :Export( :MANDATORY ) = 0;

$OUTPUT_AUTOFLUSH = 1;

# Common logging functions
sub update($) :Export( :MANDATORY )
{
    my( $message ) = validate_pos(
        @_,
        { type => SCALAR },
    );

    syslog( 'info', $message );
    print "$message\n" if( is_interactive() or ( $no_daemonize and $DEBUG ) );
    return;
}

sub setup_status_file() :Export( :MANDATORY )
{
    openlog( 'tblmgr', '', 'user' );
    return;
}

sub daemonize() :Export( :MANDATORY )
{
    $SIG{__DIE__} = sub
    {
        update( "ERROR: " . $_[0] );
        exit 1;
    };

    $SIG{__WARN__} = sub
    {
        update( "WARNING: " . $_[0] );
    };

    unless( open STDIN, '/dev/null' )
    {
        croak "Can't read /dev/null: $!";
    }

    unless( open STDOUT, '>/dev/null' )
    {
        croak "Can't write to /dev/null: $!";
    }

    my $pid = fork();

    unless( defined( $pid ) )
    {
        croak "Can't fork: $!";
    }

    if( $pid )
    {
        exit 0;
    }

    unless( setsid() )
    {
        croak "Can't start a new session: $!";
    }

    return;
}


sub manage_pid_file($$) :Export( :updater )
{
    my( $pid, $pid_file ) = validate_pos(
        @_,
        { type => SCALAR, },
        { type => SCALAR, },
    );

    if( -e $pid_file )
    {
        unless( -f $pid_file )
        {
            croak "Lock file '$pid_file' exists, but is not a regular file!";
        }

        unless( -r $pid_file )
        {
            croak "Cannot read lock file '$pid_file'";
        }

        unless( open( PID, "<$pid_file" ) )
        {
            croak "Could not open existing PID file '$pid_file': $!";
        }

        my $line = <PID>;

        if( $line )
        {
            chomp( $line );
        }

        close( PID );

        my $existing_pid;

        if( $line )
        {
            ( $existing_pid ) = ( $line =~ /^(\d+)$/ );
        }

        if(
                defined $existing_pid
            and ( $existing_pid =~ /^\d+$/ )
            and $existing_pid > 0
          )
        {
            my $process_found = 0;
            my $ps_command    = "/bin/ps -e |";

            unless( open( PS, $ps_command ) )
            {
                croak "cannot check for existing processes: $!";
            }

            my $name = $0;
            $name =~ s/^.*(tblmgr)/$1/;

            while( my $ps_line = <PS> )
            {
                chomp( $ps_line );

                if( $ps_line =~ m/$name/ )
                {
                    $process_found = 1;
                    print "Found running $name process\n";
                    last;
                }
            }

            close PS;

            if( $process_found )
            {
                croak "Service already running! PID ($existing_pid)";
            }
            else
            {
                unless( unlink( $pid_file ) )
                {
                    croak "Could not remove stale PID file: $!";
                }

                unless( open( PID, ">$pid_file" ) )
                {
                    croak "Could not open PID file: $!";
                }

                print( PID $pid );
                close( PID );
            }

            return;
        }
        else
        {
            unlink( $pid_file );
        }
    }

    unless( open( PID, '>', $pid_file ) )
    {
        croak "Could not open new PID file '$pid_file': $!";
    }

    print PID $pid;
    close( PID );

    return;
}

1;
