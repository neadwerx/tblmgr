CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    /*
        This function validates the contents of table_columns against the catalog and the contents of generation_query,
        simplifying the error handling in fn_manage_trigger_changes, which helps define the triggers that can issue
        cache_update_request notifications to the tblmgr process.
    */

    -- Get a definition of the cache table, this also serves to validate that the generation_query syntax is correct
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( ' GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    -- Begin iterating over the JSON structure in table_columns. We will validate its contents against the database's catalog
    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        -- Validate that the schema exists        
        PERFORM n.oid
           FROM pg_namespace n
          WHERE nspname::VARCHAR = my_s_record.schema_name;

        IF NOT FOUND THEN
            RAISE NOTICE 'Schema % does not exist in the catalog.', my_s_record.schema_name;
        END IF;

        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists in the schema specified
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind = 'r'
                AND c.relname::VARCHAR = my_ll_record.table_name;
            
            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               regexp_replace( value::VARCHAR, '"', '', 'g' ) AS aliased_column_name -- value is json and will be wrapped in " despite it being a varchar
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   -- Here we validate that the column is present on the table specified, and that the aliased version
                   -- of the column is present in generation_query
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;
                    
                    IF NOT FOUND THEN
                        DROP TABLE tt_cache_table_test;
                        RAISE EXCEPTION 'Table %.% does not have column %!', my_s_record.schema_name, my_ll_record.table_name, my_tl_record.column_name;
                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    -- Final check that unique_expression columns are valid, unaliased, and present in table_columns
    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
EXCEPTION
    WHEN SQLSTATE '42P01' THEN
        RAISE NOTICE 'Table used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42883' THEN
        RAISE NOTICE 'Function used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN OTHERS THEN
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';
