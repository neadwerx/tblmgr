CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    /*
        This function validates the contents of table_columns against the catalog and the contents of generation_query,
        simplifying the error handling in fn_manage_trigger_changes, which helps define the triggers that can issue
        cache_update_request notifications to the tblmgr process.
    */

    -- Get a definition of the cache table, this also serves to validate that the generation_query syntax is correct
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( ' GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    -- Begin iterating over the JSON structure in table_columns. We will validate its contents against the database's catalog
    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        -- Validate that the schema exists
        PERFORM n.oid
           FROM pg_namespace n
          WHERE nspname::VARCHAR = my_s_record.schema_name;

        IF NOT FOUND THEN
            RAISE NOTICE 'Schema % does not exist in the catalog.', my_s_record.schema_name;
        END IF;

        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists in the schema specified
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind IN( 'r', 'v' )
                AND c.relname::VARCHAR = my_ll_record.table_name;

            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               CASE WHEN json_typeof( value ) = 'string'
                                    THEN regexp_replace( value::VARCHAR, '"', '', 'g' ) -- value is json and will be wrapped in " despite it being a varchar
                                    ELSE NULL
                                     END AS aliased_column_name
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   -- Here we validate that the column is present on the table specified, and that the aliased version
                   -- of the column is present in generation_query
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;

                    IF NOT FOUND THEN
                        EXECUTE 'SELECT ' || my_tl_record.column_name
                             || '  FROM ' || my_s_record.schema_name || '.' || my_ll_record.table_name
                             || ' LIMIT 0';
                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    -- Final check that unique_expression columns are valid, unaliased, and present in table_columns
    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
EXCEPTION
    WHEN SQLSTATE '42P01' THEN
        RAISE NOTICE 'Table used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42883' THEN
        RAISE NOTICE 'Function used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42703' THEN
        RAISE NOTICE 'Column referenced in query does not exist';
        RETURN NULL;
    WHEN SQLSTATE '22004' THEN -- caused by the inner structure being invalid - check tl record
        RAISE NOTICE 'JSON parsing error';
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_triggers()
RETURNS VOID AS
 $_$
DECLARE
    my_additional_keys  VARCHAR[]; -- Keeps track of the intersection of unique_expression and triggered columns
    my_func_args        VARCHAR[]; -- Argument list of trigger function
    my_func_arg         VARCHAR;
    my_ct_record        RECORD;
    my_tr_record        RECORD;
    my_query            TEXT; -- Used for trigger creation / debug
BEGIN
    /*
     This function builds up my_func_args for each trigger function it installs

     In normal mode, my_func_args looks like:
      ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1', 'unique_key_2' ]'::JSON::VARCHAR ]::VARCHAR[]
      This gives the trigger a shortcut for the primary key of TG_RELNAME once it executes,
      As well as provides any additional keys that are a member of any unique_expression of a cache table that are also present on the table

     In rollup mode, my_func_args loops like
      ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1' ]'::JSON::VARCHAR, '{ masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }'::JSON::VARCHAR ]::VARCHAR[]
      This provides the keys used in normal mode, along with the masquerading information,
      In this mode, the first two parameters are overwritten so that the trigger can masquerade update operations
    */
    -- Loop over installed triggers and validate that they are correct
    FOR my_tr_record IN(
                        WITH tt_installed_triggers AS
                        (
                            SELECT t.tgname::VARCHAR AS trigger_name,
                                   array_remove( @extschema@.fn_parse_tgargs( t.tgargs ), 'null' ) AS args,
                                   c.relname::VARCHAR AS table_name,
                                   n.nspname::VARCHAR AS schema_name
                              FROM pg_trigger t
                        INNER JOIN pg_class c
                                ON c.oid = t.tgrelid
                               AND c.relkind = 'r'
                               AND c.relhastriggers IS TRUE
                        INNER JOIN pg_namespace n
                                ON n.oid = c.relnamespace
                             WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request'
                        ),
                        tt_needed_triggers AS
                        (
                            WITH tt_baseline_triggers AS
                            (
                                SELECT DISTINCT ON( tc.schema_name, tc.table_name )
                                       array_agg( DISTINCT ct.table_name ) AS dependent_cts,
                                       tc.schema_name,
                                       tc.table_name,
                                       @extschema@.fn_get_table_pk_col( tc.table_name ) AS primary_arg,
                                       array_agg( DISTINCT tc.column_name ) AS required_columns,
                                       array_agg( DISTINCT a.attname::VARCHAR ) AS available_columns,
                                       nullif( array_remove( array_agg( DISTINCT x ), NULL ), ARRAY[]::VARCHAR[] )::VARCHAR AS sec
                                  FROM @extschema@.tb_cache_table ct
                            INNER JOIN @extschema@.tb_cache_table_trigger_column cttc
                                    ON cttc.cache_table = ct.cache_table
                            INNER JOIN @extschema@.tb_trigger_column tc
                                    ON tc.trigger_column = cttc.trigger_column
                            INNER JOIN pg_class c
                                    ON c.relname::VARCHAR = tc.table_name
                            INNER JOIN pg_namespace n
                                    ON n.oid = c.relnamespace
                                   AND n.nspname::VARCHAR = tc.schema_name
                            INNER JOIN pg_attribute a
                                    ON a.attrelid = c.oid
                                   AND a.attnum > 0
                             LEFT JOIN unnest( ct.unique_expression ) x
                                    ON x = a.attname
                              GROUP BY tc.schema_name,
                                       tc.table_name
                            )
                                SELECT table_name,
                                       schema_name,
                                       array_remove( ARRAY[ primary_arg ] || sec, NULL ) AS args
                                  FROM tt_baseline_triggers
                        )
                            SELECT COALESCE( ttnt.schema_name, ttit.schema_name ) AS schema_name,
                                   COALESCE( ttnt.table_name, ttit.table_name ) AS table_name,
                                   @extschema@.fn_get_table_pk_col( COALESCE( ttnt.table_name, ttit.table_name ) ) AS primary_key,
                                   ttit.args AS old_def,
                                   ttnt.args AS new_def,
                                   CASE WHEN ttit.args != ttnt.args AND ttit.table_name IS NOT NULL
                                        THEN 'UPDATE_TRIGGER'
                                        WHEN ttit.table_name IS NULL
                                        THEN 'CREATE_TRIGGER'
                                        ELSE 'DROP_TRIGGER'
                                         END AS operation
                              FROM tt_needed_triggers ttnt
                        FULL OUTER JOIN tt_installed_triggers ttit
                                ON ttit.schema_name = ttnt.schema_name
                               AND ttit.table_name = ttnt.table_name
                             WHERE ( ttit.args <> ttnt.args OR ttit.args IS NULL )
                       ) LOOP
        /*
            CREATE_TRIGGER: Trigger does not exist, but a new or updated cache_table needs it to be present
            UPDATE_TRIGGER: Trigger exists, but its definition (arguments) need to be updated to match a change in requirements
                            caused by an update to a cache table.
            DELETE_TRIGGER: Trigger exists, but has been deprecated by the removal of a cache table, or a change in a cache table's
                            definition.
        */
        my_additional_keys := NULL::VARCHAR[]; -- Reset to avoid trigger bugs
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            RAISE DEBUG 'Dropping trigger on %.%', my_tr_record.schema_name, my_tr_record.table_name;
            EXECUTE 'DROP TRIGGER IF EXISTS tr_queue_cache_update_request ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        -- Begin creating trigger definition
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            my_func_args[1] := my_tr_record.primary_key;
            RAISE DEBUG 'Performing trigger op % on %.%', my_tr_record.operation, my_tr_record.schema_name, my_tr_record.table_name;
            -- Validate that the table has a primary key constraint. If it does not, abort trigger creation. This corrects an
            -- issue on restore when the primary key constraints are not set when the cache table is loaded at restore-time.
            PERFORM a.attname
               FROM pg_class c
               JOIN pg_constraint cs
                 ON cs.conrelid = c.oid
                AND cs.contype = 'p' --primary key
               JOIN pg_attribute a
                 ON a.attnum = ANY( cs.conkey )
                AND a.attrelid = c.oid
                AND a.attnum > 0
              WHERE c.relname::VARCHAR = my_tr_record.table_name;

            IF NOT FOUND THEN
                RAISE NOTICE 'Table % lacks a primary key constraint, @extschema@ cannot source updates from this table', my_tr_record.table_name;
                CONTINUE;
            END IF;
            IF( my_tr_record.new_def[1] IS NOT NULL ) THEN
                my_func_args[2] := my_tr_record.new_def[2];
                RAISE DEBUG 'Setting secondary argument for %.% trigger to %', my_tr_record.schema_name, my_tr_record.table_name, my_tr_record.new_dev[2];
            END IF;
            PERFORM *
               FROM pg_class c
               JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_tr_record.schema_name
              WHERE c.relname::VARCHAR = my_tr_record.table_name
                AND c.relkind = 'r';

            IF FOUND THEN
                 -- Create trigger
                my_query := format(
                            'CREATE TRIGGER tr_queue_cache_update_request'
                         || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                         || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L )',
                            my_func_args[1],
                            my_func_args[2]
                        );
                EXECUTE my_query;
                RAISE DEBUG 'Created trigger on %.%', my_tr_record.schema_name, my_tr_record.table_name;
            ELSE
                RAISE DEBUG 'Creation of trigger on %.% skipped - not valid', my_tr_record.schema_name, my_tr_record.table_name;
            END IF;
        END IF;
    END LOOP;
    /*
        Handles the creation of rollup-mode triggers. These triggers have a third parameter that indicate what table they are
        masquerading updates as
    */
    FOR my_tr_record IN(
            WITH tt_current_trigger_info AS
            (
                SELECT ct.cache_table,
                       COALESCE( bn.nspname::VARCHAR, 'public' ) AS schema_name,
                       bcl.relname::VARCHAR AS table_name,
                       je.value AS value,
                       ba.attname AS foreign_key,
                       (
                            '{"masquerades_for_table":"'
                         || COALESCE( mn.nspname::VARCHAR, 'public' ) || '.' || mcl.relname::VARCHAR
                         || '","fk_column_name":"'
                         || ba.attname
                         || '"}'
                       )::JSON AS rollup_info
                  FROM @extschema@.tb_cache_table ct
    INNER JOIN LATERAL (
                            SELECT ct.cache_table,
                                   value
                              FROM json_array_elements( ct.has_rollup_from_table )
                       ) jae
                    ON jae.cache_table = ct.cache_table
    INNER JOIN LATERAL (
                            SELECT jae.cache_table,
                                   key::VARCHAR,
                                   regexp_replace( value::VARCHAR, '"', '', 'g' ) AS value
                              FROM json_each( jae.value )
                       ) je
                    ON je.cache_table = ct.cache_table
            INNER JOIN pg_class bcl -- base table
                    ON bcl.relkind = 'r'
                   AND bcl.relname::VARCHAR = regexp_replace( je.key, '^.*\.', '' ) -- get table_name
             LEFT JOIN pg_namespace bn
                    ON bn.oid = bcl.relnamespace
                   AND bn.nspname::VARCHAR = regexp_replace( je.key, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_class mcl -- masqueraded table
                    ON mcl.relkind = 'r'
                   AND mcl.relname::VARCHAR = regexp_replace( je.value, '^.*\.', '' ) -- get table_name
             LEFT JOIN pg_namespace mn
                    ON mn.oid = mcl.relnamespace
                   AND mn.nspname::VARCHAR = regexp_replace( je.value, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_attribute ba
                    ON ba.attrelid = bcl.oid
                   AND ba.attnum > 0
            INNER JOIN pg_attribute ma
                    ON ma.attrelid = mcl.oid
                   AND ma.attnum > 0
            INNER JOIN pg_constraint bc
                    ON bc.conrelid = bcl.oid
                   AND bc.conrelid = ba.attrelid
                   AND bc.contype = 'f'
                   AND ba.attnum = ANY( bc.conkey )
                   AND bc.confrelid = mcl.oid
            INNER JOIN pg_constraint mc
                    ON mc.conrelid = mcl.oid
                   AND mc.conrelid = ma.attrelid
                   AND mc.contype = 'p'
                   AND ma.attnum = ANY( mc.conkey )
                 WHERE has_rollup_from_table IS NOT NULL
            ),
            tt_installed_trigger_info AS
            (
                SELECT t.tgname::VARCHAR AS trigger_name,
                       t.tgargs,
                       n.nspname::VARCHAR AS schema_name,
                       c.relname::VARCHAR AS table_name
                  FROM pg_trigger t
            INNER JOIN pg_class c
                    ON c.oid = t.tgrelid
                   AND c.relkind = 'r'
                   AND c.relhastriggers IS TRUE
            INNER JOIN pg_namespace n
                    ON n.oid = c.relnamespace
                 WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request_from_rollup_table'
            ),
            tt_trigger_logic AS
            (
                SELECT COALESCE( ttct.schema_name, ttit.schema_name ) AS schema_name,
                       COALESCE( ttct.table_name, ttit.table_name ) AS table_name,
                       CASE WHEN ttct.table_name IS NULL
                             AND ttit.table_name IS NOT NULL
                            THEN 'DROP_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NULL
                            THEN 'CREATE_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NOT NULL
                             AND rollup_info::VARCHAR IS DISTINCT FROM ( ( @extschema@.fn_parse_tgargs( ttit.tgargs ) )[2] )::JSON::VARCHAR
                            THEN 'UPDATE_TRIGGER'
                            ELSE NULL
                             END AS logic_result,
                       ttct.rollup_info,
                       ttit.trigger_name
                  FROM tt_current_trigger_info ttct
       FULL OUTER JOIN tt_installed_trigger_info ttit
                    ON ttit.table_name = ttct.table_name
            )
                SELECT schema_name,
                       table_name,
                       logic_result AS operation,
                       rollup_info,
                       trigger_name
                  FROM tt_trigger_logic
                 WHERE logic_result IS NOT NULL
                       ) LOOP
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            EXECUTE 'DROP TRIGGER ' || my_tr_record.trigger_name || ' ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            SELECT @extschema@.fn_get_table_pk_col( my_tr_record.table_name )::VARCHAR
              INTO my_func_arg;

            my_func_args[1] := my_func_arg;
            my_func_args[2] := 'null'::JSON::VARCHAR;
            my_func_args[3] := my_tr_record.rollup_info::VARCHAR;

            my_query := format(
                        'CREATE TRIGGER tr_queue_cache_update_request_from_rollup_table'
                     || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                     || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L, %L )',
                         my_func_args[1],
                         my_func_args[2],
                         my_func_args[3]
                    );
            EXECUTE my_query;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

ALTER TABLE @extschema@.tb_cache_table
    ADD COLUMN staggered_generation BOOLEAN NOT NULL DEFAULT TRUE;

COMMENT ON COLUMN @extschema@.tb_cache_table.staggered_generation IS
    'Refreshes in a staggered manner (default 10% at a time)';
