CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table
(
    in_cache_table_name    VARCHAR
)
RETURNS BOOLEAN AS
 $_$
BEGIN
    -- These are transaction level advisory locks, and will be replaced with session-level advisory locks by tblmgr_refresher.pl.
    -- these are simply inplaced to reduce the possibility of a race condition.
    IF NOT pg_try_advisory_xact_lock(
                    '@extschema@.tb_cache_table'::REGCLASS::INTEGER,
                    cache_table
                )
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
    THEN
        RAISE NOTICE 'Aborted attempt to refresh cache table, a refresh is already in progress';
        RETURN FALSE;
    END IF;

    -- Create a special entry in tb_cache_update_request for tblmgr_refresher to read in and begin the full refresh process
    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    created,
                    disable_flush,
                    full_refresh_request
                )
         SELECT schema,
                table_name,
                cache_table,
                now(),
                FALSE,
                TRUE
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name;

    RETURN TRUE;
END
 $_$
    LANGUAGE 'plpgsql';
