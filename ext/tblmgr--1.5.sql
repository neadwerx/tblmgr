-- Initial Setup, Version validation
DO
 $_$
DECLARE
    my_value    VARCHAR;
    my_version  INTEGER[];
    my_command  VARCHAR;
BEGIN
    my_version := regexp_matches( version(), 'PostgreSQL (\d)+\.(\d+)\.(\d+)' );

    -- Verify minimum version requirements are met
    IF my_version < ARRAY[9,3,1]::INTEGER[] THEN
        RAISE EXCEPTION 'Tblmgr requires version 9.3.1 or greater';
    END IF;

    IF( quote_ident( current_database() ) = 'postgres' ) THEN
        RAISE EXCEPTION 'Cannot install tblmgr on the postgres database';
    END IF;

    my_command := 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.';

    EXECUTE my_command || 'enable_notify = ''1''';
    EXECUTE my_command || 'enable_triggers = ''1''';
    EXECUTE my_command || 'enable_retry = ''1''';
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_get_config
(
    in_config_name  VARCHAR
)
RETURNS TEXT AS
 $_$
DECLARE
    my_config_value TEXT;
BEGIN
    RETURN NULLIF( current_setting( '@extschema@.' || in_config_name, TRUE ), '' );
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_tblmgr()
RETURNS VOID AS
 $_$
DECLARE
    my_command  VARCHAR;
BEGIN
    my_command := 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.';

    EXECUTE my_command || 'enable_notify = ''0''';
    EXECUTE my_command || 'enable_triggers = ''0''';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_enable_tblmgr()
RETURNS VOID AS
 $_$
DECLARE
    my_command  VARCHAR;
BEGIN
    my_command := 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.';

    EXECUTE my_command || 'enable_notify = ''1''';
    EXECUTE my_command || 'enable_triggers = ''1''';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_enable_query_retry()
RETURNS VOID AS
 $_$
BEGIN
    EXECUTE 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.enable_retry = ''1''';
    RAISE NOTICE 'GUC updated. Please issue a SIGHUP to the tblmgr daemon';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_query_retry()
RETURNS VOID AS
 $_$
BEGIN
    EXECUTE 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.enable_retry = ''0''';
    RAISE NOTICE 'GUC updated. Please issue a SIGHUP to the tblmgr daemon';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE SEQUENCE @extschema@.sq_pk_cache_update_request MAXVALUE 2147483647 CYCLE;

CREATE UNLOGGED TABLE IF NOT EXISTS @extschema@.tb_cache_update_request
(
    cache_update_request    INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_cache_update_request' ),
    schema_name             VARCHAR NOT NULL,
    table_name              VARCHAR NOT NULL,
    primary_key             INTEGER NOT NULL,
    key_info                JSON,
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    disable_flush           BOOLEAN NOT NULL DEFAULT FALSE,
    full_refresh_request    BOOLEAN NOT NULL DEFAULT FALSE
)
WITH
(
    OIDS = FALSE
);

COMMENT ON COLUMN @extschema@.tb_cache_update_request.full_refresh_request IS
    'Initiates a full refresh on the cache table by inserting the table name into tb_cache_update_request containing the cache table information with this flag set TRUE';

CREATE INDEX ix_cache_update_request_table_name ON @extschema@.tb_cache_update_request( table_name );

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_flush()
RETURNS TRIGGER AS
 $_$
BEGIN
    -- Check for in-progress cache table refreshes by looking for advisory locks held by fn_refresh_cache_table()
    PERFORM pid
       FROM pg_locks
      WHERE locktype = 'advisory'
        AND mode = 'ExclusiveLock'
        AND classid = '@extschema@.tb_cache_table'::REGCLASS::OID;

    IF FOUND THEN
        NEW.disable_flush := TRUE;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_disable_flush
    BEFORE INSERT ON @extschema@.tb_cache_update_request
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_disable_flush();

CREATE OR REPLACE FUNCTION @extschema@.fn_issue_cache_update_request()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_notify_enabled TEXT;
BEGIN
    IF( @extschema@.fn_get_config( 'enable_notify' ) = '0' ) THEN
        RETURN NEW;
    END IF;

    IF( NEW.full_refresh_request IS FALSE ) THEN
        NOTIFY cache_update_request;
    ELSIF( NEW.full_refresh_request IS TRUE ) THEN
        NOTIFY full_refresh_request;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_cache_update_request
    AFTER INSERT ON @extschema@.tb_cache_update_request
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_issue_cache_update_request();

CREATE SEQUENCE @extschema@.sq_pk_trigger_column;

CREATE TABLE IF NOT EXISTS @extschema@.tb_trigger_column
(
    trigger_column INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_trigger_column' ),
    schema_name    VARCHAR NOT NULL,
    table_name     VARCHAR NOT NULL,
    column_name    VARCHAR NOT NULL,
    UNIQUE( schema_name, table_name, column_name )
);

CREATE SEQUENCE @extschema@.sq_pk_cache_table;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_table
(
    cache_table                 INTEGER PRIMARY KEY DEFAULT nextval('@extschema@.sq_pk_cache_table'),
    table_name                  VARCHAR NOT NULL,
    generation_query            TEXT NOT NULL,
    unique_expression           VARCHAR[] NOT NULL,
    group_by_expression         VARCHAR[] DEFAULT NULL::VARCHAR[],
    has_rollup_from_table       JSON DEFAULT NULL::JSON,
    schema                      VARCHAR NOT NULL DEFAULT 'public',
    table_columns               JSON NOT NULL, -- TODO: Format for this field needs documentation
    inherits_table              VARCHAR,
    index_columns               JSON,
    created                     TIMESTAMP NOT NULL DEFAULT now(),
    staggered_generation        BOOLEAN NOT NULL DEFAULT TRUE,
    UNIQUE( schema, table_name )
);

COMMENT ON TABLE @extschema@.tb_cache_table IS
    'Stores information related to cache tables. Each row is a definition of a independent cache table';
COMMENT ON COLUMN @extschema@.tb_cache_table.generation_query IS
    'The query used to populate the table';
COMMENT ON COLUMN @extschema@.tb_cache_table.unique_expression IS
    'used to generate a unique index on the output columns. This information is used to perform updates, inserts, and deletes on the cache table, keeping is up-to-date. The elements of this array should be the column names as they appear in the output.';
COMMENT ON COLUMN @extschema@.tb_cache_table.group_by_expression IS
    'input column names that are used to group the output set. These should include aliases';
COMMENT ON COLUMN @extschema@.tb_cache_table.has_rollup_from_table IS
    'JSON structure in the following format: { "source_table":"table_to_masquerade_updates_for"}. The update engine will tread a modification to source_table in the above structure as an update to the table_to_masquerade_updates_for table instead. This requires a foreign key linking the two tables. For example, if the following structure is present: { "tb_reset_issue":"tb_reset" } The engine will follow treat a modification to tb_reset_issue as a modification to tb_reset by replcaing the primary key of tb_reset_issue with that of tb_reset, using the foreign key';
COMMENT ON COLUMN @extschema@.tb_cache_table.schema IS
    'The schema that the cache table is stored in';
COMMENT ON COLUMN @extschema@.tb_cache_table.table_columns IS
    'JSON structure used to identify source tables and aliases for all tables used in the query. An example of this structure is as follows: {"tb_reset":{"r":{"reset":"reset_id","project":"Project ID"}}}. The inner-most structure lists columns used in the query, along with their aliases. Leave the alias value as null for columns used in aggregates, functions, etc. Support for this will be added later.';
COMMENT ON COLUMN @extschema@.tb_cache_table.inherits_table IS
    'Identifies a cache table that is inherited by this cache table entry - moved from an FK to prevent problems when running pg_dump';
COMMENT ON COLUMN @extschema@.tb_cache_table.index_columns IS
    'Identifies the columns and index types. An example is as follows: {"some_output_column":"GIN( some_immutable_function( some_output_column ) )","another_column":"BTREE","yet_another_column":BTREE"}. For now only GIN and BTREE are supported';
COMMENT ON COLUMN @extschema@.tb_cache_table.staggered_generation IS
    'Refreshes in a staggered manner (default 10% at a time)';

CREATE UNLOGGED TABLE IF NOT EXISTS @extschema@.tb_cache_table_statistic
(
    cache_table                 INTEGER NOT NULL REFERENCES @extschema@.tb_cache_table,
    created                     TIMESTAMP NOT NULL DEFAULT now(),
    partial_refresh_keys        INTEGER NOT NULL DEFAULT 0,
    partial_refresh_duration    INTERVAL,
    full_refresh_rows           INTEGER NOT NULL DEFAULT 0,
    full_refresh_duration       INTERVAL,
    additional_stats            JSON
)
WITH
(
    OIDS = FALSE
);

CREATE INDEX ix_cache_table_statistic_cache_table ON @extschema@.tb_cache_table_statistic( cache_table );
CREATE SEQUENCE @extschema@.sq_pk_cache_table_trigger_column;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_table_trigger_column
(
    cache_table_trigger_column  INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_cache_table_trigger_column' ),
    cache_table                 INTEGER NOT NULL REFERENCES @extschema@.tb_cache_table,
    trigger_column              INTEGER NOT NULL REFERENCES @extschema@.tb_trigger_column,
    UNIQUE( cache_table, trigger_column )
);

COMMENT ON TABLE @extschema@.tb_cache_table_trigger_column IS
    'Stores relationship between cache tables and tables which have triggers that issue cache_update_requests. This allows for tracking of which triggers a cache table rely on.';

CREATE INDEX ix_cache_table_trigger_column_trigger_column ON @extschema@.tb_cache_table_trigger_column( trigger_column );

CREATE OR REPLACE FUNCTION @extschema@.fn_get_table_pk_col
(
    in_table_name   VARCHAR,
    in_schema_name  VARCHAR DEFAULT 'public'
)
RETURNS VARCHAR AS
 $_$
    SELECT a.attname::VARCHAR
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname = in_schema_name
      JOIN pg_attribute a
        ON a.attrelid = c.oid
      JOIN pg_constraint cn
        ON cn.conrelid = c.oid
       AND cn.contype = 'p'
       AND cn.conkey[1] = a.attnum
     WHERE c.relname::VARCHAR = in_table_name
       AND c.relkind = 'r';
 $_$
    LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION @extschema@.fn_parse_tgargs
(
    in_tgargs   BYTEA
)
RETURNS VARCHAR[] AS
 $_$
    SELECT string_to_array(
               regexp_replace(
                   encode(
                       in_tgargs,
                       'escape'
                   )::VARCHAR,
                   '\\000$',
                   ''
               ),
               '\000'
           )::VARCHAR[];
 $_$
    LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record               RECORD;
    my_table_name           VARCHAR;
    my_pk_col               VARCHAR;
    my_pk_val               INTEGER;
    my_rollup_info          JSON;
    my_ct_record            RECORD; -- Cache table record
    my_tl_record            RECORD; -- Top level record (JSON table_columns)
    my_ml_record            RECORD; -- Mid level record (JSON table_columns)
    my_ll_record            RECORD; -- Low level record (JSON table_columns)
    my_additional_keys      VARCHAR[];
    my_additional_key_info  VARCHAR;
    my_key_value            VARCHAR;
    my_fk_val               INTEGER;
    my_schema_name          VARCHAR;
BEGIN
    /*
        Expects the following:
            JSON table_columns to be in the following format:
                schema_name: {
                    table_name: {
                        table_alias_used_in_query: {
                            real_column_name:output_column_name(aliased)
                        }
                    },
                    table_name_2: {
                        table_alias_used_in_query: {
                            real_column_name:output_column_name(aliased)
                        }
                    }
                }


            Input rollup_info JSON in the following format:

            { masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }
    */
    -- Move psuedorecords into common record variable
    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        my_record := OLD;
    END IF;

    -- check enabled GUC, exit early if triggers are disabled
    IF( @extschema@.fn_get_config( 'enable_triggers' ) = '0' ) THEN
        RETURN my_record;
    END IF;

    -- Parse args
    SELECT c.relname::VARCHAR,
           n.nspname::VARCHAR
      INTO my_table_name,
           my_schema_name
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.oid = c.relnamespace
     WHERE c.oid = TG_RELID;

    my_pk_col     := TG_ARGV[0]::VARCHAR;


    -- Handle conversion of 2nd parameter if it's null ( could be the case if there is no intersection between
    -- @extschema@.tb_cache_table.unique_expression and the keys of table_columns
    IF( TG_ARGV[1]::VARCHAR ~* 'null' ) THEN
        my_additional_keys := NULL::VARCHAR[];
    ELSE
        my_additional_keys := TG_ARGV[1]::VARCHAR[];
    END IF;

    -- presence of a third parameter indicates this trigger function is operating in rollup mode.
    -- in rollup mode, the trigger masquerades updates as if they occurred on another table
    IF( TG_NARGS = 3 ) THEN
        my_rollup_info     := TG_ARGV[2]::JSON;
    ELSIF( TG_NARGS > 3 ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid call';
        RETURN my_record;
    END IF;

    -- Sanity check, this prevents transaction stopping errors that occur during restores if pg_depends
    -- is populated incorrectly during extension installation
    IF( my_pk_col IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid pk parameter';
        RETURN my_record;
    END IF;

    -- Get the value of the primary key column for the tuple that was modified by DML
    EXECUTE 'SELECT $1.' || my_pk_col
       INTO my_pk_val
      USING my_record;

    IF( my_pk_val IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request - could not determine pk value';
        RETURN my_record;
    END IF;

     -- Look for the intersection of a column in this table with the unique_expression on the target cache table
     -- This helps speed lookups as a unique index ( and possible a BTREE index on the base table FK) is used.
    IF( my_additional_keys IS NOT NULL ) THEN
        my_additional_key_info := '{';

        FOR my_ll_record IN( SELECT unnest( my_additional_keys )::VARCHAR AS key ) LOOP
            EXECUTE 'SELECT $1.' || my_ll_record.key || '::VARCHAR'
               INTO my_key_value
              USING my_record;

            my_additional_key_info := my_additional_key_info || '"' || my_ll_record.key || '":"' || my_key_value || '",';
        END LOOP;

        my_additional_key_info := rtrim( my_additional_key_info, ',' ) ||'}';
    END IF;

    -- Detect alternate (rollup) mode of operation, rewriting the variables as needed
    IF( my_rollup_info IS NOT NULL ) THEN
        FOR my_ll_record IN(
                SELECT key::VARCHAR,
                       value::VARCHAR
                  FROM json_each( my_rollup_info )
                           ) LOOP
            IF( my_ll_record.key = 'fk_column_name' ) THEN
                EXECUTE 'SELECT $1.' || my_ll_record.value
                   INTO my_fk_val
                  USING my_record;
            ELSIF( my_ll_record.key = 'masquerades_for_table' ) THEN
                my_table_name  := regexp_replace(
                                      regexp_replace( my_ll_record.value, '^.*\.', '' ),
                                      '"',
                                      '',
                                      'g'
                                  );
                my_schema_name := regexp_replace(
                                      regexp_replace( my_ll_record.value, '\..*$', '' ),
                                      '"',
                                      '',
                                      'g'
                                  );

                IF( my_schema_name = my_table_name ) THEN
                    my_schema_name := 'public';
                END IF;
            END IF;
        END LOOP;

        IF( my_table_name = TG_RELNAME ) THEN --rewrite failed
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because of invalid JSON parameter';
            RETURN my_record;
        END IF;

        IF( my_fk_val IS NULL ) THEN
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because fk column is incorrect or NULL';
        END IF;

        my_pk_val     := my_fk_val;
        my_table_name := regexp_replace( my_table_name, '"', '', 'g' ); -- Fix quotes
    END IF;

    -- Now that everything is buttoned up, issue a cache_update_request. This will cascade into a NOTIFY that will let
    -- tblmgr.pl know the queue is not empty.
    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    key_info
                )
         VALUES
                (
                    my_schema_name,
                    my_table_name,
                    my_pk_val,
                    my_additional_key_info::JSON
                );

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_trigger_changes()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record                       RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_s_record                     RECORD;
    my_trigger_column               INTEGER;
    my_cache_table_trigger_column   INTEGER;
BEGIN
    /*
        This function handles the modification of tb_cache_table.trigger_columns,
        and ensures that both tb_trigger_column and tb_cache_table_trigger_column are populated
        correctly prior to the execution of fn_manage_triggers, which installs/removes/updates triggers
        accordingly.
    */
    IF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
        -- Row's being removed - delete cache_table_trigger_columns that map triggered columns to cache_tables
        DELETE FROM @extschema@.tb_cache_table_trigger_column
              WHERE cache_table = my_record.cache_table;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW.table_columns::VARCHAR IS DISTINCT FROM OLD.table_columns::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSE
        RETURN NULL;
    END IF;

    IF( TG_OP != 'DELETE' ) THEN
        IF( TG_OP = 'UPDATE' ) THEN
            -- If this is an update, we store all the table_columns encountered for use later
            CREATE TEMP TABLE tt_update_handler
            (
                schema_name VARCHAR,
                table_name  VARCHAR,
                column_name VARCHAR
            );
        END IF;
        -- Iterate over the table_columns JSON and create a temp table using the contents
        FOR my_s_record IN(
                SELECT key::VARCHAR AS schema_name,
                       value
                  FROM json_each( my_record.table_columns )
                          ) LOOP
            FOR my_ll_record IN(
                    SELECT key::VARCHAR AS table_name,
                           value
                      FROM json_each( my_s_record.value )
                               ) LOOP
                FOR my_ml_record IN(
                        SELECT key::VARCHAR AS alias,
                               value
                          FROM json_each( my_ll_record.value )
                                   ) LOOP
                    FOR my_tl_record IN(
                            SELECT key::VARCHAR AS column_name,
                                   value::VARCHAR AS aliased_column_name
                              FROM json_each( my_ml_record.value )
                                       ) LOOP
                        -- Validate that a trigger_column record exists for this table / column combination
                        IF( TG_OP = 'UPDATE' ) THEN
                            -- Store this table_column entry into our temp table
                            INSERT INTO tt_update_handler
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        );
                        END IF;

                        -- Get or create functionality for tb_trigger_column
                        SELECT trigger_column
                          INTO my_trigger_column
                          FROM @extschema@.tb_trigger_column
                         WHERE schema_name = my_s_record.schema_name
                           AND table_name  = my_ll_record.table_name
                           AND column_name = my_tl_record.column_name;

                        IF( my_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_trigger_column
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        )
                              RETURNING trigger_column
                                   INTO my_trigger_column;
                        END IF;

                        -- Get or create functionality for tb_cache_table_trigger_column
                        SELECT cache_table_trigger_column
                          INTO my_cache_table_trigger_column
                          FROM @extschema@.tb_cache_table_trigger_column
                         WHERE cache_table = my_record.cache_table
                           AND trigger_column = my_trigger_column;

                        IF( my_cache_table_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_cache_table_trigger_column
                                        (
                                            cache_table,
                                            trigger_column
                                        )
                                 VALUES
                                        (
                                            my_record.cache_table,
                                            my_trigger_column
                                        )
                              RETURNING cache_table_trigger_column
                                   INTO my_cache_table_trigger_column;
                        END IF;
                    END LOOP;
                END LOOP;
            END LOOP;
        END LOOP;

        IF( TG_OP = 'UPDATE' ) THEN
            -- Since the UPDATE may have removed entries in table_columns, we need to validate
            -- All the table triggers we've seen while parsing table_columns against existing entries,
            -- removing the cache_table_trigger_columns referencing those trigger_columns
            WITH tt_candidate_triggers AS
            (
                SELECT trigger_column
                  FROM @extschema@.tb_trigger_column tt
             LEFT JOIN tt_update_handler uh
                    ON uh.column_name = tt.column_name
                   AND uh.table_name = tt.table_name
                   AND uh.schema_name = tt.schema_name
                 WHERE uh.schema_name IS NULL
                   AND uh.column_name IS NULL
                   AND uh.table_name IS NULL
            ),
            tt_rows_to_remove AS
            (
                SELECT cttc.cache_table_trigger_column
                  FROM @extschema@.tb_cache_table_trigger_column cttc
            INNER JOIN tt_candidate_triggers tt
                    ON tt.trigger_column = cttc.trigger_column
                 WHERE cttc.cache_table = my_record.cache_table
            )
                DELETE FROM @extschema@.tb_cache_table_trigger_column cttc
                      USING tt_rows_to_remove tt
                      WHERE tt.cache_table_trigger_column = cttc.cache_table_trigger_column;
        END IF;
    END IF;

    IF( TG_OP = 'UPDATE' OR TG_OP = 'DELETE' ) THEN
        -- Since DELETE or UPDATE operations could have removed cache_table_trigger_column entries,
        -- we will mop up unused trigger_column entries
        WITH tt_trigger_columns_to_delete AS
        (
            SELECT DISTINCT tc.trigger_column
              FROM @extschema@.tb_trigger_column tc
         LEFT JOIN @extschema@.tb_cache_table_trigger_column cttc
                ON cttc.trigger_column = tc.trigger_column
             WHERE cttc.cache_table_trigger_column IS NULL
        )
        DELETE FROM @extschema@.tb_trigger_column tc
              USING tt_trigger_columns_to_delete tt
              WHERE tt.trigger_column = tc.trigger_column;
    END IF;

    -- We've maintained tb_cache_table_trigger_column and tb_trigger_column. Now we'll execute fn_manage_triggers so that it can
    -- CREATE / UPDATE / DROP triggers according to their new definitions, if they have changed.
    PERFORM @extschema@.fn_manage_triggers();
    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_catch_trigger_changes
    AFTER INSERT OR DELETE OR UPDATE OF table_columns ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_manage_trigger_changes();

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    /*
        This function validates the contents of table_columns against the catalog and the contents of generation_query,
        simplifying the error handling in fn_manage_trigger_changes, which helps define the triggers that can issue
        cache_update_request notifications to the tblmgr process.
    */

    -- Get a definition of the cache table, this also serves to validate that the generation_query syntax is correct
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( ' GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    -- Begin iterating over the JSON structure in table_columns. We will validate its contents against the database's catalog
    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        -- Validate that the schema exists
        PERFORM n.oid
           FROM pg_namespace n
          WHERE nspname::VARCHAR = my_s_record.schema_name;

        IF NOT FOUND THEN
            RAISE NOTICE 'Schema % does not exist in the catalog.', my_s_record.schema_name;
        END IF;

        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists in the schema specified
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind IN( 'r', 'v' )
                AND c.relname::VARCHAR = my_ll_record.table_name;

            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               CASE WHEN json_typeof( value ) = 'string'
                                    THEN regexp_replace( value::VARCHAR, '"', '', 'g' ) -- value is json and will be wrapped in " despite it being a varchar
                                    ELSE NULL
                                     END AS aliased_column_name
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   -- Here we validate that the column is present on the table specified, and that the aliased version
                   -- of the column is present in generation_query
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;

                    IF NOT FOUND THEN
                        EXECUTE 'SELECT ' || my_tl_record.column_name
                             || '  FROM ' || my_s_record.schema_name || '.' || my_ll_record.table_name
                             || ' LIMIT 0';
                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    -- Final check that unique_expression columns are valid, unaliased, and present in table_columns
    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
EXCEPTION
    WHEN SQLSTATE '42P01' THEN
        RAISE NOTICE 'Table used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42883' THEN
        RAISE NOTICE 'Function used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42703' THEN
        RAISE NOTICE 'Column referenced in query does not exist';
        RETURN NULL;
    WHEN SQLSTATE '22004' THEN -- caused by the inner structure being invalid - check tl record
        RAISE NOTICE 'JSON parsing error';
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_validate_table_columns
    BEFORE INSERT OR UPDATE OF table_columns ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_validate_table_columns();

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_triggers()
RETURNS VOID AS
 $_$
DECLARE
    my_additional_keys  VARCHAR[]; -- Keeps track of the intersection of unique_expression and triggered columns
    my_func_args        VARCHAR[]; -- Argument list of trigger function
    my_func_arg         VARCHAR;
    my_ct_record        RECORD;
    my_tr_record        RECORD;
    my_query            TEXT; -- Used for trigger creation / debug
BEGIN
    /*
     This function builds up my_func_args for each trigger function it installs

     In normal mode, my_func_args looks like:
      ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1', 'unique_key_2' ]'::JSON::VARCHAR ]::VARCHAR[]
      This gives the trigger a shortcut for the primary key of TG_RELNAME once it executes,
      As well as provides any additional keys that are a member of any unique_expression of a cache table that are also present on the table

     In rollup mode, my_func_args loops like
      ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1' ]'::JSON::VARCHAR, '{ masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }'::JSON::VARCHAR ]::VARCHAR[]
      This provides the keys used in normal mode, along with the masquerading information,
      In this mode, the first two parameters are overwritten so that the trigger can masquerade update operations
    */
    -- Loop over installed triggers and validate that they are correct
    FOR my_tr_record IN(
                        WITH tt_installed_triggers AS
                        (
                            SELECT t.tgname::VARCHAR AS trigger_name,
                                   array_remove( @extschema@.fn_parse_tgargs( t.tgargs ), 'null' ) AS args,
                                   c.relname::VARCHAR AS table_name,
                                   n.nspname::VARCHAR AS schema_name
                              FROM pg_trigger t
                        INNER JOIN pg_class c
                                ON c.oid = t.tgrelid
                               AND c.relkind = 'r'
                               AND c.relhastriggers IS TRUE
                        INNER JOIN pg_namespace n
                                ON n.oid = c.relnamespace
                             WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request'
                        ),
                        tt_needed_triggers AS
                        (
                            WITH tt_baseline_triggers AS
                            (
                                SELECT DISTINCT ON( tc.schema_name, tc.table_name )
                                       array_agg( DISTINCT ct.table_name ) AS dependent_cts,
                                       tc.schema_name,
                                       tc.table_name,
                                       @extschema@.fn_get_table_pk_col( tc.table_name ) AS primary_arg,
                                       array_agg( DISTINCT tc.column_name ) AS required_columns,
                                       array_agg( DISTINCT a.attname::VARCHAR ) AS available_columns,
                                       nullif( array_remove( array_agg( DISTINCT x ), NULL ), ARRAY[]::VARCHAR[] )::VARCHAR AS sec
                                  FROM @extschema@.tb_cache_table ct
                            INNER JOIN @extschema@.tb_cache_table_trigger_column cttc
                                    ON cttc.cache_table = ct.cache_table
                            INNER JOIN @extschema@.tb_trigger_column tc
                                    ON tc.trigger_column = cttc.trigger_column
                            INNER JOIN pg_class c
                                    ON c.relname::VARCHAR = tc.table_name
                            INNER JOIN pg_namespace n
                                    ON n.oid = c.relnamespace
                                   AND n.nspname::VARCHAR = tc.schema_name
                            INNER JOIN pg_attribute a
                                    ON a.attrelid = c.oid
                                   AND a.attnum > 0
                             LEFT JOIN unnest( ct.unique_expression ) x
                                    ON x = a.attname
                              GROUP BY tc.schema_name,
                                       tc.table_name
                            )
                                SELECT table_name,
                                       schema_name,
                                       array_remove( ARRAY[ primary_arg ] || sec, NULL ) AS args
                                  FROM tt_baseline_triggers
                        )
                            SELECT COALESCE( ttnt.schema_name, ttit.schema_name ) AS schema_name,
                                   COALESCE( ttnt.table_name, ttit.table_name ) AS table_name,
                                   @extschema@.fn_get_table_pk_col( COALESCE( ttnt.table_name, ttit.table_name ) ) AS primary_key,
                                   ttit.args AS old_def,
                                   ttnt.args AS new_def,
                                   CASE WHEN ttit.args != ttnt.args AND ttit.table_name IS NOT NULL
                                        THEN 'UPDATE_TRIGGER'
                                        WHEN ttit.table_name IS NULL
                                        THEN 'CREATE_TRIGGER'
                                        ELSE 'DROP_TRIGGER'
                                         END AS operation
                              FROM tt_needed_triggers ttnt
                        FULL OUTER JOIN tt_installed_triggers ttit
                                ON ttit.schema_name = ttnt.schema_name
                               AND ttit.table_name = ttnt.table_name
                             WHERE ( ttit.args <> ttnt.args OR ttit.args IS NULL )
                       ) LOOP
        /*
            CREATE_TRIGGER: Trigger does not exist, but a new or updated cache_table needs it to be present
            UPDATE_TRIGGER: Trigger exists, but its definition (arguments) need to be updated to match a change in requirements
                            caused by an update to a cache table.
            DELETE_TRIGGER: Trigger exists, but has been deprecated by the removal of a cache table, or a change in a cache table's
                            definition.
        */
        my_additional_keys := NULL::VARCHAR[]; -- Reset to avoid trigger bugs
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            RAISE DEBUG 'Dropping trigger on %.%', my_tr_record.schema_name, my_tr_record.table_name;
            EXECUTE 'DROP TRIGGER IF EXISTS tr_queue_cache_update_request ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        -- Begin creating trigger definition
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            my_func_args[1] := my_tr_record.primary_key;
            RAISE DEBUG 'Performing trigger op % on %.%', my_tr_record.operation, my_tr_record.schema_name, my_tr_record.table_name;
            -- Validate that the table has a primary key constraint. If it does not, abort trigger creation. This corrects an
            -- issue on restore when the primary key constraints are not set when the cache table is loaded at restore-time.
            PERFORM a.attname
               FROM pg_class c
               JOIN pg_constraint cs
                 ON cs.conrelid = c.oid
                AND cs.contype = 'p' --primary key
               JOIN pg_attribute a
                 ON a.attnum = ANY( cs.conkey )
                AND a.attrelid = c.oid
                AND a.attnum > 0
              WHERE c.relname::VARCHAR = my_tr_record.table_name;

            IF NOT FOUND THEN
                RAISE NOTICE 'Table % lacks a primary key constraint, @extschema@ cannot source updates from this table', my_tr_record.table_name;
                CONTINUE;
            END IF;
            IF( my_tr_record.new_def[1] IS NOT NULL ) THEN
                my_func_args[2] := my_tr_record.new_def[2];
                RAISE DEBUG 'Setting secondary argument for %.% trigger to %', my_tr_record.schema_name, my_tr_record.table_name, my_tr_record.new_def[2];
            END IF;
            PERFORM *
               FROM pg_class c
               JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_tr_record.schema_name
              WHERE c.relname::VARCHAR = my_tr_record.table_name
                AND c.relkind = 'r';

            IF FOUND THEN
                 -- Create trigger
                my_query := format(
                            'CREATE TRIGGER tr_queue_cache_update_request'
                         || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                         || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L )',
                            my_func_args[1],
                            my_func_args[2]
                        );
                EXECUTE my_query;
                RAISE DEBUG 'Created trigger on %.%', my_tr_record.schema_name, my_tr_record.table_name;
            ELSE
                RAISE DEBUG 'Creation of trigger on %.% skipped - not valid', my_tr_record.schema_name, my_tr_record.table_name;
            END IF;
        END IF;
    END LOOP;
    /*
        Handles the creation of rollup-mode triggers. These triggers have a third parameter that indicate what table they are
        masquerading updates as
    */
    FOR my_tr_record IN(
            WITH tt_current_trigger_info AS
            (
                SELECT ct.cache_table,
                       COALESCE( bn.nspname::VARCHAR, 'public' ) AS schema_name,
                       bcl.relname::VARCHAR AS table_name,
                       je.value AS value,
                       ba.attname AS foreign_key,
                       (
                            '{"masquerades_for_table":"'
                         || COALESCE( mn.nspname::VARCHAR, 'public' ) || '.' || mcl.relname::VARCHAR
                         || '","fk_column_name":"'
                         || ba.attname
                         || '"}'
                       )::JSON AS rollup_info
                  FROM @extschema@.tb_cache_table ct
    INNER JOIN LATERAL (
                            SELECT ct.cache_table,
                                   value
                              FROM json_array_elements( ct.has_rollup_from_table )
                       ) jae
                    ON jae.cache_table = ct.cache_table
    INNER JOIN LATERAL (
                            SELECT jae.cache_table,
                                   key::VARCHAR,
                                   regexp_replace( value::VARCHAR, '"', '', 'g' ) AS value
                              FROM json_each( jae.value )
                       ) je
                    ON je.cache_table = ct.cache_table
            INNER JOIN pg_class bcl -- base table
                    ON bcl.relkind = 'r'
                   AND bcl.relname::VARCHAR = regexp_replace( je.key, '^.*\.', '' ) -- get table_name
             LEFT JOIN pg_namespace bn
                    ON bn.oid = bcl.relnamespace
                   AND bn.nspname::VARCHAR = regexp_replace( je.key, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_class mcl -- masqueraded table
                    ON mcl.relkind = 'r'
                   AND mcl.relname::VARCHAR = regexp_replace( je.value, '^.*\.', '' ) -- get table_name
             LEFT JOIN pg_namespace mn
                    ON mn.oid = mcl.relnamespace
                   AND mn.nspname::VARCHAR = regexp_replace( je.value, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_attribute ba
                    ON ba.attrelid = bcl.oid
                   AND ba.attnum > 0
            INNER JOIN pg_attribute ma
                    ON ma.attrelid = mcl.oid
                   AND ma.attnum > 0
            INNER JOIN pg_constraint bc
                    ON bc.conrelid = bcl.oid
                   AND bc.conrelid = ba.attrelid
                   AND bc.contype = 'f'
                   AND ba.attnum = ANY( bc.conkey )
                   AND bc.confrelid = mcl.oid
            INNER JOIN pg_constraint mc
                    ON mc.conrelid = mcl.oid
                   AND mc.conrelid = ma.attrelid
                   AND mc.contype = 'p'
                   AND ma.attnum = ANY( mc.conkey )
                 WHERE has_rollup_from_table IS NOT NULL
            ),
            tt_installed_trigger_info AS
            (
                SELECT t.tgname::VARCHAR AS trigger_name,
                       t.tgargs,
                       n.nspname::VARCHAR AS schema_name,
                       c.relname::VARCHAR AS table_name
                  FROM pg_trigger t
            INNER JOIN pg_class c
                    ON c.oid = t.tgrelid
                   AND c.relkind = 'r'
                   AND c.relhastriggers IS TRUE
            INNER JOIN pg_namespace n
                    ON n.oid = c.relnamespace
                 WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request_from_rollup_table'
            ),
            tt_trigger_logic AS
            (
                SELECT COALESCE( ttct.schema_name, ttit.schema_name ) AS schema_name,
                       COALESCE( ttct.table_name, ttit.table_name ) AS table_name,
                       CASE WHEN ttct.table_name IS NULL
                             AND ttit.table_name IS NOT NULL
                            THEN 'DROP_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NULL
                            THEN 'CREATE_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NOT NULL
                             AND rollup_info::VARCHAR IS DISTINCT FROM ( ( @extschema@.fn_parse_tgargs( ttit.tgargs ) )[2] )::JSON::VARCHAR
                            THEN 'UPDATE_TRIGGER'
                            ELSE NULL
                             END AS logic_result,
                       ttct.rollup_info,
                       ttit.trigger_name
                  FROM tt_current_trigger_info ttct
       FULL OUTER JOIN tt_installed_trigger_info ttit
                    ON ttit.table_name = ttct.table_name
            )
                SELECT schema_name,
                       table_name,
                       logic_result AS operation,
                       rollup_info,
                       trigger_name
                  FROM tt_trigger_logic
                 WHERE logic_result IS NOT NULL
                       ) LOOP
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            EXECUTE 'DROP TRIGGER ' || my_tr_record.trigger_name || ' ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            SELECT @extschema@.fn_get_table_pk_col( my_tr_record.table_name )::VARCHAR
              INTO my_func_arg;

            my_func_args[1] := my_func_arg;
            my_func_args[2] := 'null'::JSON::VARCHAR;
            my_func_args[3] := my_tr_record.rollup_info::VARCHAR;

            my_query := format(
                        'CREATE TRIGGER tr_queue_cache_update_request_from_rollup_table'
                     || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                     || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L, %L )',
                         my_func_args[1],
                         my_func_args[2],
                         my_func_args[3]
                    );
            EXECUTE my_query;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_indexes
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_index_name       VARCHAR;
    my_ct_record        RECORD;
    my_index_column     VARCHAR;
    my_index_type       VARCHAR;
    my_unique_columns   VARCHAR;
BEGIN
    /*
        This function handles the contents of index_columns, parsing the JSON and creating indexes
    */
    FOR my_index_name IN(
            SELECT ci.relname::VARCHAR
              FROM pg_class c
        INNER JOIN pg_index i
                ON i.indrelid = c.oid
               AND i.indisunique IS FALSE
        INNER JOIN pg_class ci
                ON ci.oid = i.indexrelid
        INNER JOIN pg_namespace n
                ON n.oid = c.relnamespace
        INNER JOIN @extschema@.tb_cache_table ct
                ON ct.table_name = c.relname::VARCHAR
               AND ct.schema = n.nspname::VARCHAR
               AND ct.cache_table = in_cache_table
                        ) LOOP
        EXECUTE 'DROP INDEX ' || my_index_name;
    END LOOP;

    SELECT unique_expression,
           schema,
           table_name,
           index_columns
      INTO my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    FOR my_index_column, my_index_type IN(
            SELECT key::VARCHAR,
                   value::VARCHAR
              FROM json_each_text( my_ct_record.index_columns )
                                         ) LOOP
        my_index_name := 'ix_' || my_ct_record.table_name || '_' || regexp_replace( my_index_column, '[^[:alnum:]]', '_', 'g' );
        my_index_name := regexp_replace( my_index_name, ',', '_', 'g' );
        IF( lower( my_index_type ) IN( 'gin', 'gist', 'brin', 'spgist', 'hash' ) ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || ' USING ' || my_index_type || '( ' || my_index_column || ' )';
        ELSIF( my_index_type ~* 'btree' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || '( ' || my_index_column || ' )';
        ELSE
            RAISE NOTICE 'Unsupported index type %. Skipping creation', my_index_type;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_analyze_table
(
    in_cache_table  INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_table_name   VARCHAR;
    my_schema_name  VARCHAR;
BEGIN
    -- Performs an ANALYZE on a newly created cache table so that statistics are up-to-date and the
    -- query planner can do what it does best :)
    SELECT table_name,
           schema
      INTO my_table_name,
           my_schema_name
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'ANALYZE ' || my_schema_name || '.' || my_table_name;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table
(
    in_cache_table_name    VARCHAR
)
RETURNS BOOLEAN AS
 $_$
BEGIN
    -- These are transaction level advisory locks, and will be replaced with session-level advisory locks by tblmgr_refresher.pl.
    -- these are simply inplaced to reduce the possibility of a race condition.
    IF NOT pg_try_advisory_xact_lock(
                    '@extschema@.tb_cache_table'::REGCLASS::INTEGER,
                    cache_table
                )
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
    THEN
        RAISE NOTICE 'Aborted attempt to refresh cache table, a refresh is already in progress';
        RETURN FALSE;
    END IF;

    -- Create a special entry in tb_cache_update_request for tblmgr_refresher to read in and begin the full refresh process
    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    created,
                    disable_flush,
                    full_refresh_request
                )
         SELECT schema,
                table_name,
                cache_table,
                now(),
                FALSE,
                TRUE
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name;

    RETURN TRUE;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    -- Handle the deletion of a cache_table by removing trigger dependencies and statistics
    EXECUTE 'DELETE FROM @extschema@.tb_cache_table ctc '
         || '      USING @extschema@.tb_cache_table ct '
         || '      WHERE ctc.inherits_table = ''' || OLD.table_name || '''';

    EXECUTE 'DELETE FROM @extschema@.tb_cache_table_statistic WHERE cache_table = ' || OLD.cache_table;
    EXECUTE 'DELETE FROM @extschema@.tb_cache_table_trigger_column WHERE cache_table = ' || OLD.cache_table;

    PERFORM @extschema@.fn_remove_debug_views();
    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.schema || '.' || OLD.table_name || ' CASCADE';

    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_drop_table
    BEFORE DELETE ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_drop_table();

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table_trigger_wrapper()
RETURNS TRIGGER AS
 $_$
BEGIN
    PERFORM @extschema@.fn_remove_debug_views();
    PERFORM @extschema@.fn_refresh_cache_table( NEW.table_name );
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_handle_table_definition_change
    AFTER INSERT OR UPDATE OF generation_query, group_by_expression, unique_expression
    ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_refresh_cache_table_trigger_wrapper();

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_cache_table
(
    in_cache_table      INTEGER,
    in_scan_fraction    FLOAT DEFAULT 0.1
)
RETURNS BOOLEAN AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_limit        INTEGER;
    my_column_name  VARCHAR;
    my_check        BOOLEAN;
    my_es_proto     VARCHAR[]; -- arrays of columns used to make record object for set comparison
    my_cs_proto     VARCHAR[];
    my_query        TEXT;
BEGIN
    /*
        This function validates the contents of a cache_table against its definition.

        Due to the asyc nature of tblmgr, there cannot be any outstanding cache_update_requests when the transaction is
        begun. It is fine if they are created later because xmin/xmax will maintain the appropriate tuple visibilities
        during the validation transaction. This check, though annoying, minimizes the possibility of a race condition and false negatives
    */
    PERFORM *
       FROM @extschema@.tb_cache_update_request;

    IF FOUND THEN
        RAISE NOTICE 'Test aborted, unserviced cache_update_requests found';
        RETURN FALSE;
    END IF;

    SELECT table_name,
           schema,
           unique_expression,
           generation_query,
           COALESCE( 'GROUP BY ' || array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR ) AS group_by,
           table_columns
      INTO STRICT my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'SELECT COUNT(*) FROM ' || my_ct_record.schema || '.' || my_ct_record.table_name
       INTO my_limit;

    my_limit := ( my_limit * in_scan_fraction )::INTEGER;
    RAISE NOTICE 'Scanning in % rows from %.%', my_limit, my_ct_record.schema, my_ct_record.table_name;

    EXECUTE
    'CREATE TEMP TABLE tt_current_set AS '
 || '( '
 || my_ct_record.generation_query || ' '
 || my_ct_record.group_by || ' '
 || ' LIMIT ' || my_limit
 || ') ';
    EXECUTE 'CREATE INDEX ix_temp_current_unique ON tt_current_set( ' || array_to_string( my_ct_record.unique_expression, ',' ) || ' )';
    ANALYZE tt_current_set;

    PERFORM *
       FROM tt_current_set;

    -- If both sets are empty then they are equal
    IF NOT FOUND THEN
        DROP TABLE IF EXISTS tt_current_set;

        EXECUTE 'SELECT * FROM ' || my_ct_record.table_name
           INTO my_limit;

        IF my_limit = 0 THEN
            RETURN TRUE;
        END IF;

        RETURN FALSE;
    END IF;

    my_query :=
    'CREATE TEMP TABLE tt_existing_set AS '
 || '( '
 || '    SELECT ct.* '
 || '      FROM ONLY ' || my_ct_record.schema || '.' || my_ct_record.table_name || ' ct ';

    IF( in_scan_fraction < 1.0 ) THEN
        RAISE NOTICE 'Performing partial scan, warning: missing or extra tuples cannot be validated.';
        my_query := my_query
     || 'INNER JOIN tt_current_set ttcs '
     || '     USING( ' || array_to_string( my_ct_record.unique_expression, ', ' ) || ' ) ';
    ELSE
        RAISE NOTICE 'Performing full set check';
    END IF;

    my_query := my_query || ') ';

    EXECUTE my_query;
    EXECUTE 'CREATE INDEX ix_temp_existing_unique ON tt_existing_set( ' || array_to_string( my_ct_record.unique_expression, ',' ) || ' )';
    ANALYZE tt_existing_set;

    PERFORM *
       FROM tt_existing_set;

    IF NOT FOUND THEN
        DROP TABLE IF EXISTS tt_current_set;
        DROP TABLE IF EXISTS tt_existing_set;
        RETURN FALSE;
    END IF;

    -- Build join clause for full set validation
    FOR my_column_name IN(
            SELECT a.attname::VARCHAR
              FROM pg_attribute a
        INNER JOIN pg_class c
                ON c.oid = a.attrelid
               AND c.relname::VARCHAR = my_ct_record.table_name
               AND c.relkind = 'r'
             WHERE a.attnum > 0
                         ) LOOP
        my_es_proto := array_append(
            my_es_proto,
            ( 'ttes.' || my_column_name )::VARCHAR
        );

        my_cs_proto := array_append(
            my_cs_proto,
            ( 'ttcs.' || my_column_name )::VARCHAR
        );
    END LOOP;

    -- Check for set disparity
    EXECUTE
    'CREATE TEMP TABLE tt_validation AS '
 || '( '
 || '          SELECT ttcs.* '
 || '            FROM tt_existing_set ttes '
 || ' FULL OUTER JOIN tt_current_set ttcs '
 || '              ON ( ' || array_to_string( my_cs_proto, ', ' ) || ' )::VARCHAR = ( ' || array_to_string( my_es_proto, ', ' ) || ' )::VARCHAR '
 || '           WHERE ttes.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttes.' ) || ' IS NULL '
 || '              OR ttcs.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttcs.' ) || ' IS NULL '
 || ') ';
    PERFORM *
       FROM tt_validation;

    IF FOUND THEN
        DROP TABLE IF EXISTS tt_validation;
        DROP TABLE IF EXISTS tt_existing_set;
        DROP TABLE IF EXISTS tt_current_set;
        RETURN FALSE;
    END IF;

    DROP TABLE IF EXISTS tt_validation;
    DROP TABLE IF EXISTS tt_existing_set;
    DROP TABLE IF EXISTS tt_current_set;

    RETURN TRUE;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_wait_for_full_refresh()
RETURNS VOID AS
 $_$
BEGIN
    -- plpgsql does not have LISTEN / NOFIFY impletemented properly such that LISTEN blocks. We must manually poll the refresh process.
    -- In this instance we are waiting for the advisory lock to be lifted by the refresher process
    LOOP
        PERFORM pid
           FROM pg_locks l
          WHERE locktype = 'advisory'
            AND mode = 'ExclusiveLock'
            AND classid = 'tblmgr.tb_cache_table'::REGCLASS::OID;

        IF NOT FOUND THEN
            RETURN;
        END IF;

        PERFORM pg_sleep( 1.0 ); -- Wait 1 second
    END LOOP;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_get_cache_table_statistics
(
    in_cache_tables VARCHAR[] DEFAULT NULL::VARCHAR[]
)
RETURNS TABLE
(
    "CT Name"                VARCHAR,
    "FR Count"               BIGINT,
    "Avg FR Time (ms)"       VARCHAR,
    "Avg FR Ind OH (ms)"     VARCHAR,
    "Avg FR Rows"            VARCHAR,
    "PR Count"               BIGINT,
    "Avg PR Time (ms)"       VARCHAR,
    "Avg PR Keys"            VARCHAR,
    "Avg Queue Peek (ms)"    VARCHAR,
    "Avg JSON OH (ms)"       VARCHAR,
    "AVG TT OH (ms)"         VARCHAR,
    "AVG UPD (ms)"           VARCHAR,
    "AVG DEL (ms)"           VARCHAR,
    "AVG INS (ms)"           VARCHAR
) AS
 $_$
BEGIN
    IF( in_cache_tables IS NULL ) THEN
        SELECT array_agg( table_name )
          INTO in_cache_tables
          FROM @extschema@.tb_cache_table;
    END IF;

    RETURN QUERY
    WITH tt_full_refresh_statistics AS
    (
        SELECT ct.cache_table,
               COALESCE( SUM( ctsf.full_refresh_rows ), 0 ) AS rows_refreshed,
               COALESCE( SUM( ctsf.full_refresh_duration ), '0 seconds'::INTERVAL ) AS total_duration,
               SUM( ( ctsf.additional_stats->>'index_creation' )::FLOAT * '1 seconds'::INTERVAL ) AS index_creation_overhead,
               COUNT( ctsf.* ) AS full_refresh_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsf
            ON ctsf.cache_table = ct.cache_table
           AND ctsf.partial_refresh_keys = 0
           AND ctsf.partial_refresh_duration IS NULL
           AND ctsf.full_refresh_rows > 0
           AND ctsf.full_refresh_duration IS NOT NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    ),
    tt_partial_refresh_statistics AS
    (
        SELECT ct.cache_table,
               COALESCE( SUM( ctsp.partial_refresh_keys ), 0 ) AS updated_keys,
               COALESCE( SUM( ctsp.partial_refresh_duration ), 0 * '1 second'::INTERVAL ) AS total_duration,
               COALESCE( SUM( ( ctsp.additional_stats->>'peek_time' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS queue_peek_overhead,
               COUNT( ctsp.* ) AS partial_refresh_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsp
            ON ctsp.cache_table = ct.cache_table
           AND ctsp.partial_refresh_keys > 0
           AND ctsp.partial_refresh_duration IS NOT NULL
           AND ctsp.full_refresh_rows = 0
           AND ctsp.full_refresh_duration IS NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    ),
    tt_partial_refresh_details AS
    (
        SELECT ct.cache_table,
               COALESCE( SUM( ( ctsd.additional_stats->>'additional_key_count' )::INTEGER ), 0 ) AS additional_key_count,
               COALESCE( SUM( ( ctsd.additional_stats->>'primary_key_count' )::INTEGER ), 0 ) AS primary_key_count,
               COALESCE( SUM( ( ctsd.additional_stats->>'table_count' )::INTEGER ), 0 ) AS table_count,
               COALESCE( SUM( ( ctsd.additional_stats->>'json_parse' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS json_parse,
               COALESCE( SUM( ( ctsd.additional_stats->>'temp_table' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS temp_table,
               COALESCE( SUM( ( ctsd.additional_stats->>'update'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_update,
               COALESCE( SUM( ( ctsd.additional_stats->>'delete'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_delete,
               COALESCE( SUM( ( ctsd.additional_stats->>'insert'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_insert,
               COUNT( ctsd.* ) AS detail_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsd
            ON ctsd.cache_table = ct.cache_table
           AND ctsd.partial_refresh_keys = 0
           AND ctsd.partial_refresh_duration IS NULL
           AND ctsd.full_refresh_rows = 0
           AND ctsd.full_refresh_duration IS NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    )
        SELECT ct.table_name AS "CT Name",
               ttfr.full_refresh_count AS "FR Count",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttfr.total_duration )::FLOAT / ttfr.full_refresh_count ), '999999999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg FR Time (ms)",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttfr.index_creation_overhead )::FLOAT / ttfr.full_refresh_count ), '99999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg FR Ind OH (ms)",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( ttfr.rows_refreshed::FLOAT / ttfr.full_refresh_count, '99999999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg FR Rows",
               ttpr.partial_refresh_count AS "PR Count",
               CASE WHEN ttpr.partial_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttpr.total_duration )::FLOAT / ttpr.partial_refresh_count ), '9999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg PR Time (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( ( ttprd.additional_key_count + ttprd.primary_key_count )::FLOAT / ttprd.detail_count, '999999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg PR Keys",
               CASE WHEN ttpr.partial_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttpr.queue_peek_overhead )::FLOAT / ttpr.partial_refresh_count ), '999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg Queue Peek (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.json_parse )::FLOAT / ttprd.detail_count ), '99D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg JSON OH (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.temp_table )::FLOAT / ttprd.detail_count ), '9999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG TT OH (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_update )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG UPD (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_delete )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG DEL (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_insert )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG INS (ms)"
          FROM @extschema@.tb_cache_table ct
    INNER JOIN tt_full_refresh_statistics ttfr
            ON ttfr.cache_table = ct.cache_table
    INNER JOIN tt_partial_refresh_statistics ttpr
            ON ttpr.cache_table = ct.cache_table
    INNER JOIN tt_partial_refresh_details ttprd
            ON ttprd.cache_table = ct.cache_table
         WHERE ct.table_name = ANY( in_cache_tables );

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

GRANT USAGE ON SCHEMA @extschema@ TO PUBLIC;
-- TODO: Better permissions to avoid letting just anyone change tblmgr config
GRANT SELECT, UPDATE, DELETE, INSERT ON ALL TABLES IN SCHEMA @extschema@ TO PUBLIC;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA @extschema@ TO PUBLIC;


SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_cache_table', '' );
--SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_trigger_column', '' );
--SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_cache_table_trigger_column', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_cache_table', '' );
--SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_trigger_column', '' );
--SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_cache_table_trigger_column', '' );

CREATE OR REPLACE FUNCTION @extschema@.fn_create_debug_snapshot()
RETURNS VOID AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_debug_view   VARCHAR;
BEGIN
    FOR my_ct_record IN(
                            SELECT table_name,
                                   unique_expression,
                                   schema,
                                   COALESCE( ' GROUP BY ' || array_to_string( group_by_expression, ',' ), '' )::VARCHAR AS group_by,
                                   generation_query
                              FROM @extschema@.tb_cache_table
                       ) LOOP
        /*
            Determine if current-state and desired-state debug views exist

            If they do not: create them
            If they do: refresh them
        */
        PERFORM *
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE relkind = 'm'
            AND relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_current';

        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_current';
        IF NOT FOUND THEN
            EXECUTE 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                 || '( '
                 || '    SELECT * '
                 || '      FROM ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || ') ';

            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_ct_record.table_name || '_debug_current ON ' || my_debug_view
                 || '('
                 || array_to_string( my_ct_record.unique_expression, ',' )
                 || ')';
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;

        PERFORM *
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE relkind = 'm'
            AND relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_desired';

        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_desired';
        IF NOT FOUND THEN
            EXECUTE 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                 || '( '
                 || my_ct_record.generation_query || ' '
                 || my_ct_record.group_by
                 || ') ';

            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_ct_record.table_name || '_debug_desired ON ' || my_debug_view
                 || '('
                 || array_to_string( my_ct_record.unique_expression, ',' )
                 || ')';
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_generate_debug_diff()
RETURNS VOID AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_debug_view   VARCHAR;
    my_column_name  VARCHAR;
    my_cs_proto     VARCHAR[];
    my_ds_proto     VARCHAR[];
    my_query        VARCHAR;
BEGIN
    FOR my_ct_record IN(
                           SELECT table_name,
                                  unique_expression
                             FROM @extschema@.tb_cache_table
                       ) LOOP
        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_diff';
        PERFORM c.relname
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE c.relkind = 'm'
            AND c.relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_diff';

        IF NOT FOUND THEN
            SELECT array_agg( 'ttds.' || a.attname::VARCHAR ),
                   array_agg( 'ttcs.' || a.attname::VARCHAR )
              INTO my_ds_proto,
                   my_cs_proto
              FROM pg_attribute a
        INNER JOIN pg_class c
                ON c.oid = a.attrelid
               AND c.relname::VARCHAR = my_ct_record.table_name
               AND c.relkind = 'r'
             WHERE a.attnum > 0;

            my_query := 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                     || '('
                     || '          SELECT ';

            FOR my_column_name IN( SELECT unnest( my_ct_record.unique_expression ) ) LOOP
                my_query := my_query || ' ttcs.' || my_column_name || ' AS current_' || my_column_name || ','
                         || ' ttds.' || my_column_name || ' AS desired_' || my_column_name || ',';
            END LOOP;

            my_query := regexp_replace( my_query, ',$', '' )
                     || '            FROM @extschema@.vw_' || my_ct_record.table_name || '_current ttcs '
                     || ' FULL OUTER JOIN @extschema@.vw_' || my_ct_record.table_name || '_desired ttds '
                     || '              ON ( ' || array_to_string( my_cs_proto, ', ' ) || ' )::VARCHAR = ( ' || array_to_string( my_ds_proto, ', ' ) || ' )::VARCHAR '
                     || '           WHERE ( ttcs.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttcs.' ) || ' IS NULL ) '
                     || '              OR ( ttds.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttds.' ) || ' IS NULL ) '
                     || ')';

            EXECUTE my_query;
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;
    END LOOP;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_remove_debug_views()
RETURNS VOID AS
 $_$
DECLARE
    my_query VARCHAR;
BEGIN
    SELECT 'DROP MATERIALIZED VIEW ' || array_to_string( array_agg( n.nspname::VARCHAR || '.' || c.relname::VARCHAR ), '; DROP MATERIALIZED VIEW ' )
      INTO my_query
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = '@extschema@'
      JOIN @extschema@.tb_cache_table ct
        ON 'vw_' || ct.table_name || '_diff' = c.relname
     WHERE c.relkind = 'm';

    IF( my_query IS NOT NULL ) THEN
        EXECUTE my_query;
    END IF;

    SELECT 'DROP MATERIALIZED VIEW ' || array_to_string( array_agg( n.nspname::VARCHAR || '.' || c.relname::VARCHAR ), '; DROP MATERIALIZED VIEW ' )
      INTO my_query
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = '@extschema@'
      JOIN @extschema@.tb_cache_table ct
        ON 'vw_' || ct.table_name || '_desired' = c.relname
        OR 'vw_' || ct.table_name || '_current' = c.relname
     WHERE c.relkind = 'm';

     IF( my_query IS NULL ) THEN
        RETURN;
     END IF;

     EXECUTE my_query;
     RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_replace_cache_table
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_drop_statement   VARCHAR;
    my_create_statement VARCHAR;
    my_cache_table_name VARCHAR;
BEGIN
    CREATE TEMP TABLE tt_dependent_objects
    (
        drop_statement   TEXT,
        create_statement TEXT,
        object_name      VARCHAR,
        is_base_obj      BOOLEAN,
        rank             INTEGER
    ) ON COMMIT DROP;

    WITH RECURSIVE tt_viewdefs AS
    (
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN pg_namespace sns
            ON sns.oid = sc.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON sns.nspname::VARCHAR = COALESCE( ct.schema, 'public' )
           AND sc.relname::VARCHAR = ct.table_name
           AND ct.cache_table = in_cache_table
         UNION
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               tt.rank + 1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN tt_viewdefs tt
            ON tt.dependent_oid = sc.oid
         WHERE sc.oid IS DISTINCT FROM dc.oid
    ),
    tt_def_prep AS
    (
        SELECT COALESCE( ns.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object_name,
               CASE WHEN c.relkind = 'm'
                    THEN 'MATERIALIZED'
                    ELSE ''
                     END AS view_type,
               regexp_replace( pg_get_viewdef( c.oid, TRUE ), ';\s*$', '' ) AS definition,
               tt.rank
          FROM tt_viewdefs tt
    INNER JOIN pg_class c
            ON c.oid = tt.dependent_oid
    INNER JOIN pg_namespace ns
            ON ns.oid = c.relnamespace
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'DROP ' || view_type || ' VIEW ' || object_name AS drop_statement,
                'CREATE ' || view_type || ' VIEW ' || object_name || ' AS ( ' || definition || ')' AS create_statement,
                object_name,
                TRUE,
                rank
           FROM tt_def_prep;

    WITH tt_fk_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
         WHERE co.contype = 'f'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype = 'f'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_fk_constraints tt;

    WITH tt_check_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
         WHERE co.contype != 'f'
           AND co.contype != 'p'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype != 'f'
           AND co.contype != 'p'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_check_constraints tt;

    --Capture triggers
    WITH tt_triggers AS
    (
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.table_name = c.relname::VARCHAR
           AND ct.schema = n.nspname::VARCHAR
           AND ct.cache_table = in_cache_table
         UNION
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'DROP TRIGGER ' || trigger_name || ' ON ' || object AS drop_statement,
                definition AS create_statement,
                trigger_name,
                FALSE,
                rank
           FROM tt_triggers;

    -- Capture indexes on dependent objects
    WITH tt_indexes AS
    (
        SELECT pg_get_indexdef( ci.oid ) AS create_statement,
               'DROP INDEX ' || ci.relname::VARCHAR AS drop_statement,
               ci.relname::VARCHAR AS object_name,
               tt.rank + 1 AS rank
          FROM pg_index i
    INNER JOIN pg_class ci
            ON ci.oid = i.indexrelid
    INNER JOIN pg_class c
            ON c.oid = i.indrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT drop_statement,
                create_statement,
                object_name,
                FALSE,
                rank
           FROM tt_indexes;

    FOR my_drop_statement IN(
        SELECT drop_statement
          FROM tt_dependent_objects
      ORDER BY rank DESC
    ) LOOP
        EXECUTE my_drop_statement;
    END LOOP;

    SELECT table_name
      INTO my_cache_table_name
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'DROP TABLE IF EXISTS ' || my_cache_table_name;
    EXECUTE 'ALTER TABLE ' || my_cache_table_name || '_temp RENAME TO ' || my_cache_table_name;

    FOR my_create_statement IN(
        SELECT create_Statement
          FROM tt_dependent_objects
      ORDER BY rank ASC
    ) LOOP
        EXECUTE my_create_statement;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql' VOLATILE PARALLEL UNSAFE;
