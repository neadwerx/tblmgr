CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record               RECORD;
    my_table_name           VARCHAR;
    my_pk_col               VARCHAR;
    my_pk_val               INTEGER;
    my_rollup_info          JSON;
    my_ct_record            RECORD; -- Cache table record
    my_tl_record            RECORD; -- Top level record (JSON table_columns)
    my_ml_record            RECORD; -- Mid level record (JSON table_columns)
    my_ll_record            RECORD; -- Low level record (JSON table_columns)
    my_additional_keys      VARCHAR[];
    my_additional_key_info  VARCHAR;
    my_key_value            VARCHAR;
    my_fk_val               INTEGER;
    my_schema_name          VARCHAR;
BEGIN
    /*
        Expects the following:
            JSON table_columns to be in the following format:
                schema_name: {
                    table_name: {
                        table_alias_used_in_query: {
                            real_column_name:output_column_name(aliased)
                        }
                    },
                    table_name_2: {
                        table_alias_used_in_query: {
                            real_column_name:output_column_name(aliased)
                        }
                    }
                }


            Input rollup_info JSON in the following format:

            { masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }
    */
    -- Move psuedorecords into common record variable
    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        my_record := OLD;
    END IF;

    -- check enabled GUC, exit early if triggers are disabled
    IF( @extschema@.fn_get_config( 'enable_triggers' ) = '0' ) THEN
        RETURN my_record;
    END IF;

    -- Parse args
    SELECT c.relname::VARCHAR,
           n.nspname::VARCHAR
      INTO my_table_name,
           my_schema_name
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.oid = c.relnamespace
     WHERE c.oid = TG_RELID;

    my_pk_col     := TG_ARGV[0]::VARCHAR;


    -- Handle conversion of 2nd parameter if it's null ( could be the case if there is no intersection between
    -- @extschema@.tb_cache_table.unique_expression and the keys of table_columns
    IF( TG_ARGV[1]::VARCHAR ~* 'null' ) THEN
        my_additional_keys := NULL::VARCHAR[];
    ELSE
        my_additional_keys := TG_ARGV[1]::VARCHAR[];
    END IF;

    -- presence of a third parameter indicates this trigger function is operating in rollup mode.
    -- in rollup mode, the trigger masquerades updates as if they occurred on another table
    IF( TG_NARGS = 3 ) THEN
        my_rollup_info     := TG_ARGV[2]::JSON;
    ELSIF( TG_NARGS > 3 ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid call';
        RETURN my_record;
    END IF;

    -- Sanity check, this prevents transaction stopping errors that occur during restores if pg_depends
    -- is populated incorrectly during extension installation
    IF( my_pk_col IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid pk parameter';
        RETURN my_record;
    END IF;

    -- Get the value of the primary key column for the tuple that was modified by DML
    EXECUTE 'SELECT $1.' || my_pk_col
       INTO my_pk_val
      USING my_record;

    IF( my_pk_val IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request - could not determine pk value';
        RETURN my_record;
    END IF;

     -- Look for the intersection of a column in this table with the unique_expression on the target cache table
     -- This helps speed lookups as a unique index ( and possible a BTREE index on the base table FK) is used.
    IF( my_additional_keys IS NOT NULL ) THEN
        my_additional_key_info := '{';

        FOR my_ll_record IN( SELECT unnest( my_additional_keys )::VARCHAR AS key ) LOOP
            EXECUTE 'SELECT $1.' || my_ll_record.key || '::VARCHAR'
               INTO my_key_value
              USING my_record;

            my_additional_key_info := my_additional_key_info || '"' || my_ll_record.key || '":"' || my_key_value || '",';
        END LOOP;

        my_additional_key_info := rtrim( my_additional_key_info, ',' ) ||'}';
    END IF;

    -- Detect alternate (rollup) mode of operation, rewriting the variables as needed
    IF( my_rollup_info IS NOT NULL ) THEN
        FOR my_ll_record IN(
                SELECT key::VARCHAR,
                       value::VARCHAR
                  FROM json_each( my_rollup_info )
                           ) LOOP
            IF( my_ll_record.key = 'fk_column_name' ) THEN
                EXECUTE 'SELECT $1.' || my_ll_record.value
                   INTO my_fk_val
                  USING my_record;
            ELSIF( my_ll_record.key = 'masquerades_for_table' ) THEN
                my_table_name  := regexp_replace(
                                      regexp_replace( my_ll_record.value, '^.*\.', '' ),
                                      '"',
                                      '',
                                      'g'
                                  );
                my_schema_name := regexp_replace(
                                      regexp_replace( my_ll_record.value, '\..*$', '' ),
                                      '"',
                                      '',
                                      'g'
                                  );

                IF( my_schema_name = my_table_name ) THEN
                    my_schema_name := 'public';
                END IF;
            END IF;
        END LOOP;

        IF( my_table_name = TG_RELNAME ) THEN --rewrite failed
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because of invalid JSON parameter';
            RETURN my_record;
        END IF;

        IF( my_fk_val IS NULL ) THEN
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because fk column is incorrect or NULL';
        END IF;

        my_pk_val     := my_fk_val;
        my_table_name := regexp_replace( my_table_name, '"', '', 'g' ); -- Fix quotes
    END IF;

    -- Now that everything is buttoned up, issue a cache_update_request. This will cascade into a NOTIFY that will let
    -- tblmgr.pl know the queue is not empty.
    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    key_info
                )
         VALUES
                (
                    my_schema_name,
                    my_table_name,
                    my_pk_val,
                    my_additional_key_info::JSON
                );

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_get_config
(
    in_config_name  VARCHAR
)
RETURNS TEXT AS
 $_$
DECLARE
    my_config_value TEXT;
BEGIN
    RETURN NULLIF( current_setting( '@extschema@.' || in_config_name, TRUE ), '' );
END
 $_$
    LANGUAGE 'plpgsql';
