DO
 $_$
DECLARE
    my_version INTEGER[];
BEGIN
    my_version := regexp_matches( version(), 'PostgreSQL (\d)+\.(\d+)\.(\d+)' );

    IF my_version < ARRAY[9,3,1]::INTEGER[] THEN
        RAISE EXCEPTION 'Tblmgr requires version 9.3.1 or greater';
    END IF;

    IF( quote_ident( current_database() ) = 'postgres' ) THEN
        RAISE EXCEPTION 'Cannot install tblmgr on the postgres database';
    END IF;

    EXECUTE 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.enable_retry = ''1''';
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_get_config
(
    in_config_name  VARCHAR
)
RETURNS TEXT AS
 $_$
DECLARE
    my_config_value TEXT;
BEGIN
    RETURN NULLIF( current_setting( '@extschema@.' || in_config_name ), '' );
EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION @extschema@.fn_enable_query_retry()
RETURNS VOID AS
 $_$
BEGIN
    EXECUTE 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.enable_retry = ''1''';
    RAISE NOTICE 'GUC updated. Please issue a SIGHUP to the tblmgr daemon';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_query_retry()
RETURNS VOID AS
 $_$
BEGIN
    EXECUTE 'ALTER DATABASE ' || quote_ident( current_database() ) || ' SET @extschema@.enable_retry = ''0''';
    RAISE NOTICE 'GUC updated. Please issue a SIGHUP to the tblmgr daemon';
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE INDEX ix_cache_table_statistic_cache_table ON @extschema@.tb_cache_table_statistic( cache_table );
CREATE OR REPLACE FUNCTION @extschema@.fn_wait_for_full_refresh()
RETURNS VOID AS
 $_$
BEGIN
    -- plpgsql does not have LISTEN / NOFIFY impletemented properly such that LISTEN blocks. We must manually poll the refresh process.
    -- In this instance we are waiting for the advisory lock to be lifted by the refresher process
    LOOP
        PERFORM pid
           FROM pg_locks l
          WHERE locktype = 'advisory'
            AND mode = 'ExclusiveLock'
            AND classid = 'tblmgr.tb_cache_table'::REGCLASS::OID;

        IF NOT FOUND THEN
            RETURN;
        END IF;
        
        PERFORM pg_sleep( 1.0 ); -- Wait 1 second
    END LOOP;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_get_cache_table_statistics
(
    in_cache_tables VARCHAR[] DEFAULT NULL::VARCHAR[]
)
RETURNS TABLE
(
    "CT Name"                VARCHAR,
    "FR Count"               BIGINT,
    "Avg FR Time (ms)"       VARCHAR,
    "Avg FR Ind OH (ms)"     VARCHAR,
    "Avg FR Rows"            VARCHAR,
    "PR Count"               BIGINT,
    "Avg PR Time (ms)"       VARCHAR,
    "Avg PR Keys"            VARCHAR,
    "Avg Queue Peek (ms)"    VARCHAR,
    "Avg JSON OH (ms)"       VARCHAR,
    "AVG TT OH (ms)"         VARCHAR,
    "AVG UPD (ms)"           VARCHAR,
    "AVG DEL (ms)"           VARCHAR,
    "AVG INS (ms)"           VARCHAR
) AS
 $_$
BEGIN
    IF( in_cache_tables IS NULL ) THEN
        SELECT array_agg( table_name )
          INTO in_cache_tables
          FROM @extschema@.tb_cache_table;
    END IF;

    RETURN QUERY
    WITH tt_full_refresh_statistics AS
    (
        SELECT ct.cache_table,
               COALESCE( SUM( ctsf.full_refresh_rows ), 0 ) AS rows_refreshed,
               COALESCE( SUM( ctsf.full_refresh_duration ), '0 seconds'::INTERVAL ) AS total_duration,
               SUM( ( ctsf.additional_stats->>'index_creation' )::FLOAT * '1 seconds'::INTERVAL ) AS index_creation_overhead,
               COUNT( ctsf.* ) AS full_refresh_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsf
            ON ctsf.cache_table = ct.cache_table
           AND ctsf.partial_refresh_keys = 0
           AND ctsf.partial_refresh_duration IS NULL
           AND ctsf.full_refresh_rows > 0
           AND ctsf.full_refresh_duration IS NOT NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    ),
    tt_partial_refresh_statistics AS
    (
        SELECT ct.cache_table,
               COALESCE( SUM( ctsp.partial_refresh_keys ), 0 ) AS updated_keys,
               COALESCE( SUM( ctsp.partial_refresh_duration ), 0 * '1 second'::INTERVAL ) AS total_duration,
               COALESCE( SUM( ( ctsp.additional_stats->>'peek_time' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS queue_peek_overhead, 
               COUNT( ctsp.* ) AS partial_refresh_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsp
            ON ctsp.cache_table = ct.cache_table
           AND ctsp.partial_refresh_keys > 0 
           AND ctsp.partial_refresh_duration IS NOT NULL
           AND ctsp.full_refresh_rows = 0 
           AND ctsp.full_refresh_duration IS NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    ),
    tt_partial_refresh_details AS
    (   
        SELECT ct.cache_table,
               COALESCE( SUM( ( ctsd.additional_stats->>'additional_key_count' )::INTEGER ), 0 ) AS additional_key_count,
               COALESCE( SUM( ( ctsd.additional_stats->>'primary_key_count' )::INTEGER ), 0 ) AS primary_key_count,
               COALESCE( SUM( ( ctsd.additional_stats->>'table_count' )::INTEGER ), 0 ) AS table_count, 
               COALESCE( SUM( ( ctsd.additional_stats->>'json_parse' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS json_parse,         
               COALESCE( SUM( ( ctsd.additional_stats->>'temp_table' )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS temp_table,         
               COALESCE( SUM( ( ctsd.additional_stats->>'update'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_update,     
               COALESCE( SUM( ( ctsd.additional_stats->>'delete'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_delete,       
               COALESCE( SUM( ( ctsd.additional_stats->>'insert'     )::FLOAT * '1 second'::INTERVAL ), '0 seconds'::INTERVAL ) AS op_insert,      
               COUNT( ctsd.* ) AS detail_count
          FROM @extschema@.tb_cache_table ct
     LEFT JOIN @extschema@.tb_cache_table_statistic ctsd
            ON ctsd.cache_table = ct.cache_table
           AND ctsd.partial_refresh_keys = 0
           AND ctsd.partial_refresh_duration IS NULL
           AND ctsd.full_refresh_rows = 0 
           AND ctsd.full_refresh_duration IS NULL
      GROUP BY ct.cache_table
      ORDER BY ct.cache_table
    )
        SELECT ct.table_name AS "CT Name",
               ttfr.full_refresh_count AS "FR Count",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttfr.total_duration )::FLOAT / ttfr.full_refresh_count ), '999999999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg FR Time (ms)",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttfr.index_creation_overhead )::FLOAT / ttfr.full_refresh_count ), '99999D99' )         
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg FR Ind OH (ms)",
               CASE WHEN ttfr.full_refresh_count > 0
                    THEN to_char( ttfr.rows_refreshed::FLOAT / ttfr.full_refresh_count, '99999999D9' )
                    ELSE NULL::FLOAT::VARCHAR 
                     END AS "Avg FR Rows",
               ttpr.partial_refresh_count AS "PR Count",
               CASE WHEN ttpr.partial_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttpr.total_duration )::FLOAT / ttpr.partial_refresh_count ), '9999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg PR Time (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( ( ttprd.additional_key_count + ttprd.primary_key_count )::FLOAT / ttprd.detail_count, '999999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg PR Keys",
               CASE WHEN ttpr.partial_refresh_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttpr.queue_peek_overhead )::FLOAT / ttpr.partial_refresh_count ), '999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg Queue Peek (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.json_parse )::FLOAT / ttprd.detail_count ), '99D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "Avg JSON OH (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.temp_table )::FLOAT / ttprd.detail_count ), '9999D99' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG TT OH (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_update )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG UPD (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_delete )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG DEL (ms)",
               CASE WHEN ttprd.detail_count > 0
                    THEN to_char( 1000 * ( EXTRACT( epoch FROM ttprd.op_insert )::FLOAT / ttprd.detail_count ), '9999D9' )
                    ELSE NULL::FLOAT::VARCHAR
                     END AS "AVG INS (ms)"
          FROM @extschema@.tb_cache_table ct
    INNER JOIN tt_full_refresh_statistics ttfr
            ON ttfr.cache_table = ct.cache_table
    INNER JOIN tt_partial_refresh_statistics ttpr
            ON ttpr.cache_table = ct.cache_table
    INNER JOIN tt_partial_refresh_details ttprd
            ON ttprd.cache_table = ct.cache_table
         WHERE ct.table_name = ANY( in_cache_tables );

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';
CREATE OR REPLACE FUNCTION @extschema@.fn_create_debug_snapshot()
RETURNS VOID AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_debug_view   VARCHAR;
BEGIN
    FOR my_ct_record IN(
                            SELECT table_name,
                                   unique_expression,
                                   schema,
                                   COALESCE( ' GROUP BY ' || array_to_string( group_by_expression, ',' ), '' )::VARCHAR AS group_by,
                                   generation_query
                              FROM @extschema@.tb_cache_table
                       ) LOOP
        /*
            Determine if current-state and desired-state debug views exist

            If they do not: create them
            If they do: refresh them 
        */
        PERFORM *
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE relkind = 'm'
            AND relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_current';
    
        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_current';
        IF NOT FOUND THEN
            EXECUTE 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                 || '( '
                 || '    SELECT * '
                 || '      FROM ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || ') ';
            
            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_ct_record.table_name || '_debug_current ON ' || my_debug_view
                 || '('
                 || array_to_string( my_ct_record.unique_expression, ',' )
                 || ')';
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;

        PERFORM *
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE relkind = 'm'
            AND relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_desired';
    
        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_desired';
        IF NOT FOUND THEN
            EXECUTE 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                 || '( '
                 || my_ct_record.generation_query || ' '
                 || my_ct_record.group_by
                 || ') ';

            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_ct_record.table_name || '_debug_desired ON ' || my_debug_view
                 || '('
                 || array_to_string( my_ct_record.unique_expression, ',' )
                 || ')';
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_generate_debug_diff()
RETURNS VOID AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_debug_view   VARCHAR;
    my_column_name  VARCHAR;
    my_cs_proto     VARCHAR[];
    my_ds_proto     VARCHAR[];
    my_query        VARCHAR;
BEGIN
    FOR my_ct_record IN(
                           SELECT table_name,
                                  unique_expression
                             FROM @extschema@.tb_cache_table
                       ) LOOP
        my_debug_view := '@extschema@.vw_' || my_ct_record.table_name || '_diff';
        PERFORM c.relname
           FROM pg_class c
           JOIN pg_namespace n
             ON n.oid = c.relnamespace
            AND n.nspname::VARCHAR = '@extschema@'
          WHERE c.relkind = 'm'
            AND c.relname::VARCHAR = 'vw_' || my_ct_record.table_name || '_diff';

        IF NOT FOUND THEN
            SELECT array_agg( 'ttds.' || a.attname::VARCHAR ),
                   array_agg( 'ttcs.' || a.attname::VARCHAR )
              INTO my_ds_proto,
                   my_cs_proto
              FROM pg_attribute a
        INNER JOIN pg_class c
                ON c.oid = a.attrelid
               AND c.relname::VARCHAR = my_ct_record.table_name
               AND c.relkind = 'r'
             WHERE a.attnum > 0;
            
            my_query := 'CREATE MATERIALIZED VIEW ' || my_debug_view || ' AS '
                     || '('
                     || '          SELECT ';

            FOR my_column_name IN( SELECT unnest( my_ct_record.unique_expression ) ) LOOP
                my_query := my_query || ' ttcs.' || my_column_name || ' AS current_' || my_column_name || ','
                         || ' ttds.' || my_column_name || ' AS desired_' || my_column_name || ',';
            END LOOP;

            my_query := regexp_replace( my_query, ',$', '' )
                     || '            FROM @extschema@.vw_' || my_ct_record.table_name || '_current ttcs '
                     || ' FULL OUTER JOIN @extschema@.vw_' || my_ct_record.table_name || '_desired ttds '
                     || '              ON ( ' || array_to_string( my_cs_proto, ', ' ) || ' )::VARCHAR = ( ' || array_to_string( my_ds_proto, ', ' ) || ' )::VARCHAR '
                     || '           WHERE ( ttcs.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttcs.' ) || ' IS NULL ) '
                     || '              OR ( ttds.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttds.' ) || ' IS NULL ) '
                     || ')';
        ELSE
            EXECUTE 'REFRESH MATERIALIZED VIEW ' || my_debug_view;
        END IF;
    END LOOP; 
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_remove_debug_views()
RETURNS VOID AS
 $_$
DECLARE
    my_query VARCHAR;
BEGIN
    SELECT 'DROP MATERIALIZED VIEW ' || array_to_string( array_agg( n.nspname::VARCHAR || '.' || c.relname::VARCHAR ), '; DROP MATERIALIZED VIEW ' )
      INTO my_query
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = '@extschema@'
      JOIN @extschema@.tb_cache_table ct
        ON 'vw_' || ct.table_name || '_diff' = c.relname
     WHERE c.relkind = 'm';
    
    IF( my_query IS NOT NULL ) THEN
        EXECUTE my_query;
    END IF;

    SELECT 'DROP MATERIALIZED VIEW ' || array_to_string( array_agg( n.nspname::VARCHAR || '.' || c.relname::VARCHAR ), '; DROP MATERIALIZED VIEW ' )
      INTO my_query
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname::VARCHAR = '@extschema@'
      JOIN @extschema@.tb_cache_table ct
        ON 'vw_' || ct.table_name || '_desired' = c.relname
        OR 'vw_' || ct.table_name || '_current' = c.relname
     WHERE c.relkind = 'm';

     IF( my_query IS NULL ) THEN
        RETURN;
     END IF;

     EXECUTE my_query;
     RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table_trigger_wrapper()
RETURNS TRIGGER AS
 $_$
BEGIN
    PERFORM @extschema@.fn_remove_debug_views();
    PERFORM @extschema@.fn_refresh_cache_table( NEW.table_name );
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    -- Handle the deletion of a cache_table by removing trigger dependencies and statistics
    EXECUTE 'DELETE FROM @extschema@.tb_cache_table ctc '
         || '      USING @extschema@.tb_cache_table ct '
         || '      WHERE ctc.inherits_table = ''' || OLD.table_name || '''';

    EXECUTE 'DELETE FROM @extschema@.tb_cache_table_statistic WHERE cache_table = ' || OLD.cache_table;
    EXECUTE 'DELETE FROM @extschema@.tb_cache_table_trigger_column WHERE cache_table = ' || OLD.cache_table;

    PERFORM @extschema@.fn_remove_debug_views();
    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.schema || '.' || OLD.table_name || ' CASCADE';

    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';
