CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    /*
        This function validates the contents of table_columns against the catalog and the contents of generation_query,
        simplifying the error handling in fn_manage_trigger_changes, which helps define the triggers that can issue
        cache_update_request notifications to the tblmgr process.
    */

    PERFORM pid
       FROM pg_catalog.pg_stat_activity
      WHERE datname = current_database()
        AND application_name = 'pg_restore';

    IF FOUND THEN
        -- Do nothing during restore
        RETURN NEW;
    END IF;

    -- Get a definition of the cache table, this also serves to validate that the generation_query syntax is correct
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( ' GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    -- Begin iterating over the JSON structure in table_columns. We will validate its contents against the database's catalog
    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        -- Validate that the schema exists
        PERFORM n.oid
           FROM pg_namespace n
          WHERE nspname::VARCHAR = my_s_record.schema_name;

        IF NOT FOUND THEN
            RAISE NOTICE 'Schema % does not exist in the catalog.', my_s_record.schema_name;
        END IF;

        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists in the schema specified
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind IN( 'r', 'v' )
                AND c.relname::VARCHAR = my_ll_record.table_name;

            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               CASE WHEN json_typeof( value ) = 'string'
                                    THEN regexp_replace( value::VARCHAR, '"', '', 'g' ) -- value is json and will be wrapped in " despite it being a varchar
                                    ELSE NULL
                                     END AS aliased_column_name
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   -- Here we validate that the column is present on the table specified, and that the aliased version
                   -- of the column is present in generation_query
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;

                    IF NOT FOUND THEN
                        EXECUTE 'SELECT ' || my_tl_record.column_name
                             || '  FROM ' || my_s_record.schema_name || '.' || my_ll_record.table_name
                             || ' LIMIT 0';
                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    -- Final check that unique_expression columns are valid, unaliased, and present in table_columns
    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
EXCEPTION
    WHEN SQLSTATE '42P01' THEN
        RAISE NOTICE 'Table used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42883' THEN
        RAISE NOTICE 'Function used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42703' THEN
        RAISE NOTICE 'Column referenced in query does not exist';
        RETURN NULL;
    WHEN SQLSTATE '22004' THEN -- caused by the inner structure being invalid - check tl record
        RAISE NOTICE 'JSON parsing error';
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_trigger_changes()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record                       RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_s_record                     RECORD;
    my_trigger_column               INTEGER;
    my_cache_table_trigger_column   INTEGER;
BEGIN
    /*
        This function handles the modification of tb_cache_table.trigger_columns,
        and ensures that both tb_trigger_column and tb_cache_table_trigger_column are populated
        correctly prior to the execution of fn_manage_triggers, which installs/removes/updates triggers
        accordingly.
    */
    IF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
        -- Row's being removed - delete cache_table_trigger_columns that map triggered columns to cache_tables
        DELETE FROM @extschema@.tb_cache_table_trigger_column
              WHERE cache_table = my_record.cache_table;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW.table_columns::VARCHAR IS DISTINCT FROM OLD.table_columns::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSE
        RETURN NULL;
    END IF;

    PERFORM pid
       FROM pg_catalog.pg_stat_activity
      WHERE datname = current_database()
        AND application_name = 'pg_restore';

    IF FOUND THEN
        -- Do nothing during restore
        RETURN my_record;
    END IF;

    IF( TG_OP != 'DELETE' ) THEN
        IF( TG_OP = 'UPDATE' ) THEN
            -- If this is an update, we store all the table_columns encountered for use later
            CREATE TEMP TABLE tt_update_handler
            (
                schema_name VARCHAR,
                table_name  VARCHAR,
                column_name VARCHAR
            );
        END IF;
        -- Iterate over the table_columns JSON and create a temp table using the contents
        FOR my_s_record IN(
                SELECT key::VARCHAR AS schema_name,
                       value
                  FROM json_each( my_record.table_columns )
                          ) LOOP
            FOR my_ll_record IN(
                    SELECT key::VARCHAR AS table_name,
                           value
                      FROM json_each( my_s_record.value )
                               ) LOOP
                FOR my_ml_record IN(
                        SELECT key::VARCHAR AS alias,
                               value
                          FROM json_each( my_ll_record.value )
                                   ) LOOP
                    FOR my_tl_record IN(
                            SELECT key::VARCHAR AS column_name,
                                   value::VARCHAR AS aliased_column_name
                              FROM json_each( my_ml_record.value )
                                       ) LOOP
                        -- Validate that a trigger_column record exists for this table / column combination
                        IF( TG_OP = 'UPDATE' ) THEN
                            -- Store this table_column entry into our temp table
                            INSERT INTO tt_update_handler
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        );
                        END IF;

                        -- Get or create functionality for tb_trigger_column
                        SELECT trigger_column
                          INTO my_trigger_column
                          FROM @extschema@.tb_trigger_column
                         WHERE schema_name = my_s_record.schema_name
                           AND table_name  = my_ll_record.table_name
                           AND column_name = my_tl_record.column_name;

                        IF( my_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_trigger_column
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        )
                              RETURNING trigger_column
                                   INTO my_trigger_column;
                        END IF;

                        -- Get or create functionality for tb_cache_table_trigger_column
                        SELECT cache_table_trigger_column
                          INTO my_cache_table_trigger_column
                          FROM @extschema@.tb_cache_table_trigger_column
                         WHERE cache_table = my_record.cache_table
                           AND trigger_column = my_trigger_column;

                        IF( my_cache_table_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_cache_table_trigger_column
                                        (
                                            cache_table,
                                            trigger_column
                                        )
                                 VALUES
                                        (
                                            my_record.cache_table,
                                            my_trigger_column
                                        )
                              RETURNING cache_table_trigger_column
                                   INTO my_cache_table_trigger_column;
                        END IF;
                    END LOOP;
                END LOOP;
            END LOOP;
        END LOOP;

        IF( TG_OP = 'UPDATE' ) THEN
            -- Since the UPDATE may have removed entries in table_columns, we need to validate
            -- All the table triggers we've seen while parsing table_columns against existing entries,
            -- removing the cache_table_trigger_columns referencing those trigger_columns
            WITH tt_candidate_triggers AS
            (
                SELECT trigger_column
                  FROM @extschema@.tb_trigger_column tt
             LEFT JOIN tt_update_handler uh
                    ON uh.column_name = tt.column_name
                   AND uh.table_name = tt.table_name
                   AND uh.schema_name = tt.schema_name
                 WHERE uh.schema_name IS NULL
                   AND uh.column_name IS NULL
                   AND uh.table_name IS NULL
            ),
            tt_rows_to_remove AS
            (
                SELECT cttc.cache_table_trigger_column
                  FROM @extschema@.tb_cache_table_trigger_column cttc
            INNER JOIN tt_candidate_triggers tt
                    ON tt.trigger_column = cttc.trigger_column
                 WHERE cttc.cache_table = my_record.cache_table
            )
                DELETE FROM @extschema@.tb_cache_table_trigger_column cttc
                      USING tt_rows_to_remove tt
                      WHERE tt.cache_table_trigger_column = cttc.cache_table_trigger_column;
        END IF;
    END IF;

    IF( TG_OP = 'UPDATE' OR TG_OP = 'DELETE' ) THEN
        -- Since DELETE or UPDATE operations could have removed cache_table_trigger_column entries,
        -- we will mop up unused trigger_column entries
        WITH tt_trigger_columns_to_delete AS
        (
            SELECT DISTINCT tc.trigger_column
              FROM @extschema@.tb_trigger_column tc
         LEFT JOIN @extschema@.tb_cache_table_trigger_column cttc
                ON cttc.trigger_column = tc.trigger_column
             WHERE cttc.cache_table_trigger_column IS NULL
        )
        DELETE FROM @extschema@.tb_trigger_column tc
              USING tt_trigger_columns_to_delete tt
              WHERE tt.trigger_column = tc.trigger_column;
    END IF;

    -- We've maintained tb_cache_table_trigger_column and tb_trigger_column. Now we'll execute fn_manage_triggers so that it can
    -- CREATE / UPDATE / DROP triggers according to their new definitions, if they have changed.
    PERFORM @extschema@.fn_manage_triggers();
    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table_trigger_wrapper()
RETURNS TRIGGER AS
 $_$
BEGIN
    PERFORM pid
       FROM pg_catalog.pg_stat_activity
      WHERE datname = current_database()
        AND application_name = 'pg_restore';

    IF FOUND THEN
        -- Do nothing during restore
        RETURN NEW;
    END IF;

    PERFORM @extschema@.fn_remove_debug_views();
    PERFORM @extschema@.fn_refresh_cache_table( NEW.table_name );
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';
