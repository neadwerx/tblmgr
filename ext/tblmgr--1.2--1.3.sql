CREATE OR REPLACE FUNCTION @extschema@.fn_manage_indexes
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_index_name       VARCHAR;
    my_ct_record        RECORD;
    my_index_column     VARCHAR;
    my_index_type       VARCHAR;
    my_unique_columns   VARCHAR;
BEGIN
    /*
        This function handles the contents of index_columns, parsing the JSON and creating indexes
    */
    FOR my_index_name IN(
            SELECT ci.relname::VARCHAR
              FROM pg_class c
        INNER JOIN pg_index i
                ON i.indrelid = c.oid
               AND i.indisunique IS FALSE
        INNER JOIN pg_class ci
                ON ci.oid = i.indexrelid
        INNER JOIN pg_namespace n
                ON n.oid = c.relnamespace
        INNER JOIN @extschema@.tb_cache_table ct
                ON ct.table_name = c.relname::VARCHAR
               AND ct.schema = n.nspname::VARCHAR
               AND ct.cache_table = in_cache_table
                        ) LOOP
        EXECUTE 'DROP INDEX ' || my_index_name;
    END LOOP;

    SELECT unique_expression,
           schema,
           table_name,
           index_columns
      INTO my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    FOR my_index_column, my_index_type IN(
            SELECT key::VARCHAR,
                   value::VARCHAR
              FROM json_each_text( my_ct_record.index_columns )
                                         ) LOOP
        my_index_name := 'ix_' || my_ct_record.table_name || '_' || regexp_replace( my_index_column, '[^[:alnum:]]', '_', 'g' );
        my_index_name := regexp_replace( my_index_name, ',', '_', 'g' );
        IF( lower( my_index_type ) IN( 'gin', 'gist', 'brin', 'spgist', 'hash' ) ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || ' USING ' || my_index_type || '( ' || my_index_column || ' )';
        ELSIF( my_index_type ~* 'btree' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || '( ' || my_index_column || ' )';
        ELSE
            RAISE NOTICE 'Unsupported index type %. Skipping creation', my_index_type;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_replace_cache_table
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_drop_statement   VARCHAR;
    my_create_statement VARCHAR;
    my_cache_table_name VARCHAR;
BEGIN
    CREATE TEMP TABLE tt_dependent_objects
    (
        drop_statement   TEXT,
        create_statement TEXT,
        object_name      VARCHAR,
        is_base_obj      BOOLEAN,
        rank             INTEGER
    ) ON COMMIT DROP;

    WITH RECURSIVE tt_viewdefs AS
    (
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN pg_namespace sns
            ON sns.oid = sc.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON sns.nspname::VARCHAR = COALESCE( ct.schema, 'public' )
           AND sc.relname::VARCHAR = ct.table_name
           AND ct.cache_table = in_cache_table
         UNION
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               tt.rank + 1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN tt_viewdefs tt
            ON tt.dependent_oid = sc.oid
         WHERE sc.oid IS DISTINCT FROM dc.oid
    ),
    tt_def_prep AS
    (
        SELECT COALESCE( ns.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object_name,
               CASE WHEN c.relkind = 'm'
                    THEN 'MATERIALIZED'
                    ELSE ''
                     END AS view_type,
               regexp_replace( pg_get_viewdef( c.oid, TRUE ), ';\s*$', '' ) AS definition,
               tt.rank
          FROM tt_viewdefs tt
    INNER JOIN pg_class c
            ON c.oid = tt.dependent_oid
    INNER JOIN pg_namespace ns
            ON ns.oid = c.relnamespace
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'DROP ' || view_type || ' VIEW ' || object_name AS drop_statement,
                'CREATE ' || view_type || ' VIEW ' || object_name || ' AS ( ' || definition || ')' AS create_statement,
                object_name,
                TRUE,
                rank
           FROM tt_def_prep;

    WITH tt_fk_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
         WHERE co.contype = 'f'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype = 'f'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_fk_constraints tt;

    WITH tt_check_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
         WHERE co.contype != 'f'
           AND co.contype != 'p'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype != 'f'
           AND co.contype != 'p'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_check_constraints tt;

    --Capture triggers
    WITH tt_triggers AS
    (
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN @extschema@.tb_cache_table ct
            ON ct.table_name = c.relname::VARCHAR
           AND ct.schema = n.nspname::VARCHAR
           AND ct.cache_table = in_cache_table
         UNION
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'DROP TRIGGER ' || trigger_name || ' ON ' || object AS drop_statement,
                definition AS create_statement,
                trigger_name,
                FALSE,
                rank
           FROM tt_triggers;

    -- Capture indexes on dependent objects
    WITH tt_indexes AS
    (
        SELECT pg_get_indexdef( ci.oid ) AS create_statement,
               'DROP INDEX ' || ci.relname::VARCHAR AS drop_statement,
               ci.relname::VARCHAR AS object_name,
               tt.rank + 1 AS rank
          FROM pg_index i
    INNER JOIN pg_class ci
            ON ci.oid = i.indexrelid
    INNER JOIN pg_class c
            ON c.oid = i.indrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT drop_statement,
                create_statement,
                object_name,
                FALSE,
                rank
           FROM tt_indexes;

    FOR my_drop_statement IN(
        SELECT drop_statement
          FROM tt_dependent_objects
      ORDER BY rank DESC
    ) LOOP
        EXECUTE my_drop_statement;
    END LOOP;

    SELECT table_name
      INTO my_cache_table_name
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'DROP TABLE IF EXISTS ' || my_cache_table_name;
    EXECUTE 'ALTER TABLE ' || my_cache_table_name || '_temp RENAME TO ' || my_cache_table_name;

    FOR my_create_statement IN(
        SELECT create_Statement
          FROM tt_dependent_objects
      ORDER BY rank ASC
    ) LOOP
        EXECUTE my_create_statement;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql' VOLATILE PARALLEL UNSAFE;
