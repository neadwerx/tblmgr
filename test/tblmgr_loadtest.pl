#!/usr/bin/perl

$|=1;

use strict;
use warnings;

use DBI;
use Carp;
use Readonly;

Readonly my $LOOP_COUNT => 10000;

sub get_pk_array($$$)
{
    my( $table_name, $pk_col, $handle ) = @_;

    my @array;

    my $q = <<SQL;
    SELECT $pk_col AS pk
      FROM $table_name
  ORDER BY random()
     LIMIT $LOOP_COUNT
SQL

    my $sth = $handle->prepare( $q );

    unless( $sth->execute() )
    {
        croak 'Failed to get primary key array';
    }

    while( my $row = $sth->fetchrow_hashref() )
    {
        push( @array, $row->{pk} );
    }

    return @array;
}

sub toggle_triggers($$$)
{
    my( $handle, $table_name, $state ) = @_;

    my $disable_trigger_q = <<SQL;
    SELECT t.tgname::VARCHAR AS trigger_name
      FROM pg_trigger t
      JOIN pg_class c
        ON c.oid = t.tgrelid
       AND c.relkind = 'r'
       AND c.relhastriggers IS TRUE
       AND c.relname::VARCHAR = '$table_name'
     WHERE t.tgname::VARCHAR LIKE 'tr_protect_key%'
SQL

    my $trigger_sth = $handle->prepare( $disable_trigger_q );
    unless( $trigger_sth->execute() )
    {
        croak 'Failed to get trigger list';
    }

    while( my $row = $trigger_sth->fetchrow_hashref() )
    {
        my $trigger_name = $row->{trigger_name};

        if( $state )
        {
            unless( $handle->do( "ALTER TABLE $table_name ENABLE TRIGGER $trigger_name" ) )
            {
                croak "Error enabling trigger $trigger_name";
            }
        }
        else
        {
            unless( $handle->do( "ALTER TABLE $table_name DISABLE TRIGGER $trigger_name" ) )
            {
                croak "Error disabling trigger $trigger_name";
            }
        }
    }

    return;
}

my $index_defs = { }; # Global to store index definitions for the below function between calls
sub toggle_unique_constraints($$$)
{
    my( $handle, $table_name, $state ) = @_;

    my $index_q = <<SQL;
    SELECT ci.relname AS index_name,
           pg_get_indexdef( ci.oid ) AS definition,
           cs.conname AS constraint_name
      FROM pg_index i
      JOIN pg_class c
        ON c.oid = i.indrelid
       AND c.relname::VARCHAR = '$table_name'
      JOIN pg_class ci
        ON ci.oid = i.indexrelid
 LEFT JOIN pg_constraint cs
        ON cs.conrelid = c.oid
       AND cs.contype = 'u'
       AND cs.conindid = ci.oid
 LEFT JOIN pg_constraint cpc
        ON cpc.conrelid = c.oid
       AND cpc.contype = 'p'
       AND cpc.conindid = ci.oid
     WHERE i.indisunique
       AND cpc.oid IS NULL -- No PK constraints will be dropped
SQL
    my $index_sth = $handle->prepare( $index_q );
    unless( $index_sth->execute() )
    {
        croak 'Failed to get unique index list';
    }

    if( $state )
    {
        foreach my $index_name( keys %{$index_defs->{$table_name}} )
        {
            my $definition = $index_defs->{$table_name}->{$index_name};

            unless( $handle->do( $definition ) )
            {
                carp "Failed to create index $index_name";
            }
        }
    }
    else
    {
        while( my $row = $index_sth->fetchrow_hashref() )
        {
            my $index_name = $row->{index_name};
            my $index_def  = $row->{definition};
            my $constraint = $row->{constraint_name};

            $index_defs->{$table_name}->{$index_name} = $index_def;

            if( $constraint )
            {
                unless( $handle->do( "ALTER TABLE $table_name DROP CONSTRAINT $constraint" ) )
                {
                    carp "Failed to drop constraint $constraint on $table_name";
                }
            }

            unless( $handle->do( "DROP INDEX IF EXISTS $index_name" ) )
            {
                carp "Failed to drop index $index_name( $index_def )";
            }
        }
    }
}

sub get_update_segment($$$$$)
{
    my( $handle, $type, $table_name, $column_name, $typedef ) = @_;
    my $update_segment = '';
    my $string = '';
    my @chars = ( "A" .. "Z", "a" .. "z" );
    $string .= $chars[ rand @chars ] for 0..8;

    if( $type eq 'json' )
    {
        $update_segment = "'{\"fallback\":\"$string\"}'";
    }
    elsif( $type =~ /timestamp/ or $type =~ /date/ )
    {
        $update_segment = " COALESCE( $column_name, now() ) + interval '1 day'";
    }
    elsif( $type =~ /interval/ )
    {
        $update_segment = " interval '30 minutes' ";
    }
    elsif( $type eq 'boomean' or $type eq 'bool' )
    {
        $update_segment = " NOT $column_name ";
    }
    elsif( $type eq 'char' or $type eq 'character varying' or $type eq 'varchar' or $type eq 'text' )
    {
        $update_segment = "'$string'::$typedef";
    }
    elsif( $type eq 'double precision' )
    {
        $update_segment = " COALESCE( $column_name, 0.0 ) + 1 ";
    }
    elsif( $type eq 'int4' )
    {
        my $fk_info_q = <<SQL;
        SELECT ft.relname AS table_name,
               pk.attname AS pk_col
          FROM pg_class c
          JOIN pg_attribute a
            ON a.attnum > 0
           AND a.attrelid = c.oid
           AND a.attname::VARCHAR = '$column_name'
          JOIN pg_constraint fk
            ON fk.conrelid = c.oid
           AND a.attnum = ANY( fk.conkey )
           AND fk.contype = 'f'
          JOIN pg_class ft
            ON ft.oid = fk.confrelid
          JOIN pg_attribute pk
            ON pk.attrelid = ft.oid
           AND pk.attnum > 0
          JOIN pg_constraint pkc
            ON pkc.conrelid = ft.oid
           AND pkc.contype = 'p'
           AND pk.attnum = ANY( pkc.conkey )
         WHERE c.relname::VARCHAR = '$table_name'
SQL

        my $fk_info_sth = $handle->prepare( $fk_info_q );
        unless( $fk_info_sth->execute() )
        {
            croak 'Failed to determinate foreign key relationship';
        }

        my $fk_row = $fk_info_sth->fetchrow_hashref();

        if( $fk_info_sth->rows() > 0 )
        {
            my $foreign_table = $fk_row->{table_name};
            my $foreign_pk    = $fk_row->{pk_col};

            my $new_val = <<SQL;
            SELECT x.$foreign_pk AS new_val
              FROM $foreign_table x
--         LEFT JOIN $table_name y
--                ON y.$column_name = x.$foreign_pk
          ORDER BY random()
             LIMIT 1
SQL

            my $new_value_sth = $handle->prepare( $new_val );

            unless( $new_value_sth->execute() )
            {
                croak 'Failed to retreive new fk value';
            }

            my $new_value_row = $new_value_sth->fetchrow_hashref();

            my $new_value = $new_value_row->{new_val};

            $update_segment = " $new_value ";
        }
        else
        {
            $update_segment = " COALESCE( $column_name, 0 ) + 1 ";
        }
    }

    return $update_segment;
}

my $handle = DBI->connect( 'dbi:Pg:dbname=thdca_tblmgr_test;host=athena.internal;port=5432', 'postgres', undef );

unless( $handle )
{
    croak 'Failed to connect to database';
}

my $catalog_query = <<SQL;
    SELECT tc.table_name,
           tc.column_name
      FROM tblmgr.tb_trigger_column tc
INNER JOIN pg_class c
        ON c.relname::VARCHAR = tc.table_name
       AND c.relkind = 'r'
INNER JOIN pg_attribute a
        ON a.attname::VARCHAR = tc.column_name
       AND a.attnum > 0
       AND a.attrelid = c.oid
 LEFT JOIN pg_constraint cn
        ON cn.conrelid = c.oid
       AND cn.contype = 'p'
       AND cn.conkey[1] = a.attnum
     WHERE cn.oid IS NULL -- Exclude primary key updates
  ORDER BY tc.table_name,
           tc.column_name
SQL

my $sth = $handle->prepare( $catalog_query );

unless( $sth->execute() )
{
    croak 'Failed to inspect schema monitored by tblmgr';
}

my $pair_count = $sth->rows();
print "Hammering the DB with UPDATEs with $LOOP_COUNT updates per each $pair_count table/column pair ...\n";
while( my $row = $sth->fetchrow_hashref() )
{
    my $column_name = $row->{column_name};
    my $table_name  = $row->{table_name};

    my $type_query = <<SQL;
    SELECT t.typname,
           CASE WHEN t.typname::VARCHAR = 'varchar'::VARCHAR
                THEN t.typname::VARCHAR || '(' || ( a.atttypmod - 4 )::VARCHAR || ')'
                WHEN t.typname::VARCHAR = 'bpchar'::VARCHAR
                THEN 'char' || '(' || ( a.atttypmod - 4 )::VARCHAR || ')'
                WHEN t.typname::VARCHAR = 'numeric'::VARCHAR
                THEN 'numeric(' || ((( a.atttypmod - 4 ) >> 16 ) & 65535 )::VARCHAR || ',' || ( ( a.atttypmod - 4 ) & 65535 )::VARCHAR || ')'
                ELSE t.typname
                 END AS data_type
      FROM pg_attribute a
INNER JOIN pg_type t
        ON t.oid = a.atttypid
INNER JOIN pg_class c
        ON c.oid = a.attrelid
       AND c.relname::VARCHAR = '$table_name'
     WHERE a.attnum > 0
       AND a.attname::VARCHAR = '$column_name'
SQL

    my $type_sth = $handle->prepare( $type_query );
    unless( $type_sth->execute() )
    {
        croak 'Failed to get type information for trigger_column';
    }

    my $type_row = $type_sth->fetchrow_hashref();
    my $type     = $type_row->{typname};
    my $typedef  = $type_row->{data_type};

    my $primary_key_q = <<SQL;
    SELECT a.attname::VARCHAR
      FROM pg_class c
      JOIN pg_attribute a
        ON a.attrelid = c.oid
      JOIN pg_constraint cn
        ON cn.conrelid = c.oid
       AND cn.contype = 'p'
       AND cn.conkey[1] = a.attnum
     WHERE c.relname::VARCHAR = '$table_name'
SQL

    my $pk_sth = $handle->prepare( $primary_key_q );

    unless( $pk_sth->execute() )
    {
        croak "Failed to get primary key for cached table $table_name";
    }

    my $pk_row = $pk_sth->fetchrow_hashref();
    my $pk_col = $pk_row->{attname};

    my @pk_array = get_pk_array( $table_name, $pk_col, $handle );

    # Disable tr_protect_keys if exists
    print "Disabling triggers / unique indexes for $table_name...\n";

    toggle_triggers(
        $handle,
        $table_name,
        0
    );

    toggle_unique_constraints(
        $handle,
        $table_name,
        0
    );

    foreach my $pk( @pk_array )
    {
        my $update_segment = get_update_segment(
            $handle,
            $type,
            $table_name,
            $column_name,
            $typedef
        );

        my $statement = <<SQL;
        UPDATE $table_name
           SET $column_name = $update_segment
         WHERE $pk_col = $pk
SQL

        unless( $handle->do( $statement ) )
        {
            croak "Failed to execute $statement";
        }
    }

    toggle_triggers(
        $handle,
        $table_name,
        1
    );

    toggle_unique_constraints(
        $handle,
        $table_name,
        1
    );
}

# We're done hammering the database, now let's monitor tb_cache_update_reuqest for all the requests to be purged.

my $requests_q = <<SQL;
    SELECT COUNT(cache_update_request) AS count
      FROM tblmgr.tb_cache_update_request
SQL

my $requests_sth = $handle->prepare( $requests_q );
   $requests_sth->execute() or croak 'Failed to get outstanding requests';
my $requests_row = $requests_sth->fetchrow_hashref();
my $count        = $requests_row->{count};

while( $count > 0 )
{
    sleep 1;
    print "$count unserviced requests remaining\n";
    $requests_sth->execute() or carp 'Failed to get outstanding requests';
    $requests_row = $requests_sth->fetchrow_hashref();
    $count = $requests_row->{count};
}

print "All requests serviced. Checking accuracy...\n";

my $ct_query = <<SQL;
    SELECT table_name,
           cache_table
      FROM tblmgr.tb_cache_table
SQL

my $ct_sth = $handle->prepare( $ct_query );
unless( $ct_sth->execute() )
{
    croak 'Failed to get cache table listing';
}

my $validate_q = <<SQL;
    SELECT tblmgr.fn_validate_cache_table( ?, 1 ) AS result
SQL
my $validate_sth = $handle->prepare( $validate_q );

while( my $row = $ct_sth->fetchrow_hashref() )
{
    my $pk_cache_table = $row->{cache_table};
    my $table_name     = $row->{table_name};

    $validate_sth->bind_param( 1, $pk_cache_table );
    print "Validating integrity of $table_name...\n";
    unless( $validate_sth->execute() )
    {
        croak 'Failed to execute validation routine';
    }

    my $validation_result = $validate_sth->fetchrow_hashref();
    my $is_valid = $validation_result->{result};

    if( $is_valid )
    {
        print "Table $table_name is valid!\n";
    }
    else
    {
        print "Table $table_name IS NOT valid\n";
    }
}

exit 0;
