#!/usr/bin/perl

$|=1;

use utf8;
use strict;
use warnings;

use DBI;
use JSON;
use Data::Dumper;

my $SLEEP_TIMER = 15; # Restrict this further to find the minimum update time
sub query($$;$$)
{
    my( $handle, $query, $delim, $params ) = @_;
    my $sth = $handle->prepare( $query );
    my $has_params = 0;
       $has_params = 1 if( ref( $params ) eq 'ARRAY' and scalar( @$params ) > 0 );

    if( $has_params )
    {
        my $param_index = 1;

        foreach my $param( @$params )
        {
            $sth->bind_param( $param_index, $param );
            $param_index++;
        }
    }

    unless( $sth->execute() )
    {
        my $message = "Failed to execute query\n$query\n";

        if( $has_params )
        {
            $message .= "with params:\n";
            $message .= Dumper( $params );
        }

        die( "$message\n" );
    }

    my $result = {};

    if( $delim )
    {
        $result = $sth->fetchall_hashref( $delim );
    }
    else
    {
        $result = $sth->fetchrow_hashref();
    }

    $sth->finish();
    return %$result if( $result );
    return;
}

sub query_one($$;$)
{
    my( $handle, $query, $params ) = @_;

    return query( $handle, $query, undef, $params );
}

sub query_all($$$;$)
{
    my( $handle, $query, $delim, $params ) = @_;

    return query( $handle, $query, $delim, $params );
}

sub build_insert_query($$$)
{
    my( $handle, $table_name, $record ) = @_;

    my $insert = "INSERT INTO $table_name (";

    foreach my $key( sort { $a cmp $b } keys %$record )
    {
        $insert .= "$key,";
    }

    $insert  =~ s/,$//;
    $insert .= ' ) VALUES ( ';
    my @params;

    foreach my $key( sort { $a cmp $b } keys %$record )
    {
        my $val = $record->{$key};

        $insert .= '?,';
        push( @params, $val );
    }

    $insert  =~ s/,$//;
    $insert .= ')';

    my $sth = $handle->prepare( $insert );
    my $ind = 1;
    foreach my $param( @params )
    {
        $sth->bind_param( $ind, $param );
        $ind++;
    }

    $sth->execute() or die( 'INSERT failed' );
    return;
}

sub get_ct_count($$)
{
    my( $handle, $ct ) = @_;

    my %data = query_one(
        $handle,
        "SELECT COUNT(*) FROM $ct"
    );

    return $data{count};
}

sub check_update($$$$)
{
    my( $handle, $ct_name, $table_name, $orig_record ) = @_;

    my %record = %$orig_record;
    my $record = \%record;
    # Returns 1 pass, 0 fail
    my %unique = query_one(
        $handle,
        "SELECT unique_expression
           FROM tblmgr.tb_cache_table
          WHERE table_name = '$ct_name'",
    );

    my $pk_value;
    my $pk_column;

    my $pk_col_query = <<SQL;
    SELECT tblmgr.fn_get_table_pk_col( '$table_name' ) AS pk_col
SQL
    my %pk_col_data  = query_one(
        $handle,
        $pk_col_query
    );

    my $pk_col = $pk_col_data{pk_col};
    my $pk_val = $record->{$pk_col};

    delete( $record->{$pk_col} );
    my @unique = @{$unique{unique_expression}};

    foreach my $key( @unique )
    {
        if( $record->{$key} )
        {
            delete( $record->{$key} );
        }
    }

    my $update_statement = "UPDATE $table_name SET ";
    my $count            = scalar keys %$record;
    my $target_index     = int rand( $count );
    my $index            = 0;
    my $update_revert    = "UPDATE $table_name SET ";

    foreach my $column( keys %$record )
    {
        my $value = $record->{$column};
        my $original = $record->{$column};

        unless( defined $original )
        {
            $original = 'NULL';
        }

        unless( defined $value )
        {
            $value = 'NULL';
        }

        my $type_query = <<SQL;
        SELECT t.typname
          FROM pg_attribute a
    INNER JOIN pg_type t
            ON t.oid = a.atttypid
    INNER JOIN pg_class c
            ON c.oid = a.attrelid
           AND c.relname = '$table_name'
         WHERE a.attnum > 0
           AND a.attname = '$column'
SQL
        my %type_data = query_one(
            $handle,
            $type_query
        );

        my $type = $type_data{typname};

        # Data fuzzing for updates
        if( $index == $target_index )
        {
            # Modify data so update goes through
            if( $type eq 'int4' )
            {
                my %fk_info = query_one(
                    $handle,
                    "SELECT ft.relname AS table_name,
                            pk.attname AS pk_col
                       FROM pg_class c
                       JOIN pg_attribute a
                         ON a.attnum > 0
                        AND a.attrelid = c.oid
                        AND a.attname = '$column'
                       JOIN pg_constraint fk
                         ON fk.conrelid = c.oid
                        AND a.attnum = ANY( fk.conkey )
                        AND fk.contype = 'f'
                       JOIN pg_class ft
                         ON ft.oid = fk.confrelid
                       JOIN pg_attribute pk
                         ON pk.attrelid = ft.oid
                        AND pk.attnum > 0
                       JOIN pg_constraint pkc
                         ON pkc.conrelid = ft.oid
                        AND pkc.contype = 'p'
                        AND pk.attnum = ANY( pkc.conkey )
                      WHERE c.relname::VARCHAR = '$table_name'"
                );

                my $foreign_table = $fk_info{table_name};
                my $foreign_pk    = $fk_info{pk_col};

                if( defined $foreign_table and defined $foreign_pk )
                {
                    my %new_val = query_one(
                        $handle,
                        "SELECT $foreign_pk AS pk
                           FROM $foreign_table
                          LIMIT 1"
                    );

                    $value = $new_val{pk};
                }
                elsif( $value eq 'NULL' )
                {
                    $value = 1;
                }
                else
                {
                    $value++;
                }
            }
            elsif( $type eq 'boolean' or $type eq 'bool' )
            {
                if( $value or $value eq 'NULL' )
                {
                    $value = 'FALSE';
                }
                else
                {
                    $value = 'TRUE';
                }
            }
            elsif( $type eq 'json' )
            {
                my $new_val = { };

                if( $value ne 'NULL' )
                {
                   $new_val = decode_json( $value );
                }

                $new_val->{'foo'} = 'bar';
                $value   = encode_json( $new_val );
            }
            elsif( $type eq 'character varying' or $type eq 'character' or $type eq 'text' or $type eq 'varchar' )
            {
                $value = 'FooBarBaz';
            }
            elsif( $type eq 'double precision' )
            {
                if( $value eq 'NULL' )
                {
                    $value = 1.234;
                }
                else
                {
                    $value = $value + 1;
                }
            }
            elsif( $type =~ /timestamp/ or $type =~ /date/ )
            {
                $value = 'now()';
            }
            else
            {
                warn "Unrecognized type $type\n";
            }
        }

        if( $value eq 'NULL' )
        {
            $update_statement .= "$column = NULL::$type,";
        }
        elsif( $value eq 'now()' )
        {
            $update_statement .= "$column = ${value}::$type,";
        }
        else
        {
            $update_statement .= "$column = '$value'::$type,";
        }

        if( $original eq 'NULL' )
        {
            $update_revert    .= "$column = NULL::$type,";
        }
        else
        {
            $update_revert    .= "$column = '$original'::$type,";
        }

        $index++;
    }

    $update_statement =~ s/,$//;
    $update_statement .= " WHERE $pk_col = $pk_val";
    $update_revert    =~ s/,$//;
    $update_revert    .= " WHERE $pk_col = $pk_val";

    $handle->do( $update_statement ) or return 0;
    sleep( $SLEEP_TIMER );
    $handle->do( $update_revert    ) or return 0;
    sleep( $SLEEP_TIMER );

    return 1;
}

my $handle = DBI->connect( 'dbi:Pg:dbname=thdca_tblmgr_test;host=athena.internal;port=5432', 'postgres', undef ) or die( 'Failed to connect' );

print "CT_PROJECT_DEPARTMENT ====================================================\n";
$handle->do(
    "SELECT tblmgr.fn_refresh_cache_table( 'ct_project_department' )"
);

my %data = query_one(
    $handle,
   'SELECT project
      FROM tb_project_merchandise_classification
  GROUP BY project
    HAVING COUNT( project_merchandise_classification ) = 1
     LIMIT 1'
);

my $pk_project = $data{project};

my $start_count = get_ct_count( $handle, 'ct_project_department' );

my %record = query_one(
    $handle,
   'SELECT *
      FROM tb_project_merchandise_classification
     WHERE project = ?',
    [$pk_project]
);

$handle->do( "DELETE FROM tb_project_merchandise_classification WHERE project = $pk_project" );

sleep( $SLEEP_TIMER );

my $del_count = get_ct_count( $handle, 'ct_project_department' );

unless( $start_count == ($del_count + 1) )
{
    die( "DEL failed on ct_project_department SC: $start_count DC: $del_count\n" );
}

build_insert_query( $handle, 'tb_project_merchandise_classification', \%record );

sleep( $SLEEP_TIMER );

my $ins_count = get_ct_count( $handle, 'ct_project_department' );

unless( $ins_count == $start_count )
{
    die( "INS failed on ct_project_department\n" );
}

unless( check_update( $handle, 'ct_project_department', 'tb_project_merchandise_classification', \%record ) )
{
    die( "UPD failed on ct_project_department\n" );
}

print "CT_PESD_FILTER ====================================================\n";

$handle->do( "SELECT tblmgr.fn_refresh_cache_table( 'ct_pesd_filter' )" );

%data = query_one(
    $handle,
    'SELECT r.reset
       FROM ONLY tb_reset r
 INNER JOIN tb_project pj
         ON pj.project = r.project
        AND pj.project_status IN( 3, 5 )
        AND pj.archived IS NULL
      WHERE r.in_scope IS TRUE
      LIMIT 1'
);

my $pk_reset = $data{reset};
$start_count = get_ct_count( $handle, 'ct_pesd_filter' );
%record = query_one(
    $handle,
    'SELECT *
       FROM ONLY tb_reset
      WHERE reset = ?',
    [$pk_reset]
);

$handle->do( "UPDATE tb_reset SET in_scope = FALSE WHERE reset = $pk_reset" );

sleep( $SLEEP_TIMER );

$del_count = get_ct_count( $handle, 'ct_pesd_filter' );

unless( $start_count == ( $del_count + 1 ) )
{
    die( "DEL failed on ct_pesd_filter\n" );
}

$handle->do( "UPDATE tb_reset SET in_scope = TRUE WHERE reset = $pk_reset" );

sleep( $SLEEP_TIMER );

$ins_count = get_ct_count( $handle, 'ct_pesd_filter' );

unless( $ins_count == $start_count )
{
    die( "INS failed on ct_pesd_filter\n" );
}

unless( check_update( $handle, 'ct_pesd_filter', 'tb_reset', \%record ) )
{
    die( "UPD failed on ct_pesd_filter\n" );
}

# Special piece to test phsycial insert rather than data-mediated insert on ct_pesd_filter

my %project_candidate = query_one(
    $handle,
    'SELECT pj.project,
            pj.program
       FROM tb_project pj
 INNER JOIN ONLY tb_reset r
         ON r.project = pj.project
      WHERE pj.archived IS NULL
        AND pj.project_status IN( 3, 5 )
   GROUP BY pj.project
     HAVING COUNT( r.reset ) < 200
   ORDER BY COUNT( r.reset ) ASC
      LIMIT 1'
);

$pk_project = $project_candidate{project};
my $pk_program = $project_candidate{program};
my %location_candidate = query_one(
    $handle,
    'SELECT l.location
       FROM tb_location l
  LEFT JOIN ONLY tb_reset r
         ON r.location = l.location
        AND r.project = ?
      WHERE l.location_type = 1
      LIMIT 1',
    [$pk_project]
);

my $pk_location = $location_candidate{location};
my $map =
{
    location => $pk_location,
    project  => $pk_project,
    program  => $pk_program,
    in_scope => 1,
};

$map = encode_json( $map );

my $pre_ins_count = get_ct_count( $handle, 'ct_pesd_filter' );

my %reset = query_one(
    $handle,
    'SELECT fn_new_reset( ? ) AS reset',
    [$map]
);

$pk_reset = $reset{reset};

sleep( $SLEEP_TIMER );
my $post_ins_count = get_ct_count( $handle, 'ct_pesd_filter' );

unless( $pre_ins_count == ( $post_ins_count - 1 ) )
{
    die( "INS 2 failed on ct_pesd_filter pre: $pre_ins_count post: $post_ins_count\n" );
}

# Special logic to test rollup / masquerading functionality
my %pre_rollup = query_one(
    $handle,
    'SELECT SUM( c.reset_issue_counts ) AS reset_issue_count
       FROM ct_pesd_filter ct
CROSS JOIN LATERAL ( SELECT svals( ct.reset_issue_counts )::INTEGER AS reset_issue_counts ) c
      WHERE ct.reset = ?',
    [ $pk_reset ]
);

my $pre_rollup_count = $pre_rollup{reset_issue_count};
$handle->do( "INSERT INTO tb_reset_issue
                 (
                    creator,
                    modifier,
                    reset,
                    reset_issue_type,
                    body_general,
                    owner,
                    reset_issue_status
                 )
          SELECT 0 AS creator,
                 0 AS modifier,
                 $pk_reset AS reset,
                 rit.reset_issue_type,
                 'tblmgr update masquerade test',
                 0,
                 ris.reset_issue_status
            FROM tb_reset_issue_status ris
      CROSS JOIN tb_reset_issue_type rit
           WHERE ris.reset_issue_status > 0
           LIMIT 1"
);

# get pk since fn_redirect_insert prevents RETURNING
my %reset_issue = query_one(
    $handle,
    'SELECT reset_issue
       FROM tb_reset_issue
      WHERE reset = ?
        AND creator = 0
        AND modifier = 0
        AND owner = 0',
    [ $pk_reset ]
);

my $pk_reset_issue = $reset_issue{reset_issue};
sleep( $SLEEP_TIMER );
my %post_rollup = query_one(
    $handle,
    'SELECT SUM( c.reset_issue_counts ) AS reset_issue_count
       FROM ct_pesd_filter ct
CROSS JOIN LATERAL ( SELECT svals( ct.reset_issue_counts )::INTEGER AS reset_issue_counts ) c
      WHERE ct.reset = ?',
    [ $pk_reset ]
);

my $post_rollup_count = $post_rollup{reset_issue_count};

unless( $pre_rollup_count == ( $post_rollup_count - 1 ) )
{
    die( "MASQ failed on ct_pesd_filter" );
}

$handle->do( "DELETE FROM tb_reset_issue WHERE reset_issue = $pk_reset_issue" ) or die( "MASQ DEL failed on ct_pesd_filter" );
sleep( $SLEEP_TIMER );
my %post_del_rollup_count = query_one(
    $handle,
    'SELECT SUM( c.reset_issue_counts ) AS reset_issue_count
       FROM ct_pesd_filter ct
CROSS JOIN LATERAL ( SELECT svals( ct.reset_issue_counts )::INTEGER AS reset_issue_counts ) c
      WHERE ct.reset = ?',
    [ $pk_reset ]
);

my $post_del_rollup_count = $post_del_rollup_count{reset_issue_count};

unless( $post_del_rollup_count == $pre_rollup_count )
{
    print "pk_reset: $pk_reset, pk_reset_issue: $pk_reset_issue\n";
    die( "MASQ DEL failed on ct_pesd_filter" );
}

$handle->do( "DELETE FROM tb_reset_event WHERE reset = $pk_reset" ) or die( "DEL failed on ct_pesd_filter\n" );
$handle->do( "DELETE FROM tb_reset WHERE reset = $pk_reset" ) or die( "DEL failed on ct_pesd_filter\n" );
sleep( $SLEEP_TIMER );
my $post_del_count = get_ct_count( $handle, 'ct_pesd_filter' );
unless( $post_del_count == $pre_ins_count )
{
    die( "DEL 2 failed on ct_pesd_filter\n" );
}

print "passed\n";
