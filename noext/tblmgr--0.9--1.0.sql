CREATE OR REPLACE FUNCTION tblmgr.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    /*
        This function validates the contents of table_columns against the catalog and the contents of generation_query,
        simplifying the error handling in fn_manage_trigger_changes, which helps define the triggers that can issue
        cache_update_request notifications to the tblmgr process.
    */

    -- Get a definition of the cache table, this also serves to validate that the generation_query syntax is correct
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( ' GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    -- Begin iterating over the JSON structure in table_columns. We will validate its contents against the database's catalog
    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        -- Validate that the schema exists
        PERFORM n.oid
           FROM pg_namespace n
          WHERE nspname::VARCHAR = my_s_record.schema_name;

        IF NOT FOUND THEN
            RAISE NOTICE 'Schema % does not exist in the catalog.', my_s_record.schema_name;
        END IF;

        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists in the schema specified
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind = 'r'
                AND c.relname::VARCHAR = my_ll_record.table_name;

            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               CASE WHEN json_typeof( value ) = 'string'
                                    THEN regexp_replace( value::VARCHAR, '"', '', 'g' ) -- value is json and will be wrapped in " despit
                                    ELSE NULL
                                     END AS aliased_column_name
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   -- Here we validate that the column is present on the table specified, and that the aliased version
                   -- of the column is present in generation_query
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;

                    IF NOT FOUND THEN
                        EXECUTE 'SELECT ' || my_tl_record.column_name
                             || '  FROM ' || my_s_record.schema_name || '.' || my_ll_record.table_name
                             || ' LIMIT 0';

                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    -- Final check that unique_expression columns are valid, unaliased, and present in table_columns
    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
EXCEPTION
    WHEN SQLSTATE '42P01' THEN
        RAISE NOTICE 'Table used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42883' THEN
        RAISE NOTICE 'Function used in query does not yet exist. This is either due to a pg_restore or a search_path issue.';
        RETURN NEW;
    WHEN SQLSTATE '42703' THEN
        RAISE NOTICE 'Column referenced in query does not exist';
        RETURN NULL;
    WHEN SQLSTATE '22004' THEN -- caused by the inner structure being invalid - check tl record
        RAISE NOTICE 'JSON parsing error';
        RETURN NULL;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION tblmgr.fn_replace_cache_table
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_drop_statement   VARCHAR;
    my_create_statement VARCHAR;
    my_cache_table_name VARCHAR;
BEGIN
    CREATE TEMP TABLE tt_dependent_objects
    (
        drop_statement   TEXT,
        create_statement TEXT,
        object_name      VARCHAR,
        is_base_obj      BOOLEAN,
        rank             INTEGER
    ) ON COMMIT DROP;

    WITH RECURSIVE tt_viewdefs AS
    (
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN pg_namespace sns
            ON sns.oid = sc.relnamespace
    INNER JOIN tblmgr.tb_cache_table ct
            ON sns.nspname::VARCHAR = COALESCE( ct.schema, 'public' )
           AND sc.relname::VARCHAR = ct.table_name
           AND ct.cache_table = in_cache_table
         UNION
        SELECT DISTINCT ON( dc.oid, sc.oid )
               dc.oid AS dependent_oid,
               sc.oid,
               tt.rank + 1 AS rank
          FROM pg_depend d
    INNER JOIN pg_rewrite rw
            ON rw.oid = d.objid
    INNER JOIN pg_class dc
            ON dc.oid = rw.ev_class
           AND dc.relkind IN( 'm', 'v' )
    INNER JOIN pg_class sc
            ON sc.oid = d.refobjid
    INNER JOIN tt_viewdefs tt
            ON tt.dependent_oid = sc.oid
         WHERE sc.oid IS DISTINCT FROM dc.oid
    ),
    tt_def_prep AS
    (
        SELECT COALESCE( ns.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object_name,
               CASE WHEN c.relkind = 'm'
                    THEN 'MATERIALIZED'
                    ELSE ''
                     END AS view_type,
               regexp_replace( pg_get_viewdef( c.oid, TRUE ), ';\s*$', '' ) AS definition,
               tt.rank
          FROM tt_viewdefs tt
    INNER JOIN pg_class c
            ON c.oid = tt.dependent_oid
    INNER JOIN pg_namespace ns
            ON ns.oid = c.relnamespace
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'DROP ' || view_type || ' VIEW ' || object_name AS drop_statement,
                'CREATE ' || view_type || ' VIEW ' || object_name || ' AS ( ' || definition || ')' AS create_statement,
                object_name,
                TRUE,
                rank
           FROM tt_def_prep;

    WITH tt_fk_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tblmgr.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
         WHERE co.contype = 'f'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.confrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN pg_class cr
            ON cr.oid = co.conrelid
    INNER JOIN pg_namespace nr
            ON nr.oid = cr.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( nr.nspname::VARCHAR, 'public' ) || '.' || cr.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype = 'f'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_fk_constraints tt;

    WITH tt_check_constraints AS
    (
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tblmgr.tb_cache_table ct
            ON ct.schema = n.nspname::VARCHAR
           AND ct.table_name = c.relname::VARCHAR
           AND ct.cache_table = in_cache_table
         WHERE co.contype != 'f'
           AND co.contype != 'p'
         UNION
        SELECT co.conname::VARCHAR AS constraint_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_constraintdef( co.oid, TRUE ) AS definition,
               tt.rank + 1 AS rank
          FROM pg_constraint co
    INNER JOIN pg_class c
            ON c.oid = co.conrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
         WHERE co.contype != 'f'
           AND co.contype != 'p'
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP CONSTRAINT ' || constraint_name AS drop_statement,
                'ALTER TABLE ' || object || ' ADD CONSTRAINT ' || constraint_name || ' ' || definition AS create_statement,
                constraint_name,
                FALSE,
                rank
           FROM tt_check_constraints tt;

    --Capture triggers
    WITH tt_triggers AS
    (
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tblmgr.tb_cache_table ct
            ON ct.table_name = c.relname::VARCHAR
           AND ct.schema = n.nspname::VARCHAR
           AND ct.cache_table = in_cache_table
         UNION
        SELECT t.tgname AS trigger_name,
               COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR AS object,
               pg_get_triggerdef( t.oid, TRUE ) AS definition,
               2 AS rank
          FROM pg_trigger t
    INNER JOIN pg_class c
            ON c.oid = t.tgrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT 'ALTER TABLE ' || object || ' DROP TRIGGER ' || trigger_name AS drop_statement,
                definition AS create_statement,
                trigger_name,
                FALSE,
                rank
           FROM tt_triggers;

    -- Capture indexes on dependent objects
    WITH tt_indexes AS
    (
        SELECT pg_get_indexdef( ci.oid ) AS create_statement,
               'DROP INDEX ' || ci.relname::VARCHAR AS drop_statement,
               ci.relname::VARCHAR AS object_name,
               tt.rank + 1 AS rank
          FROM pg_index i
    INNER JOIN pg_class ci
            ON ci.oid = i.indexrelid
    INNER JOIN pg_class c
            ON c.oid = i.indrelid
    INNER JOIN pg_namespace n
            ON n.oid = c.relnamespace
    INNER JOIN tt_dependent_objects tt
            ON tt.object_name = COALESCE( n.nspname::VARCHAR, 'public' ) || '.' || c.relname::VARCHAR
           AND tt.is_base_obj IS TRUE
    )
    INSERT INTO tt_dependent_objects
                (
                    drop_statement,
                    create_statement,
                    object_name,
                    is_base_obj,
                    rank
                )
         SELECT drop_statement,
                create_statement,
                object_name,
                FALSE,
                rank
           FROM tt_indexes;

    FOR my_drop_statement IN(
        SELECT drop_statement
          FROM tt_dependent_objects
      ORDER BY rank DESC
    ) LOOP
        EXECUTE my_drop_statement;
    END LOOP;

    SELECT table_name
      INTO my_cache_table_name
      FROM tblmgr.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'DROP TABLE IF EXISTS ' || my_cache_table_name;
    EXECUTE 'ALTER TABLE ' || my_cache_table_name || '_temp RENAME TO ' || my_cache_table_name;

    FOR my_create_statement IN(
        SELECT create_Statement
          FROM tt_dependent_objects
      ORDER BY rank ASC
    ) LOOP
        EXECUTE my_create_statement;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';
