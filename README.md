Cache Table Manager (tblmgr)
============================

#Summary:

Tblmgr is an extension for PostgreSQL that seeks to bridge the gap between a
MATERIALIZED VIEW and TABLE.

The caveat of MATERIALIZED VIEWs that tblmgr seeks to overcome is the inability
for database users to access the view when a REFRESH MATERIALIZE VIEW operation
is underway (before PostgreSQL 9.4). Refresh times are also faster than that of
a full MATERIALIZED VIEW REFRESH.

Tblmgr implements this functionality by storing a query that would be used to
generate a MATERIALIZED VIEW, along with additional information, to maintain
the cache table while the base tables are being modified.

## Description:

Tblmgr allows the creation of denormalized cache tables. Tables are defined by
placing a row in the table tblmgr.tb_cache_table.

These cache tables are then managed by the tblmgr service, which aggregates
modifications to base tables as either an UPDATE, DELETE, or INSERT on the
cache tables.

Tblmgr is built around the async NOTIFY / LISTEN system of PostgreSQL. Triggers
placed on base tables fire when INSERT, UPDATE, or DELETE operations occur.
These triggers record the modified table's primary key information, along with
additional key information into tblmgr.tb_cache_update_request. INSERTs into
tblmgr.tb_cache_update_request trigger a NOTIFY, which tblmgr.pl LISTENs for.
When a notification occurs, tblmgr.pl consumes the rows in
tblmgr.tb_cache_update_request, and using the primary key and additional key
information, determines what cache tables need to be updated to reflect changes
in the base table.

## Requirements:

* Perl with the following modules:
    - DBI::Pg
    - JSON
    - IO::Select
    - IO::Handle
    - Sys::Syslog
    - Getopt::Std
    - Time::HiRes
    - POSIX
    - Data::Dumper
    - Readonly
    - Carp
    - Cwd
    - Params::Validate
    - English
    - IO::Interactive

## Installation:


1. Checkout the tblmgr source from bitbucket.
2. Ensure that pg_config is in the user's path.
3. As root, run 'make install' with tblmgr.control and Makefile in your cwd.
4. Login to database server as superuser.
5. Run: 'CREATE EXTENSION tblmgr'
6. Start tblmgr.pl process with connection parameters to target database

## Alternate Installation (AWS RDS)

1. Checkout the tblmgr source from bitbucket
2. Run the noext/tblmgr.sql file against the target database
3. Start tblmgr.pl process with connection parameters to target database

## Example Usage:

### Existing Schema:

```
CREATE TABLE tb_order_type
(
    order_type              INTEGER PRIMARY KEY,
    label                   VARCHAR
);

CREATE SEQUENCE sq_pk_location;
CREATE TABLE tb_location
(
    location                INTEGER PRIMARY KEY DEFAULT nextval( 'sq_pk_order' ),
    name                    VARCHAR,
    address_line_one        VARCHAR NOT NULL,
    address_line_two        VARCHAR,
    city                    VARCHAR NOT NULL,
    region                  VARCHAR NOT NULL,
    country                 VARCHAR NOT NULL,
    zip_postal              VARCHAR NOT NULL
);

CREATE SEQUENCE sq_pk_order;
CREATE TABLE tb_order
(
    orderid                 INTEGER PRIMARY KEY DEFAULT nextval( 'sq_pk_order' ),
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    modified                TIMESTAMP NOT NULL DEFAULT now(),
    submitted               TIMESTAMP,
    destination_location    INTEGER NOT NULL REFERENCES tb_location,
    order_type              INTEGER NOT NULL REFERENCE tb_order_type,
    shipped                 TIMESTAMP,
    tracking_number         VARCHAR,
    canceled                TIMESTAMP
);

CREATE SEQEUNCE sq_pk_article;
CREATE TABLE tb_article
(
    article                 INTEGER PRIMARY KEY DEFAULT nextval( 'sq_pk_article' ),
    upc                     VARCHAR NOT NULL,
    unit_price              FLOAT NOT NULL,
    name                    VARCHAR NOT NULL,
    description             TEXT,
    CHECK( unit_price > 0.00 )
);

CREATE SEQUENCE sq_pk_line_item;
CREATE TABLE tb_line_item
(
    line_item               INTEGER PRIMARY KEY DEFAULT nextval( 'sq_pk_line_item' ),
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    source_location         INTEGER NOT NULL REFERENCES tb_location,
    orderid                 INTEGER NOT NULL REFERENCES tb_order,
    article                 INTEGER NOT NULL REFERENCES tb_article,
    quantity                INTEGER NOT NULL,
    CHECK( quantity > 0 )
);

CREATE OR REPLACE FUNCTION fn_get_order_invoice_price
(
    in_orderid  INTEGER
)
RETURNS FLOAT AS
 $_$
    SELECT SUM( a.unit_price * li.quantity ) AS price
      FROM tb_order o
INNER JOIN tb_line_item li
        ON li.orderid = o.orderid
INNER JOIN tb_article a
        ON a.article = li.article
     WHERE o.orderid = in_orderid
  GROUP BY o.orderid;
 $_$
    LANGUAGE SQL STABLE;
```

### Tblmgr Usage:
```
INSERT INTO tblmgr.tb_cache_table
            (
                table_name,
                generation_query,
                unique_expression,
                group_by_expression,
                has_rollup_from_table,
                schema,
                table_columns,
                index_columns
            )
     VALUES
            (
                'ct_outstanding_orders',
                '
    SELECT o.orderid,
           COUNT( li.line_item )                    AS line_items,
           SUM( li.quantity )                       AS quantity,
           fn_get_order_invoice_price( o.orderid )  AS invoice_price,
           ot.label                                 AS order_type,
           dl.address_line_one || ' ' || 
        || COALESCE( dl.address_line_two || ' ', '' )
        || dl.city || ', '
        || dl.region || ' '
        || dl.zip_postal || ' '
        || dl.country                               AS destination
      FROM tb_order o
INNER JOIN tb_order_type ot
        ON ot.order_type = o.order_type
INNER JOIN tb_line_item li
        ON li.orderid = o.orderid
INNER JOIN tb_location dl
        ON dl.location = o.destination_location
     WHERE o.shipped IS NULL', -- generation_query
                ARRAY['orderid']::VARCHAR[], -- unique_expression
                ARRAY['o.orderid']::VARCHAR[], -- group_by_expression
                '{"public.tb_article":"public.tb_line_item"}'::JSON, -- has_rollup_from_table
                'public', -- schema
'{
    "public":{
        "tb_order":{
            "o":{
                "orderid":"orderid"
            }
        },
        "tb_order_type":{
            "ot":{
                "label":"order_type"
            }
        },
        "tb_location":{
            "dl":{
                "address_line_one":"destination",
                "address_line_two":"destination",
                "city":"destination",
                "region":"destination",
                "country":"destination",
                "zip_postal":"destination"
            }
        },
        "tb_line_item":{
            "li":{
                "line_item":"line_items",
                "quantity":"quantity"
            }
        }
    }
}'::JSON, -- table_columns
                '{"orderid":"BTREE"}'::JSON -- index_columns
            );
```

This is a simple but useful example as it demonstrates nearly all the functionality of tblmgr.
The query used to generate the cache table displays outstanding orders ( orders that have yet to be shipped / invoiced for our example system )
This involves a few joins, as well as a rollup ( `fn_get_order_invoice_price` ).

We'll start with the simple stuff:

- `generation_query` + `group_by_expression`: used together to populate the table
- `unique_expression`: identifies what column(s) in the output set should be considered unique. tblmgr will generate a UNIQUE INDEX on these column(s). This is required to be able to update tuples in `ct_outstanding_orders` as updates occur to `tb_order`, `tb_order_type`, `tb_location`, and/or `tb_line_item`
- `index_columns`: Tells tblmgr what indexes of what type to generate on the table.
- `table_columns`: JSON structure that tells tblmgr what tables and columns are used in the query.
    - First level: table schema
    - Second level: table name
    - Third level: alias used in query
    - Forth level: column name -> output alias
- `has_rollup_from_table`: JSON structure that tells tblmgr that some aggregation or function used in the query relies on a table not mentioned in `table_columns`. Tblmgr takes in the table listed as keys in the json structure, and treats modifications to it as if they happened to the tables listed as values. In this example, an update to `tb_article` is treated instead as an update to `tb_line_item`. The effective outcome is that an update to `tb_article.unit_price` is caught by tblmgr and updated accordingly in `ct_outstanding_orders`. This is referred internally as masqueraded updates, and the tables MUST be related by a foreign key
