ALTER TABLE @extschema@.tb_cache_update_request
    ADD COLUMN full_refresh_request BOOLEAN NOT NULL DEFAULT FALSE;

COMMENT ON COLUMN @extschema@.tb_cache_update_request.full_refresh_request IS
    'Initiates a full refresh on the cache table by inserting the tables name into tb_cache_update_request containing the cache table information with this flag set TRUE';

CREATE TEMP TABLE tt_migrate
(
    parent VARCHAR,
    child  VARCHAR
);

INSERT INTO tt_migrate
     SELECT ctp.table_name AS parent,
            ctc.table_name AS child
       FROM @extschema@.tb_cache_table ctp
 INNER JOIN @extschema@.tb_Cache_table ctc
         ON ctc.inherits = ctp.cache_table;

ALTER TABLE @extschema@.tb_cache_table
    DROP COLUMN inherits;

ALTER TABLE @extschema@.tb_cache_table
    ADD COLUMN inherits_table VARCHAR;

UPDATE @extschema@.tb_cache_table ct
   SET inherits_table = tt.parent
  FROM tt_migrate tt
 WHERE ct.table_name = tt.child;

DROP TABLE tt_migrate;

COMMENT ON COLUMN @extschema@.tb_cache_table.inherits_table IS
    'Identifies a cache table that is iherited by this cache table entry - moved from a FK to prevent problems when running pg_dump';

DROP FUNCTION IF EXISTS @extschema@.fn_refresh_cache_table( VARCHAR );

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table
(
    in_cache_table_name    VARCHAR
)
RETURNS BOOLEAN AS
 $_$
BEGIN
    IF NOT pg_try_advisory_xact_lock(
                    '@extschema@.tb_cache_table'::REGCLASS::INTEGER,
                    cache_table
                )
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
    THEN
        RAISE NOTICE 'Aborted attempt to refresh cache table, a refresh is already in progress';
        RETURN FALSE;
    END IF;

    -- Attempt to lock tb_cache_update_request globally. This is a tblmgr-level signal
    -- that there is a refresh currently underway
    IF NOT pg_try_advisory_xact_lock(
               '@extschema@.tb_cache_update_request'::REGCLASS::INTEGER
           ) THEN
        RAISE NOTICE 'Aborted attempt to refresh cache table, a refresh is already in progress';
        RETURN FALSE;
    END IF;

    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    created,
                    disable_flush,
                    full_refresh_request
                )
         SELECT schema,
                table_name,
                cache_table,
                now(),
                FALSE,
                TRUE
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name;

    RETURN TRUE;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    EXECUTE 'DELETE FROM @extschema@.tb_cache_table ctc '
         || '      USING @extschema@.tb_cache_table ct '
         || '      WHERE ctc.inherits_table = ' || quote_literal( OLD.table_name );

    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.schema || '.' || OLD.table_name || ' CASCADE';
    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_issue_cache_update_request()
RETURNS TRIGGER AS
 $_$
BEGIN
    IF( NEW.full_refresh_request IS FALSE ) THEN
        NOTIFY cache_update_request;
    ELSIF( NEW.full_refresh_request IS TRUE ) THEN
        NOTIFY full_refresh_request;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';
