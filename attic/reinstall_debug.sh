#!/bin/bash

set -e

[ -n "$1" ] || {
    echo "Must specify a database"
    exit 1
}

{
    echo "DELETE FROM tblmgr.tb_cache_table;"
    echo "DROP EXTENSION tblmgr CASCADE;"
} > "/tmp/remove_tblmgr.sql"

git pull origin mater
make install

psql -U postgres -d "$1" -1 -f /tmp/remove_tblmgr.sql
psql -U postgres -d "$1" -1 -c "CREATE EXTENSION tblmgr SCHEMA tblmgr;"
psql -U postgres -d "$1" -1 -f ./test/xerp/fn_get_top_level_location_type.sql
psql -U postgres -d "$1" -1 -f ./test/xerp/fn_get_reset_survey_status_counts_hstore.sql
psql -U postgres -d "$1" -1 -f ./test/xerp/fn_get_reset_issue_status_counts_hstore.sql
psql -U postgres -d "$1" -1 -f ./test/xerp/ct_pesd_filter.sql
psql -U postgres -d "$1" -1 -f ./test/xerp/ct_location_group.sql
psql -U postgres -d "$1" -1 -f ./test/xerp/ct_project_department.sql
