CREATE SEQUENCE @extschema@.sq_pk_cache_update_request MAXVALUE 2147483647 CYCLE;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_update_request
(
    cache_update_request    INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_cache_update_request' ),
    table_name              VARCHAR NOT NULL,
    primary_key             INTEGER NOT NULL,
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    op                      CHAR(1) NOT NULL
);

CREATE OR REPLACE FUNCTION @extschema@.fn_issue_cache_update_request()
RETURNS TRIGGER AS
 $_$
BEGIN
    NOTIFY cache_update_request;
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_cache_update_request
    AFTER INSERT ON @extschema@.tb_cache_update_request
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_issue_cache_update_request();

CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_table_name   VARCHAR;
    my_record       RECORD;
    my_pk_colname   VARCHAR;
    my_pk_value     INTEGER;
BEGIN
    my_table_name := TG_RELNAME::VARCHAR;

    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
    END IF;
    
    SELECT pga.attname
      INTO STRICT my_pk_colname
      FROM pg_index pgi
INNER JOIN pg_class pgc
        ON pgc.oid = pgi.indrelid
       AND pgc.oid = my_table_name::REGCLASS
INNER JOIN pg_attribute pga
        ON pga.attrelid = pgc.oid
       AND pga.attnum = ANY( pgi.indkey )
       AND pgi.indisprimary IS TRUE;
    
    EXECUTE 'SELECT $1.' || my_pk_colname
       INTO my_pk_value
      USING my_record;

    INSERT INTO @extschema@.tb_cache_update_request
                (
                    table_name,
                    primary_key,
                    op
                )
         VALUES
                (
                    my_table_name,
                    my_pk_value,
                    left( TG_OP, 1 )
                );

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request_from_rollup_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record               RECORD;
    my_table_name           VARCHAR;
    my_fk_colname           VARCHAR;
    my_target_table_name    VARCHAR;
    my_pk_value             INTEGER;
BEGIN
    --Masquerades a modification to the table the trigger is
    -- present on as an update to the table that the table has an FK to.
    
    my_table_name := TG_RELNAME::VARCHAR;
    
    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
    END IF;

    SELECT masquerades_for_table
      INTO STRICT my_target_table_name
      FROM @extschema@.tb_table_trigger
     WHERE table_name = my_table_name
       AND masquerades_for_table IS NOT NULL;

    SELECT ca.attname
      INTO my_fk_colname
      FROM pg_constraint pco
INNER JOIN pg_class pcl
        ON pcl.oid = pco.confrelid
       AND pcl.relname = my_target_table_name
INNER JOIN pg_class ccl
        ON ccl.oid = pco.conrelid
       AND ccl.relname IS DISTINCT FROM my_target_table_name
       AND ccl.relname = my_table_name
INNER JOIN pg_attribute ca
        ON ca.attrelid = pco.conrelid
       AND ca.attnum = ANY( pco.conkey )
     WHERE pco.contype = 'f'; -- FK

    EXECUTE 'SELECT $1.' || my_fk_colname
       INTO my_pk_value
      USING my_record;

    INSERT INTO @extschema@.tb_cache_update_request
                (
                    table_name,
                    primary_key,
                    op
                )
        VALUES
                (
                    my_target_table_name,
                    my_pk_value,
                    'U'
                );

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE SEQUENCE sq_pk_managed_table;

CREATE TABLE IF NOT EXISTS @extschema@.tb_managed_table
(
    managed_table           INTEGER PRIMARY KEY DEFAULT nextval('sq_pk_managed_table'),
    table_name              VARCHAR NOT NULL,
    generation_query        TEXT NOT NULL,
    unique_expression       VARCHAR[] NOT NULL,
    group_by_expression     VARCHAR[] DEFAULT NULL::VARCHAR[],
    has_distinct            BOOLEAN NOT NULL DEFAULT FALSE,
    has_rollup_from_table   JSON DEFAULT NULL::JSON,
    schema                  VARCHAR NOT NULL DEFAULT 'public',
    table_columns           JSON NOT NULL,
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    full_refresh_count      INTEGER NOT NULL DEFAULT 0,
    partial_refresh_count   INTEGER NOT NULL DEFAULT 0,
    last_full_refresh       TIMESTAMP,
    last_partial_refresh    TIMESTAMP,
    full_refresh_duration   INTERVAL,
    partial_refresh_duration INTERVAL
);

COMMENT ON TABLE @extschema@.tb_managed_table IS
    'Stores';

CREATE SEQUENCE sq_pk_table_trigger;

CREATE TABLE IF NOT EXISTS @extschema@.tb_table_trigger
(
    table_trigger           INTEGER PRIMARY KEY DEFAULT nextval('sq_pk_table_trigger'),
    schema                  VARCHAR NOT NULL DEFAULT 'public',
    table_name              VARCHAR NOT NULL,
    masquerades_for_table   VARCHAR DEFAULT NULL
);

CREATE UNIQUE INDEX ix_table_name_check ON @extschema@.tb_table_trigger( table_name, COALESCE( masquerades_for_table, ''::VARCHAR ) );

CREATE SEQUENCE sq_pk_managed_table_trigger;

CREATE TABLE IF NOT EXISTS @extschema@.tb_managed_table_trigger
(
    managed_table_trigger   INTEGER PRIMARY KEY DEFAULT nextval('sq_pk_managed_table_trigger'),
    managed_table           INTEGER NOT NULL REFERENCES @extschema@.tb_managed_table,
    table_trigger           INTEGER NOT NULL REFERENCES @extschema@.tb_table_trigger,
    UNIQUE( managed_table, table_trigger )
);

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_triggers()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_min_messages VARCHAR;
    my_trigger_name VARCHAR;
    my_record       RECORD;
BEGIN
    my_trigger_name := 'tr_queue_cache_update_request';
    
    IF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
 
        my_min_messages := current_setting( 'client_min_messages' );

        SET client_min_messages TO 'WARNING';

        IF( my_record.masquerades_for_table IS NOT NULL ) THEN
            EXECUTE 'DROP TRIGGER IF EXISTS ' || my_trigger_name || '_from_rollup_table'
                 || '    ON ' || my_record.schema || '.' || my_record.table_name;
        ELSE
            EXECUTE 'DROP TRIGGER IF EXISTS ' || my_trigger_name
                 || '    ON ' || my_record.schema || '.' || my_record.table_name;
        END IF;

        EXECUTE 'SET client_min_messages TO ' || my_min_messages;
    ELSIF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;

        IF( NEW.masquerades_for_table IS NULL ) THEN
            EXECUTE 'CREATE TRIGGER ' || my_trigger_name
                 || '    AFTER INSERT OR DELETE OR UPDATE '
                 || '    ON ' || my_record.schema || '.' || my_record.table_name
                 || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request();';
        ELSE
            EXECUTE 'CREATE TRIGGER ' || my_trigger_name || '_from_rollup_table'
                 || '    AFTER INSERT OR DELETE OR UPDATE '
                 || '    ON ' || my_record.schema || '.' || my_record.table_name
                 || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request_from_rollup_table();';
        END IF;
    END IF;

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_manage_triggers
    AFTER INSERT OR DELETE ON @extschema@.tb_table_trigger
    FOR EACH ROW EXECUTE PROCEDURE fn_manage_triggers();


CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_view
(
    in_view_name    VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_unique_expression    VARCHAR[];
    my_query                TEXT;
    my_group_by             TEXT;
    my_duration             INTERVAL;
    my_start                TIMESTAMP;
    my_min_messages         VARCHAR;
BEGIN
    SELECT clock_timestamp()
      INTO my_start;

    SELECT unique_expression,
           generation_query,
           COALESCE( array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR )
      INTO my_unique_expression,
           my_query,
           my_group_by
      FROM @extschema@.tb_managed_table
     WHERE table_name = in_view_name;

    my_min_messages := current_setting( 'client_min_messages' );
    EXECUTE 'CREATE TABLE ' || in_view_name || '_temp AS ( ' || my_query || my_group_by || ' )';
    EXECUTE 'DROP INDEX IF EXISTS ix_' || in_view_name;
    EXECUTE 'CREATE UNIQUE INDEX ix_' || in_view_name || ' ON ' || in_view_name || '_temp( ' || array_to_string( my_unique_expression, ',' ) || ' )';

    SET client_min_messages TO 'WARNING';
    EXECUTE 'DROP TABLE IF EXISTS ' || in_view_name;
    EXECUTE 'ALTER TABLE ' || in_view_name || '_temp RENAME TO ' || in_view_name;
    EXECUTE 'SET client_min_messages TO ' || my_min_messages;

    UPDATE @extschema@.tb_managed_table
       SET full_refresh_duration = COALESCE( full_refresh_duration, '0 seconds'::INTERVAL ) + ( clock_timestamp() - my_start )::INTERVAL,
           last_full_refresh     = clock_timestamp(),
           full_refresh_count    = full_refresh_count + 1
     WHERE table_name = in_view_name;
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_top_level_record     RECORD;
    my_middle_level_record  RECORD;
    my_bottom_level_record  RECORD;
    my_rollup_source        RECORD;
    my_test                 VARCHAR;
    my_pk_column            VARCHAR;
    my_pk_col_found         BOOLEAN;
    my_group_by_found       BOOLEAN;
    my_table_trigger        INTEGER;
BEGIN
    FOR my_top_level_record IN( SELECT key, value FROM json_each( NEW.table_columns ) ) LOOP
        -- Validate that key is a table_name
        SELECT oid::VARCHAR
          INTO my_test
          FROM pg_class
         WHERE relname = my_top_level_record.key;
        
        IF NOT FOUND THEN
            RAISE EXCEPTION 'JSON key % is not a valid table!', my_top_level_record.key;
        END IF;
        
        SELECT pga.attname::VARCHAR
          INTO my_pk_column
          FROM pg_index pgi
    INNER JOIN pg_class pgc
            ON pgc.oid = pgi.indrelid
           AND pgc.oid = my_top_level_record.key::REGCLASS
    INNER JOIN pg_attribute pga
            ON pga.attrelid = pgc.oid
           AND pga.attnum = ANY( pgi.indkey )
         WHERE pgi.indisprimary;

        IF NOT FOUND THEN
            RAISE EXCEPTION 'JSON key ( table % ) has no PK!', my_top_level_record.key;
        END IF;

        my_pk_col_found := FALSE;
        
        SELECT CASE WHEN NEW.group_by_expression IS NOT NULL
                    THEN TRUE
                    ELSE FALSE
                     END
          INTO my_group_by_found;

        IF( my_group_by_found IS FALSE AND NEW.has_distinct IS FALSE ) THEN
            -- If we are not grouping, we need to validate columns in the data hash, otherwise they'd be aggregates and
            -- difficult to validate ( requiring a more complex hash )
            FOR my_middle_level_record IN( SELECT key, value FROM json_each( my_top_level_record.value ) ) LOOP
                FOR my_bottom_level_record IN( SELECT key, value FROM json_each( my_middle_level_record.value ) ) LOOP
                    SELECT a.attname
                      INTO STRICT my_test
                      FROM pg_class c
                      JOIN pg_attribute a
                        ON a.attrelid = c.oid
                       AND a.attnum > 0
                     WHERE c.relname = my_top_level_record.key
                       AND a.attname = my_bottom_level_record.key;
                    
                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'JSON invalid! column % is not present on %', my_bottom_level_record.key, my_top_level_record.key;
                    END IF;
                    
                    IF( my_bottom_level_record.key = my_pk_column ) THEN
                        my_pk_col_found := TRUE;
                    END IF;
                END LOOP;
            END LOOP;
            
            IF( my_pk_col_found IS FALSE ) THEN
                RAISE EXCEPTION 'Primary key for table % (%)  MUST be in the query output!', my_top_level_record.key, my_pk_column;
            END IF;
        END IF;

        -- Create UPDATE trigger on all table.column, INSERT, DELETE records on table
        my_test := NULL;

        SELECT table_name
          INTO my_test
          FROM @extschema@.tb_table_trigger
         WHERE table_name = my_top_level_record.key;
        
        IF ( my_test IS NULL ) THEN
            INSERT INTO @extschema@.tb_table_trigger
                        (
                            schema,
                            table_name
                        )
                 VALUES
                        (
                            NEW.schema,
                            my_top_level_record.key
                        )
              RETURNING table_trigger
                   INTO my_table_trigger;

            INSERT INTO @extschema@.tb_managed_table_trigger
                        (
                            managed_table,
                            table_trigger
                        )
                 VALUES
                        (
                            NEW.managed_table,
                            my_table_trigger
                        );
        END IF;
    END LOOP;
        
    -- Create triggers that handle rollup functions
    IF( NEW.has_rollup_from_table IS NOT NULL ) THEN
        FOR my_rollup_source IN(
                                   SELECT je.*
                                     FROM json_array_elements( NEW.has_rollup_from_table ) tt,
                                  LATERAL (
                                              SELECT key,
                                                     value
                                                FROM json_each( tt.value )
                                          ) je
                               ) LOOP
            
            INSERT INTO @extschema@.tb_table_trigger
                        (
                            schema,
                            table_name,
                            masquerades_for_table
                        )
                 VALUES
                        (
                            NEW.schema,
                            my_rollup_source.key::VARCHAR,
                            regexp_replace( my_rollup_source.value::VARCHAR, '"', '', 'g' ) -- fix issue where json text values are returned wrapped in "
                        )
              RETURNING table_trigger
                   INTO my_table_trigger;

            INSERT INTO @extschema@.tb_managed_table_trigger
                        (
                            managed_table,
                            table_trigger
                        )
                 VALUES
                        (
                            NEW.managed_table,
                            my_table_trigger
                        );
        END LOOP;
    END IF;

    PERFORM @extschema@.fn_refresh_view( NEW.table_name::VARCHAR );

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_validate_table_columns
    AFTER INSERT OR UPDATE OF table_columns ON @extschema@.tb_managed_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_validate_table_columns();

CREATE OR REPLACE FUNCTION fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.table_name || ' CASCADE;';
    
    FOR my_trigger_record IN(
                                WITH tt_last_remaining_triggers AS
                                (
                                    SELECT table_trigger,
                                           COUNT( managed_table ) AS managed_table_count,
                                           array_agg( managed_table ) AS managed_tables
                                      FROM @extschema@.tb_managed_table_trigger
                                  GROUP BY table_trigger
                                    HAVING COUNT( managed_table ) = 1
                                )
                                    SELECT table_trigger
                                      FROM tt_last_remaining_triggers
                                     WHERE OLD.managed_table = ANY( managed_tables )
                            ) LOOP 
        DELETE FROM @extschema@.tb_table_trigger
              WHERE table_trigger = my_trigger_record.table_trigger;
    END LOOP;

    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_drop_table
    BEFORE DELETE ON @extschema@.tb_managed_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_drop_table();

GRANT USAGE ON SCHEMA tblmgr TO PUBLIC;
GRANT SELECT, UPDATE, DELETE, INSERT ON ALL TABLES IN SCHEMA tblmgr TO PUBLIC;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA tblmgr TO PUBLIC;
