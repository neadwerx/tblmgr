SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_managed_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_managed_table', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_managed_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_managed_table', '' );

ALTER TABLE @extschema@.tb_managed_table_trigger
    DROP CONSTRAINT tb_managed_table_trigger_managed_table_fkey;

ALTER TABLE @extschema@.tb_managed_table_trigger
    ADD CONSTRAINT tb_managed_table_trigger_managed_table_fkey FOREIGN KEY (managed_table) REFERENCES @extschema@.tb_managed_table(managed_table) DEFERRABLE;

CREATE SEQUENCE @extschema@.sq_pk_table_index;

CREATE TABLE IF NOT EXISTS @extschema@.tb_table_index
(
    table_index     INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_table_index' ),
    managed_table   INTEGER NOT NULL REFERENCES @extschema@.tb_managed_table DEFERRABLE,
    name            VARCHAR NOT NULL,
    definition      VARCHAR NOT NULL,
    is_unique       BOOLEAN NOT NULL DEFAULT FALSE
);

SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_table_index', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_table_index', '' );

ALTER TABLE @extschema@.tb_managed_table
    ADD COLUMN inherits INTEGER REFERENCES @extschema@.tb_managed_table;

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_index()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_table_name   VARCHAR;
    my_unique       VARCHAR;
BEGIN
    IF( TG_OP = 'DELETE' ) THEN
        EXECUTE 'DROP INDEX IF EXISTS ' || OLD.name;

        RETURN OLD;
    ELSIF( TG_OP = 'INSERT' OR TG_OP = 'UPDATE' ) THEN
        SELECT mt.table_name,
               CASE WHEN ti.is_unique IS TRUE
                    THEN 'UNIQUE'
                    ELSE ''
                     END
          INTO my_table_name,
               my_unique
          FROM @extschema@.tb_table_index ti
          JOIN @extschema@.tb_managed_table mt
            ON mt.managed_table = ti.managed_table
         WHERE ti.table_index = NEW.table_index;

        EXECUTE 'CREATE ' || my_unique || ' INDEX ON @extschema@.' || my_table_name || '( ' || NEW.definition || ' )';

        RETURN NEW;
    END IF;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_manage_indexes
    AFTER INSERT OR DELETE OR UPDATE ON @extschema@.tb_table_index
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_manage_index();

CREATE OR REPLACE FUNCTION @extschema@.fn_analyze_table
(
    in_table_name   VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_table_has_json   BOOLEAN;
    my_functions_exist  BOOLEAN;
BEGIN
    my_table_has_json := FALSE;

    SELECT 'JSON'::REGTYPE::NAME = ANY( array_agg( t.typname ) )
      INTO my_table_has_json
      FROM pg_class c
      JOIN pg_attribute a
        ON a.attrelid = c.oid
       AND a.attnum > 0
      JOIN pg_type t
        ON t.oid = a.atttypid
     WHERE c.relname = in_table_name;

    IF( my_table_has_json IS TRUE ) THEN
        SELECT CASE WHEN COUNT( proname ) = 2
                    THEN TRUE
                    ELSE FALSE
                     END
          INTO my_functions_exist
          FROM pg_proc
         WHERE proname IN( 'fn_disable_json_equals_operator', 'fn_enable_json_equals_operator' );

        IF( my_functions_exist IS TRUE ) THEN
            PERFORM fn_disable_json_equals_operator();
            EXECUTE 'ANALYZE ' || in_table_name;
            PERFORM fn_enable_json_equals_operator();
        ELSE
            RAISE NOTICE 'JSON class op disable functions do no exist - ANALYZE will take a while.';
            EXECUTE 'ANALYZE ' || in_table_name || ' VERBOSE';
        END IF;
    ELSE
        EXECUTE 'ANALYZE ' || in_table_name;
    END IF;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_view
(
    in_view_name    VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_unique_expression    VARCHAR[];
    my_query                TEXT;
    my_group_by             TEXT;
    my_duration             INTERVAL;
    my_start                TIMESTAMP;
    my_min_messages         VARCHAR;
    inherit_flag            BOOLEAN;
    my_managed_table        INTEGER;
    my_parent_table         INTEGER;
    my_unique               VARCHAR;
    my_child_table          RECORD;
    my_index_row            RECORD;
BEGIN
    SELECT clock_timestamp()
      INTO my_start;
     
    SELECT unique_expression,
           generation_query,
           COALESCE( 'GROUP BY ' || array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR ),
           managed_table,
           inherits
      INTO my_unique_expression,
           my_query,
           my_group_by,
           my_managed_table,
           my_parent_table
      FROM @extschema@.tb_managed_table
     WHERE table_name = in_view_name;

    IF( my_managed_table IS NULL ) THEN
        RAISE NOTICE 'TBLMGR: Table % is not managed.', in_view_name;
        RETURN;
    END IF;
    
    IF( my_parent_table IS NOT NULL ) THEN
        RAISE NOTICE 'TBLMGR: Deferring table refresh to parent table';
        PERFORM @extschema@.fn_refresh_view( table_name )
           FROM @extschema@.tb_managed_table
          WHERE managed_table = my_parent_table;
        RETURN;
    END IF;
    
    --Handle the case where the target table is inherited
    inherit_flag := FALSE;

    SELECT CASE WHEN COUNT( managed_table ) > 0
                THEN TRUE
                ELSE FALSE
                 END AS is_inherited
      INTO inherit_flag
      FROM @extschema@.tb_managed_table
     WHERE inherits = my_managed_table;
   
    my_min_messages := current_setting( 'client_min_messages' );
    SET client_min_messages TO 'WARNING';

    EXECUTE 'CREATE TABLE ' || in_view_name || '_temp AS ( ' || my_query || my_group_by || ' )';
    EXECUTE 'DROP INDEX IF EXISTS ix_' || in_view_name;
    EXECUTE 'CREATE UNIQUE INDEX ix_' || in_view_name || '_temp ON ' || in_view_name || '_temp( ' || array_to_string( my_unique_expression, ',' ) || ')';

    FOR my_index_row IN( SELECT * FROM @extschema@.tb_table_index WHERE managed_table = my_managed_table ) LOOP
        SELECT CASE WHEN is_unique IS TRUE
                    THEN 'UNIQUE'::VARCHAR
                    ELSE ''::VARCHAR
                     END
          INTO my_unique
          FROM @extschema@.tb_table_index
         WHERE table_index = my_index_row.table_index;

        EXECUTE 'CREATE ' || my_unique || ' INDEX ' || my_index_row.name || '_temp ON ' || in_view_name || '_temp ( ' || my_index_row.definition || ' )';
    END LOOP;

    IF( inherit_flag IS TRUE ) THEN
        FOR my_child_table IN( SELECT * FROM @extschema@.tb_managed_table WHERE inherits = my_managed_table ) LOOP
            EXECUTE 'CREATE TABLE ' || my_child_table.table_name || '_temp () INHERITS ( ' || in_view_name || '_temp )';
            EXECUTE 'INSERT INTO ' || my_child_table.table_name || '_temp '
                  || my_child_table.generation_query || COALESCE( 'GROUP BY ' || array_to_string( my_child_table.group_by_expression, ',' ), '' );
            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_child_table.table_name || '_temp ON ' || my_child_table.table_name || '_temp( ' || array_to_string( my_child_table.unique_expression, ',' ) || ')';

            FOR my_index_row IN( SELECT * FROM @extschema@.tb_table_index WHERE managed_table = my_child_table.managed_table ) LOOP
                SELECT CASE WHEN is_unique IS TRUE
                            THEN 'UNIQUE'::VARCHAR
                            ELSE ''::VARCHAR
                             END
                  INTO my_unique
                  FROM @extschema@.tb_table_index
                 WHERE table_index = my_index_row.table_index;

                EXECUTE 'CREATE ' || my_unique || ' INDEX ' || my_index_row.name || '_temp ON ' || my_child_table.table_name || '_temp ( ' || my_index_row.definition || ' )';
            END LOOP;
        END LOOP;
    END IF;

    IF( inherit_flag IS TRUE ) THEN
        EXECUTE 'DROP TABLE IF EXISTS ' || in_view_name || ' CASCADE';
        
        -- Handle case where table inheritance is broken - manually remove the inherited table
        FOR my_child_table IN( SELECT * FROM @extschema@.tb_managed_table WHERE inherits = my_managed_table ) LOOP
            PERFORM relname
               FROM pg_class
              WHERE relkind = 'r'
                AND relname = my_child_table.table_name;

            IF FOUND THEN
                EXECUTE 'DROP TABLE IF EXISTS ' || my_child_table.table_name;
            END IF;
        END LOOP;
    ELSE
        EXECUTE 'DROP TABLE IF EXISTS ' || in_view_name;
    END IF;

    EXECUTE 'ALTER TABLE ' || in_view_name || '_temp RENAME TO ' || in_view_name;
    EXECUTE 'ALTER INDEX ix_' || in_view_name || '_temp RENAME TO ix_' || in_view_name;

    PERFORM @extschema@.fn_analyze_table( in_view_name );

    IF( inherit_flag IS TRUE ) THEN
        FOR my_child_table IN( SELECT * FROM @extschema@.tb_managed_table WHERE inherits = my_managed_table ) LOOP
            EXECUTE 'ALTER TABLE ' || my_child_table.table_name || '_temp RENAME TO ' || my_child_table.table_name;
            EXECUTE 'ALTER INDEX ix_' || my_child_table.table_name || '_temp RENAME TO ix_' || my_child_table.table_name;
        
            FOR my_index_row IN( SELECT * FROM @extschema@.tb_table_index WHERE managed_table = my_child_table.managed_table ) LOOP
                EXECUTE 'ALTER INDEX ' || my_index_row.name || '_temp RENAME TO ' || my_index_row.name;
            END LOOP;
            
            PERFORM @extschema@.fn_analyze_table( my_child_table.table_name );
        END LOOP;
    END IF;
    
    EXECUTE 'SET client_min_messages TO ' || my_min_messages;

    UPDATE @extschema@.tb_managed_table
       SET full_refresh_duration = COALESCE( full_refresh_duration, '0 seconds'::INTERVAL ) + ( clock_timestamp() - my_start )::INTERVAL,
           last_full_refresh     = clock_timestamp(),
           full_refresh_count    = full_refresh_count + 1
     WHERE table_name = in_view_name;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table
(
    in_table_name   VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    SET CONSTRAINTS ALL DEFERRED;
    EXECUTE 'DROP TABLE IF EXISTS ' || in_table_name || ' CASCADE';

    DELETE FROM @extschema@.tb_table_index ti
          USING @extschema@.tb_managed_table mt
          WHERE mt.managed_table = ti.managed_table
            AND mt.table_name = in_table_name;
    DELETE FROM @extschema@.tb_managed_table_trigger mtt
         USING @extschema@.tb_managed_table mt
         WHERE mt.managed_table = mtt.managed_table
           AND mt.table_name = in_table_name; 

    FOR my_trigger_record IN(
                                WITH tt_last_remaining_triggers AS
                                (
                                    SELECT table_trigger,
                                           COUNT( managed_table ) AS managed_table_count,
                                           array_agg( managed_table ) AS managed_tables
                                      FROM @extschema@.tb_managed_table_trigger
                                  GROUP BY table_trigger
                                    HAVING COUNT( managed_table ) = 1
                                )
                                    SELECT tt.table_trigger
                                      FROM tt_last_remaining_triggers tt
                                      JOIN @extschema@.tb_managed_table mt
                                        ON mt.managed_table = ANY( managed_tables )
                                       AND mt.table_name = in_table_name
                            ) LOOP
        DELETE FROM @extschema@.tb_table_trigger
              WHERE table_trigger = my_trigger_record.table_trigger;
    END LOOP;
    DELETE FROM @extschema@.tb_managed_table mtc
          USING @extschema@.tb_managed_table mt
          WHERE mtc.inherits = mt.managed_table
            AND mt.table_name = in_table_name;
    DELETE FROM @extschema@.tb_managed_table WHERE table_name = in_table_name;
    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    SET CONSTRAINTS ALL DEFERRED;
    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.table_name || ' CASCADE';
    EXECUTE 'DELETE FROM @extschema@.tb_table_index WHERE managed_table = ' || OLD.managed_table;

    DELETE FROM @extschema@.tb_managed_table_trigger mtt
          WHERE managed_table = OLD.managed_table;
    FOR my_trigger_record IN(
                                WITH tt_last_remaining_triggers AS
                                (
                                    SELECT table_trigger,
                                           COUNT( managed_table ) AS managed_table_count,
                                           array_agg( managed_table ) AS managed_tables
                                      FROM @extschema@.tb_managed_table_trigger
                                  GROUP BY table_trigger
                                    HAVING COUNT( managed_table ) = 1
                                )
                                    SELECT table_trigger
                                      FROM tt_last_remaining_triggers
                                     WHERE OLD.managed_table = ANY( managed_tables )
                            ) LOOP
        DELETE FROM @extschema@.tb_table_trigger
              WHERE table_trigger = my_trigger_record.table_trigger;
    END LOOP;
    
    EXECUTE 'DELETE FROM @extschema@.tb_managed_table mtc '
         || '      USING @extschema@.tb_managed_table mt '
         || '      WHERE mtc.inherits = ' || OLD.managed_table;
    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_view_trigger_wrapper()
RETURNS TRIGGER AS
 $_$
BEGIN
    PERFORM @extschema@.fn_refresh_view( NEW.table_name );
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_handle_table_definition_change
    AFTER UPDATE OF generation_query, group_by_expression, has_distinct, unique_expression ON @extschema@.tb_managed_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_refresh_view_trigger_wrapper();

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_top_level_record     RECORD;
    my_middle_level_record  RECORD;
    my_bottom_level_record  RECORD;
    my_rollup_source        RECORD;
    my_test                 VARCHAR;
    my_pk_column            VARCHAR;
    my_pk_col_found         BOOLEAN;
    my_group_by_found       BOOLEAN;
    my_table_trigger        INTEGER;
BEGIN
    FOR my_top_level_record IN( SELECT key, value FROM json_each( NEW.table_columns ) ) LOOP
        -- Validate that key is a table_name
        SELECT oid::VARCHAR
          INTO my_test
          FROM pg_class
         WHERE relname = my_top_level_record.key;

        IF NOT FOUND THEN
            RAISE EXCEPTION 'JSON key % is not a valid table!', my_top_level_record.key;
        END IF;

        SELECT pga.attname::VARCHAR
          INTO my_pk_column
          FROM pg_index pgi
    INNER JOIN pg_class pgc
            ON pgc.oid = pgi.indrelid
           AND pgc.oid = my_top_level_record.key::REGCLASS
    INNER JOIN pg_attribute pga
            ON pga.attrelid = pgc.oid
           AND pga.attnum = ANY( pgi.indkey )
         WHERE pgi.indisprimary;

        IF NOT FOUND THEN
            RAISE EXCEPTION 'JSON key ( table % ) has no PK!', my_top_level_record.key;
        END IF;

        my_pk_col_found := FALSE;

        SELECT CASE WHEN NEW.group_by_expression IS NOT NULL
                    THEN TRUE
                    ELSE FALSE
                     END
          INTO my_group_by_found;

        IF( my_group_by_found IS FALSE AND NEW.has_distinct IS FALSE ) THEN
            -- If we are not grouping, we need to validate columns in the data hash, otherwise they'd be aggregates and
            -- difficult to validate ( requiring a more complex hash )
            FOR my_middle_level_record IN( SELECT key, value FROM json_each( my_top_level_record.value ) ) LOOP
                FOR my_bottom_level_record IN( SELECT key, value FROM json_each( my_middle_level_record.value ) ) LOOP
                    SELECT a.attname
                      INTO STRICT my_test
                      FROM pg_class c
                      JOIN pg_attribute a
                        ON a.attrelid = c.oid
                       AND a.attnum > 0
                     WHERE c.relname = my_top_level_record.key
                       AND a.attname = my_bottom_level_record.key;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'JSON invalid! column % is not present on %', my_bottom_level_record.key, my_top_level_record.key;
                    END IF;

                    IF( my_bottom_level_record.key = my_pk_column ) THEN
                        my_pk_col_found := TRUE;
                    END IF;
                END LOOP;
            END LOOP;

            IF( my_pk_col_found IS FALSE ) THEN
                RAISE EXCEPTION 'Primary key for table % (%)  MUST be in the query output!', my_top_level_record.key, my_pk_column;
            END IF;
        END IF;

        -- Create UPDATE trigger on all table.column, INSERT, DELETE records on table
        my_test := NULL;

        SELECT table_name
          INTO my_test
          FROM @extschema@.tb_table_trigger
         WHERE table_name = my_top_level_record.key;
          
        IF ( my_test IS NULL ) THEN
            INSERT INTO @extschema@.tb_table_trigger
                        (
                            schema,
                            table_name
                        )   
                 VALUES     
                        (
                            NEW.schema,
                            my_top_level_record.key
                        )   
              RETURNING table_trigger
                   INTO my_table_trigger;
              
            INSERT INTO @extschema@.tb_managed_table_trigger
                        (
                            managed_table,
                            table_trigger
                        )   
                 VALUES     
                        (
                            NEW.managed_table,
                            my_table_trigger
                        );  
        ELSE
            SELECT table_trigger
              INTO my_table_trigger
              FROM @extschema@.tb_table_trigger
             WHERE table_name = my_top_level_record.key;

            INSERT INTO @extschema@.tb_managed_table_trigger
                        (
                            managed_table,
                            table_trigger
                        )   
                 VALUES     
                        (
                            NEW.managed_table,
                            my_table_trigger
                        );  
        END IF;
    END LOOP;           
        
    -- Create triggers that handle rollup functions
    IF( NEW.has_rollup_from_table IS NOT NULL ) THEN
        FOR my_rollup_source IN(
                                   SELECT je.*
                                     FROM json_array_elements( NEW.has_rollup_from_table ) tt,
                                  LATERAL (
                                              SELECT key,
                                                     value
                                                FROM json_each( tt.value )
                                          ) je       
                               ) LOOP
            my_test := NULL;
            my_table_trigger := NULL;

            SELECT table_trigger
              INTO my_table_trigger
              FROM @extschema@.tb_table_trigger
             WHERE masquerades_for_table = regexp_replace( my_rollup_source.value::VARCHAR, '"', '', 'g' ) -- fix issue where json text values are returned wrapped in "
               AND table_name = my_rollup_source.key::VARCHAR
               AND schema = NEW.schema;
            
            IF( my_table_trigger IS NULL ) THEN
                INSERT INTO @extschema@.tb_table_trigger
                            (
                                schema,
                                table_name,
                                masquerades_for_table
                            )   
                     VALUES     
                            (
                                NEW.schema,
                                my_rollup_source.key::VARCHAR,
                                regexp_replace( my_rollup_source.value::VARCHAR, '"', '', 'g' ) -- fix issue where json text values are returned wrapped in "
                            )
                  RETURNING table_trigger
                       INTO my_table_trigger;
            END IF;
              
            INSERT INTO @extschema@.tb_managed_table_trigger
                        (
                            managed_table,
                            table_trigger
                        )
                 VALUES
                        (
                            NEW.managed_table,
                            my_table_trigger
                        );
        END LOOP;
    END IF;

    PERFORM @extschema@.fn_refresh_view( NEW.table_name::VARCHAR );

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';
