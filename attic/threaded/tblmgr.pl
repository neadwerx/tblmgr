#!/usr/bin/perl

$| = 1;

use utf8;
use strict;
use warnings;

use DBI;
use JSON;
use IO::Select;
use IO::Handle;
use Sys::Syslog;
use Getopt::Std;
use Time::HiRes qw( gettimeofday tv_interval );
use POSIX qw( strftime setsid :sys_wait_h );
use Data::Dumper;
use Readonly;
use Carp;

Readonly::Scalar my $LISTEN_CHANNEL      => 'cache_update_request';
Readonly::Scalar my $DEBUG               => 1;
Readonly::Scalar my $SEGMENT_PERCENTAGE  => 0.01;

my( $conn_map, $handle, $sel, $file_descriptor );
my $_verbose = 0;
my $_timing  = 0;
my $_schema  = 'tblmgr';
my $child_pid = 0;

my $_children = { };

sub update($)
{
    my( $msg ) = @_;
    syslog( 'info', "$msg" ) if( $msg );
    print "$msg\n" if( $msg and -t STDIN );
    return;
}

sub setup_status_file()
{
    openlog( 'tblmgr', '', 'user' );
    return;
}

sub daemonize()
{
    $SIG{__DIE__} = sub
    {
        update( "ERROR: " . $_[0] );
        exit 1;
    };

    $SIG{__WARN__} = sub
    {
        update( "WARNING: " . $_[0] );
    };

    chdir '/' or croak "Can't chdir to /: $!";
    open STDIN, '/dev/null'     or croak( "Can't read /dev/null: $!" );
    open STDOUT, '>/dev/null'   or croak( "Can't write to /dev/null: $!" );
    defined( my $pid = fork() ) or croak( "Can't fork: $!" );
    exit 0 if $pid;
    setsid()                    or croak( "Can't start a new session: $!" );
}

sub manage_pid_file($$)
{
    my( $pid, $pid_file ) = @_;

    if( -e $pid_file )
    {
        croak( "Lock file '$pid_file' exists, but is not a regular file!" ) unless( -f $pid_file );
        croak( "Cannot read lock file '$pid_file'" ) unless( -r $pid_file );

        open( PID, "<$pid_file" ) or croak( "Could not open existing PID file '$pid_file': $!" );
        chomp( my $line = <PID> );
        close( PID );

        my $existing_pid;

        ( $existing_pid ) = ( $line =~ /^(\d+)$/ );

        if(
                defined $existing_pid
            and ( $existing_pid =~ /^\d+$/ )
            and $existing_pid > 0
          )
        {
            my $process_found = 0;
            my $ps_command    = '/bin/ps -e |';

            unless( open( PS, $ps_command ) )
            {
                croak( "cannot check for existing processes: $!" );
            }

            while( <PS> )
            {
                my( $check_pid ) = ( $_ =~ /^\D+(\d+)\D.*$/ );

                next unless( defined $check_pid and $check_pid =~ /^\d+$/ and $check_pid > 0 );

                if( int( $existing_pid ) == int( $check_pid ) )
                {
                    $process_found = 1;
                    last;
                }
            }

            close PS;

            if( $process_found )
            {
                croak( "Service already running! PID ($existing_pid)" );
            }
            else
            {
                unlink( $pid_file ) or croak( "Could not remove stale PID file: $!" );
                open( PID, ">$pid_file" ) or croak( "Could not open PID file: $!" );
                print( PID $pid );
                close( PID );
            }
        }
    }
    else
    {
        open( PID, ">$pid_file" ) or croak( "Could not open new PID file '$pid_file': $!" );
        print( PID $pid );
        close( PID );
    }

    return;
}

sub try_query($)
{
    my( $query ) = @_;
    my $sth;

    until( ( $sth = $handle->prepare( $query ) ) and $sth->execute() )
    {
        return $sth if( $handle->ping() > 0 );

        until( $handle and $handle->ping() > 0 )
        {
            update( 'DB connection is down!' ) if( $_verbose or $DEBUG );
            sleep 0.5;
            $handle = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef );
        }

        update( 'Reconnected to database' ) if( $_verbose or $DEBUG );
        $handle->do( "LISTEN $LISTEN_CHANNEL" );
        $file_descriptor = $handle->func( 'getfd' );
        $sel             = IO::Select->new( $file_descriptor );
    }

    return $sth;
}

sub get_pk_column_name($)
{
    my( $table_name ) = @_;

    my $pk_col;

    my $q = <<SQL;
    SELECT pga.attname AS pk_col
      FROM pg_index pgi
INNER JOIN pg_class pgc
        ON pgc.oid = pgi.indrelid
       AND pgc.oid = '${table_name}'::REGCLASS
INNER JOIN pg_attribute pga
        ON pga.attrelid = pgc.oid
       AND pga.attnum = ANY( pgi.indkey )
     WHERE pgi.indisprimary
SQL
    my $sth = try_query( $q );
    my $row = $sth->fetchrow_hashref();
       $pk_col = $row->{'pk_col'};

    return $pk_col;
}

sub get_table_columns($;$)
{
    my( $table_name, $exclude_columns ) = @_;

    my $exclude = '';

    if(
            defined $exclude_columns
        and ref( $exclude_columns ) eq 'ARRAY'
        and scalar( @$exclude_columns ) > 0
      )
    {
        $exclude = "AND pga.attname NOT IN( '" . join( "','", @$exclude_columns ) . "' )";
    }

    my @return_columns;

    my $query = <<SQL;
    SELECT pga.attname
      FROM pg_attribute pga
INNER JOIN pg_class pgc
        ON pgc.oid = pga.attrelid
       AND pgc.relname = '$table_name'
     WHERE pga.attnum > 0
           $exclude
  ORDER BY pga.attnum
SQL

    update( $query ) if( $DEBUG );
    my $sth = try_query( $query );

    while( my $row = $sth->fetchrow_hashref() )
    {
        push( @return_columns, $row->{attname} );
    }

    return @return_columns;
}

sub process_cache_table_updates()
{

}

sub usage($)
{
    my( $msg ) = @_;
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0\n";
    print " -d dbname\n";
    print " -U username\n";
    print " -h host\n";
    print " [ -p port ]\n";
    print " [ -P pid_file ]\n";
    print " [ -s tblmgr_schema (default: tblmgr)\n";
    print " [ -D ( do not daemonize ) ]\n";
    print " [ -V ( verbose ) ]\n";
    print " [ -T ( show timings ) ]\n";
    exit 1;
}

sub main()
{
    our( $opt_D, $opt_d, $opt_U, $opt_h, $opt_p, $opt_P, $opt_V, $opt_T, $opt_s );
    my @original_argv = @ARGV;

    usage( 'Invalid arguments' ) unless( getopts( 'd:U:h:p:P:s:DVT' ) );

    my $dbname       = $opt_d;
    my $host         = $opt_h;
    my $user         = $opt_U;
    my $port         = $opt_p;
    my $no_daemonize = $opt_D;
    my $pid_file     = $opt_P;
       $_schema      = $opt_s if( $opt_s );
       $_verbose     = $opt_V if( $opt_V );
       $_timing      = $opt_T if( $opt_T );

    $port = 5432 unless( defined $port );
    usage( 'Invalid port' ) if( defined $port and ($port !~ /^\d+$/ or $port < 1 or $port > 65536) );
    usage( 'Invalid dbname'   ) unless( defined $dbname and length( $dbname ) > 0 );
    usage( 'Invalid username' ) unless( defined $user   and length( $user   ) > 0 );
    usage( 'Invalid hostname' ) unless( defined $host   and length( $host   ) > 0 );

    unless( defined $pid_file and length( $pid_file ) > 0 )
    {
        $pid_file = "/var/run/tblmgr.$dbname.pid";
    }

    daemonize() unless( $no_daemonize );
    setup_status_file();

    my $pid = $$;

    manage_pid_file( $pid, $pid_file );
    
    # Fork refresher process
    my $check_pid = fork();
    if( defined( $check_pid ) and $check_pid == 0 )
    {
        $_children->{refresher} = $check_pid;
    }
    elsif( defined $check_pid and $check_pid > 0 )
    {
        my $command_string = "./tblmgr_refresher.pl @original_argv";
        system( $command_string );
        exit 1;
    }
    else
    {
        update( "TBLMGR ERROR: Failed to fork and start refresher service" );
        exit 1;
    }

    my $conn_string = "dbi:Pg:dbname=${dbname};host=${host};port=${port}";
    $conn_map->{conn_string} = $conn_string;
    $conn_map->{user}        = $user;

    $handle = DBI->connect( $conn_string, $user, undef ) or croak( 'Could not connect to database' );

    update( 'STARTUP: Checking for cache update requests' ) if( $_verbose );
    process_cache_table_updates();

    update( "STARTUP: Listening on channel '$LISTEN_CHANNEL'" ) if( $_verbose );
    $handle->do( "LISTEN $LISTEN_CHANNEL" );
    $file_descriptor = $handle->func( 'getfd' );
    $sel             = IO::Select->new( $file_descriptor );

    my( $update_process_start, $update_finished, $update_duration );
    my( $time_since_last_notify, $last_notify_time );

    while(1)
    {
        update( 'Pre WAIT for NOTIFY' ) if( $DEBUG );

        $last_notify_time = [gettimeofday];

        update( "Waiting for NOTIFY on '$LISTEN_CHANNEL'" ) if( $_verbose );

        $sel->can_read; # Blocks and waits
        my $notify = $handle->func( 'pg_notifies' );
        update( 'Checking notify' ) if( $DEBUG );
        update( 'Processing notify...' ) if( $_verbose );
        $time_since_last_notify = tv_interval( $last_notify_time, [gettimeofday] );
        update( "Time since last notify: $time_since_last_notify" ) if( $_timing );

        if( $notify )
        {
            my( $relname, $pid ) = @$notify;

            if( $relname eq $LISTEN_CHANNEL )
            {
                $update_process_start = [gettimeofday];
                process_cache_table_updates();

                if( $_timing or $_verbose )
                {
                    $update_finished      = [gettimeofday];
                    $update_duration      = tv_interval( $update_process_start, $update_finished );
                    update( "Time to process last NOTIFY: $update_duration seconds" );
                }
            }
        }
        else
        {
            try_query( 'SELECT 1' );
        }

        sleep( 0.25 );
    }

    return;
}

main();
waitpid( $child_pid, 0 ) if( $child_pid );
exit 0;
