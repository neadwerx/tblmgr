CREATE SEQUENCE @extschema@.sq_pk_cache_update_request MAXVALUE 2147483647 CYCLE;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_update_request
(
    cache_update_request    INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_cache_update_request' ),
    schema_name             VARCHAR NOT NULL,
    table_name              VARCHAR NOT NULL,
    primary_key             INTEGER NOT NULL,
    key_info                JSON,
    created                 TIMESTAMP NOT NULL DEFAULT now(),
    disable_flush           BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE INDEX ix_cache_update_request_table_name ON @extschema@.tb_cache_update_request( table_name );

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_flush()
RETURNS TRIGGER AS 
 $_$
BEGIN
    -- Check for in-progress cache table refreshes by looking for advisory locks held by fn_refresh_cache_table()
    PERFORM pid
       FROM pg_locks
      WHERE locktype = 'advisory'
        AND mode = 'ExclusiveLock'
        AND objid = 'tb_cache_update_request'::REGCLASS;
    
    IF FOUND THEN
        NEW.disable_flush := TRUE;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_disable_flush
    BEFORE INSERT ON @extschema@.tb_cache_update_request
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_disable_flush();

CREATE OR REPLACE FUNCTION @extschema@.fn_issue_cache_update_request()
RETURNS TRIGGER AS
 $_$
BEGIN
    NOTIFY cache_update_request;
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_cache_update_request
    AFTER INSERT ON @extschema@.tb_cache_update_request
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_issue_cache_update_request();

CREATE SEQUENCE @extschema@.sq_pk_trigger_column;

CREATE TABLE IF NOT EXISTS @extschema@.tb_trigger_column
(
    trigger_column INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_trigger_column' ),
    schema_name    VARCHAR NOT NULL,
    table_name     VARCHAR NOT NULL,
    column_name    VARCHAR NOT NULL,
    UNIQUE( schema_name, table_name, column_name )
);

CREATE SEQUENCE @extschema@.sq_pk_cache_table;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_table
(
    cache_table                 INTEGER PRIMARY KEY DEFAULT nextval('@extschema@.sq_pk_cache_table'),
    table_name                  VARCHAR NOT NULL,
    generation_query            TEXT NOT NULL,
    unique_expression           VARCHAR[] NOT NULL,
    group_by_expression         VARCHAR[] DEFAULT NULL::VARCHAR[],
    has_rollup_from_table       JSON DEFAULT NULL::JSON,
    schema                      VARCHAR NOT NULL DEFAULT 'public',
    table_columns               JSON NOT NULL, -- TODO: Format for this field needs documentation
    inherits                    INTEGER REFERENCES @extschema@.tb_cache_table,
    index_columns               JSON,
    created                     TIMESTAMP NOT NULL DEFAULT now()
);

COMMENT ON TABLE @extschema@.tb_cache_table IS
    'Stores information related to cache tables. Each row is a definition of a independent cache table';
COMMENT ON COLUMN @extschema@.tb_cache_table.generation_query IS
    'The query used to populate the table';
COMMENT ON COLUMN @extschema@.tb_cache_table.unique_expression IS
    'used to generate a unique index on the output columns. This information is used to perform updates, inserts, and deletes on the cache table, keeping is up-to-date. The elements of this array should be the column names as they appear in the output.';
COMMENT ON COLUMN @extschema@.tb_cache_table.group_by_expression IS
    'input column names that are used to group the output set. These should include aliases';
COMMENT ON COLUMN @extschema@.tb_cache_table.has_rollup_from_table IS
    'JSON structure in the following format: { "source_table":"table_to_masquerade_updates_for"}. The update engine will tread a modification to source_table in the above structure as an update to the table_to_masquerade_updates_for table instead. This requires a foreign key linking the two tables. For example, if the following structure is present: { "tb_reset_issue":"tb_reset" } The engine will follow treat a modification to tb_reset_issue as a modification to tb_reset by replcaing the primary key of tb_reset_issue with that of tb_reset, using the foreign key';
COMMENT ON COLUMN @extschema@.tb_cache_table.schema IS
    'The schema that the cache table is stored in';
COMMENT ON COLUMN @extschema@.tb_cache_table.table_columns IS
    'JSON structure used to identify source tables and aliases for all tables used in the query. An example of this structure is as follows: {"tb_reset":{"r":{"reset":"reset_id","project":"Project ID"}}}. The inner-most structure lists columns used in the query, along with their aliases. Leave the alias value as null for columns used in aggregates, functions, etc. Support for this will be added later.';
COMMENT ON COLUMN @extschema@.tb_cache_table.inherits IS
    'Identifies a pk_cache_table that is inherited by this cache_table entry';
COMMENT ON COLUMN @extschema@.tb_cache_table.index_columns IS
    'Identifies the columns and index types. An example is as follows: {"some_output_column":"GIN( some_immutable_function( some_output_column ) )","another_column":"BTREE","yet_another_column":BTREE"}. For now only GIN and BTREE are supported';

CREATE UNLOGGED TABLE IF NOT EXISTS @extschema@.tb_cache_table_statistic
(
    cache_table                 INTEGER NOT NULL REFERENCES @extschema@.tb_cache_table,
    created                     TIMESTAMP NOT NULL DEFAULT now(),
    partial_refresh_keys        INTEGER NOT NULL DEFAULT 0,
    partial_refresh_duration    INTERVAL,
    full_refresh_rows           INTEGER NOT NULL DEFAULT 0,
    full_refresh_duration       INTERVAL,
    additional_stats            JSON
) WITHOUT OIDS;

CREATE SEQUENCE @extschema@.sq_pk_cache_table_trigger_column;

CREATE TABLE IF NOT EXISTS @extschema@.tb_cache_table_trigger_column
(
    cache_table_trigger_column  INTEGER PRIMARY KEY DEFAULT nextval( '@extschema@.sq_pk_cache_table_trigger_column' ),
    cache_table                 INTEGER NOT NULL REFERENCES @extschema@.tb_cache_table,
    trigger_column              INTEGER NOT NULL REFERENCES @extschema@.tb_trigger_column,
    UNIQUE( cache_table, trigger_column )
);

CREATE INDEX ix_cache_table_trigger_column_trigger_column ON @extschema@.tb_cache_table_trigger_column( trigger_column );

CREATE OR REPLACE FUNCTION @extschema@.fn_get_table_pk_col
(
    in_table_name   VARCHAR,
    in_schema_name  VARCHAR DEFAULT 'public'
)
RETURNS VARCHAR AS
 $_$
    SELECT a.attname::VARCHAR
      FROM pg_class c
      JOIN pg_namespace n
        ON n.oid = c.relnamespace
       AND n.nspname = in_schema_name
      JOIN pg_attribute a
        ON a.attrelid = c.oid
      JOIN pg_constraint cn
        ON cn.conrelid = c.oid
       AND cn.contype = 'p'
       AND cn.conkey[1] = a.attnum
     WHERE c.relname::VARCHAR = in_table_name
       AND c.relkind = 'r';
 $_$
    LANGUAGE SQL STABLE;

CREATE OR REPLACE FUNCTION @extschema@.fn_parse_tgargs
(
    in_tgargs   BYTEA
)
RETURNS VARCHAR[] AS
 $_$
    SELECT string_to_array(
               regexp_replace(
                   encode(
                       in_tgargs,
                       'escape'
                   )::VARCHAR,
                   '\\000$',
                   ''
               ),
               '\000'
           )::VARCHAR[];
 $_$
    LANGUAGE SQL IMMUTABLE;

CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record               RECORD;
    my_table_name           VARCHAR;
    my_pk_col               VARCHAR;
    my_pk_val               INTEGER;
    my_rollup_info          JSON;
    my_ct_record            RECORD; -- Cache table record
    my_tl_record            RECORD; -- Top level record (JSON table_columns)
    my_ml_record            RECORD; -- Mid level record (JSON table_columns)
    my_ll_record            RECORD; -- Low level record (JSON table_columns)
    my_additional_keys      VARCHAR[];
    my_additional_key_info  VARCHAR;
    my_key_value            VARCHAR;
    my_fk_val               INTEGER;
    my_schema_name          VARCHAR;
BEGIN
    /*
        Expects the following:
            JSON table_columns to be in the following format:
                table_name: {
                    table_alias_used_in_query: {
                        real_column_name:output_column_name(aliased)
                    }
                }

            Input rollup_info JSON in the following format:

            { masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }
    */
    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSE
        my_record := OLD;
    END IF;

    -- Parse args
    SELECT c.relname::VARCHAR,
           n.nspname::VARCHAR
      INTO my_table_name,
           my_schema_name
      FROM pg_class c
INNER JOIN pg_namespace n
        ON n.oid = c.relnamespace
     WHERE c.oid = TG_RELID;
    
    my_pk_col     := TG_ARGV[0]::VARCHAR;
    
    
    -- Handle conversion of 2nd parameter if it's null ( could be the case if there is no intersection between
    -- @extschema@.tb_cache_table.unique_expression and the keys of table_columns
    IF( TG_ARGV[1]::VARCHAR ~* 'null' ) THEN
        my_additional_keys := NULL::VARCHAR[];
    ELSE
        my_additional_keys := TG_ARGV[1]::VARCHAR[];
    END IF;

    IF( TG_NARGS = 3 ) THEN
        my_rollup_info     := TG_ARGV[2]::JSON;
    ELSIF( TG_NARGS > 3 ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid call';
        RETURN my_record;
    END IF;

    IF( my_pk_col IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request due to invalid pk parameter';
        RETURN my_record;
    END IF;

    EXECUTE 'SELECT $1.' || my_pk_col
       INTO my_pk_val
      USING my_record;

    IF( my_pk_val IS NULL ) THEN
        RAISE NOTICE 'fn_queue_cache_update_request ignoring request - could not determine pk value';
        RETURN my_record;
    END IF;

    IF( my_additional_keys IS NOT NULL ) THEN
        my_additional_key_info := '{';

        FOR my_ll_record IN( SELECT unnest( my_additional_keys )::VARCHAR AS key ) LOOP
            EXECUTE 'SELECT $1.' || my_ll_record.key || '::VARCHAR'
               INTO my_key_value
              USING my_record;

            my_additional_key_info := my_additional_key_info || '"' || my_ll_record.key || '":"' || my_key_value || '",';
        END LOOP;

        my_additional_key_info := rtrim( my_additional_key_info, ',' ) ||'}';
    END IF;

    -- Detect alternate (rollup) mode of operation, rewriting the variables as needed
    IF( my_rollup_info IS NOT NULL ) THEN
        FOR my_ll_record IN(
                SELECT key::VARCHAR,
                       value::VARCHAR
                  FROM json_each( my_rollup_info )
                           ) LOOP
            IF( my_ll_record.key = 'fk_column_name' ) THEN
                EXECUTE 'SELECT $1.' || my_ll_record.value
                   INTO my_fk_val
                  USING my_record;
            ELSIF( my_ll_record.key = 'masquerades_for_table' ) THEN
                my_table_name  := regexp_replace( my_ll_record.value, '^.*\.', '' );
                my_schema_name := regexp_replace( my_ll_record.value, '\..*$', '' );

                IF( my_schema_name = my_table_name ) THEN
                    my_schema_name := 'public';
                END IF;
            END IF;
        END LOOP;

        IF( my_table_name = TG_RELNAME ) THEN --rewrite failed
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because of invalid JSON parameter';
            RETURN my_record;
        END IF;

        IF( my_fk_val IS NULL ) THEN
            RAISE NOTICE 'fn_queue_cache_update_request failed in rollup mode because fk column is incorrect or NULL';
        END IF;

        my_pk_val := my_fk_val;
        my_table_name := regexp_replace( my_table_name, '"', '', 'g' ); -- Fix quotes
    END IF;

    INSERT INTO @extschema@.tb_cache_update_request
                (
                    schema_name,
                    table_name,
                    primary_key,
                    key_info
                )
         VALUES
                (
                    my_schema_name,
                    my_table_name,
                    my_pk_val,
                    my_additional_key_info::JSON
                );

    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_trigger_changes()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_record                       RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_s_record                     RECORD;
    my_trigger_column               INTEGER;
    my_cache_table_trigger_column   INTEGER;
BEGIN
    IF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
        -- Row's being removed - delete cache_table_trigger_columns that map triggered columns to cache_tables
        DELETE FROM @extschema@.tb_cache_table_trigger_column
              WHERE cache_table = my_record.cache_table;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW.table_columns::VARCHAR IS DISTINCT FROM OLD.table_columns::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSE
        RETURN NULL;
    END IF;

    IF( TG_OP != 'DELETE' ) THEN
        IF( TG_OP = 'UPDATE' ) THEN
            -- If this is an update, we store all the table_columns encountered for use later
            CREATE TEMP TABLE tt_update_handler
            (
                schema_name VARCHAR,
                table_name  VARCHAR,
                column_name VARCHAR
            );
        END IF;
        -- Iterate over the table_columns JSON and create a temp table using the contents
        FOR my_s_record IN(
                SELECT key::VARCHAR AS schema_name,
                       value
                  FROM json_each( my_record.table_columns )
                          ) LOOP
            FOR my_ll_record IN(
                    SELECT key::VARCHAR AS table_name,
                           value
                      FROM json_each( my_s_record.value )
                               ) LOOP
                FOR my_ml_record IN(
                        SELECT key::VARCHAR AS alias,
                               value
                          FROM json_each( my_ll_record.value )
                                   ) LOOP
                    FOR my_tl_record IN(
                            SELECT key::VARCHAR AS column_name,
                                   value::VARCHAR AS aliased_column_name
                              FROM json_each( my_ml_record.value )
                                       ) LOOP
                        -- Validate that a trigger_column record exists for this table / column combination
                        IF( TG_OP = 'UPDATE' ) THEN
                            -- Store this table_column entry into our temp table
                            INSERT INTO tt_update_handler
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        );
                        END IF;

                        -- Get or create functionality for tb_trigger_column
                        SELECT trigger_column
                          INTO my_trigger_column
                          FROM @extschema@.tb_trigger_column
                         WHERE schema_name = my_s_record.schema_name
                           AND table_name  = my_ll_record.table_name
                           AND column_name = my_tl_record.column_name;
           
                        IF( my_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_trigger_column
                                        (
                                            schema_name,
                                            table_name,
                                            column_name
                                        )
                                 VALUES
                                        (
                                            my_s_record.schema_name,
                                            my_ll_record.table_name,
                                            my_tl_record.column_name
                                        )
                              RETURNING trigger_column
                                   INTO my_trigger_column;
                        END IF;

                        -- Get or create functionality for tb_cache_table_trigger_column
                        SELECT cache_table_trigger_column
                          INTO my_cache_table_trigger_column
                          FROM @extschema@.tb_cache_table_trigger_column
                         WHERE cache_table = my_record.cache_table
                           AND trigger_column = my_trigger_column;

                        IF( my_cache_table_trigger_column IS NULL ) THEN
                            INSERT INTO @extschema@.tb_cache_table_trigger_column
                                        (
                                            cache_table,
                                            trigger_column
                                        )
                                 VALUES
                                        (
                                            my_record.cache_table,
                                            my_trigger_column
                                        )
                              RETURNING cache_table_trigger_column
                                   INTO my_cache_table_trigger_column;
                        END IF;
                    END LOOP;
                END LOOP;
            END LOOP;
        END LOOP;
         
        IF( TG_OP = 'UPDATE' ) THEN
            -- Since the UPDATE may have removed entries in table_columns, we need to validate
            -- All the table triggers we've seen while parsing table_columns against existing entries,
            -- removing the cache_table_trigger_columns referencing those trigger_columns
            WITH tt_candidate_triggers AS
            (
                SELECT trigger_column
                  FROM @extschema@.tb_trigger_column tt
             LEFT JOIN tt_update_handler uh
                    ON uh.column_name = tt.column_name
                   AND uh.table_name = tt.table_name
                   AND uh.schema_name = tt.schema_name
                 WHERE uh.schema_name IS NULL
                   AND uh.column_name IS NULL
                   AND uh.table_name IS NULL
            ),
            tt_rows_to_remove AS
            (
                SELECT cttc.cache_table_trigger_column
                  FROM @extschema@.tb_cache_table_trigger_column cttc
            INNER JOIN tt_candidate_triggers tt
                    ON tt.trigger_column = cttc.trigger_column
                 WHERE cttc.cache_table = my_record.cache_table
            )
                DELETE FROM @extschema@.tb_cache_table_trigger_column cttc
                      USING tt_rows_to_remove tt
                      WHERE tt.cache_table_trigger_column = cttc.cache_table_trigger_column;
        END IF;
    END IF;

    IF( TG_OP = 'UPDATE' OR TG_OP = 'DELETE' ) THEN
        -- Since DELETE or UPDATE operations could have removed cache_table_trigger_column entries,
        -- we will mop up unused trigger_column entries
        WITH tt_trigger_columns_to_delete AS
        (
            SELECT DISTINCT tc.trigger_column
              FROM @extschema@.tb_trigger_column tc
         LEFT JOIN @extschema@.tb_cache_table_trigger_column cttc
                ON cttc.trigger_column = tc.trigger_column
             WHERE cttc.cache_table_trigger_column IS NULL
        )
        DELETE FROM @extschema@.tb_trigger_column tc
              USING @extschema@.tt_trigger_columns_to_delete tt
              WHERE tt.trigger_column = tc.trigger_column;
    END IF;

    -- Call function that parsed trigger_column so that it can adjust the fn_queue_cache_update_request triggers
    PERFORM @extschema@.fn_manage_triggers();
    RETURN my_record;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_catch_trigger_changes
    AFTER INSERT OR DELETE OR UPDATE OF table_columns ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_manage_trigger_changes();

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_table_columns()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_s_record                     RECORD;
    my_ll_record                    RECORD;
    my_ml_record                    RECORD;
    my_tl_record                    RECORD;
    my_validated_unique_expression  VARCHAR[];
BEGIN
    EXECUTE 'CREATE TEMP TABLE tt_cache_table_test AS'
         || '( '
         || NEW.generation_query
         || COALESCE( 'GROUP BY ' || array_to_string( NEW.group_by_expression, ', ' ), '' )
         || '    LIMIT 0 '
         || ') ';

    FOR my_s_record IN(
            SELECT key::VARCHAR AS schema_name,
                   value
              FROM json_each( NEW.table_columns )
                      ) LOOP
        FOR my_ll_record IN(
                SELECT key::VARCHAR AS table_name,
                       value
                  FROM json_each( my_s_record.value )
                           ) LOOP
            -- Validate that table exists
            PERFORM c.relname::VARCHAR
               FROM pg_class c
         INNER JOIN pg_namespace n
                 ON n.oid = c.relnamespace
                AND n.nspname::VARCHAR = my_s_record.schema_name
              WHERE c.relkind = 'r'
                AND c.relname::VARCHAR = my_ll_record.table_name;
            
            IF NOT FOUND THEN
                DROP TABLE tt_cache_table_test;
                RAISE EXCEPTION 'Table %.% does not exist in the catalog.', my_s_record.schema_name, my_ll_record.table_name;
            END IF;

            FOR my_ml_record IN(
                    SELECT key::VARCHAR AS alias,
                           value
                      FROM json_each( my_ll_record.value )
                               ) LOOP
                FOR my_tl_record IN(
                        SELECT key::VARCHAR AS column_name,
                               regexp_replace( value::VARCHAR, '"', '', 'g' ) AS aliased_column_name -- value is json and will be wrapped in " despite it being a varchar
                          FROM json_each( my_ml_record.value )
                                   ) LOOP
                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = my_ll_record.table_name
                INNER JOIN pg_namespace n
                        ON n.oid = c.relnamespace
                       AND n.nspname::VARCHAR = my_s_record.schema_name
                     WHERE a.attnum > 0
                       AND a.attname::VARCHAR = my_tl_record.column_name;
                    
                    IF NOT FOUND THEN
                        DROP TABLE tt_cache_table_test;
                        RAISE EXCEPTION 'Table %.% does not have column %!', my_s_record.schema_name, my_ll_record.table_name, my_tl_record.column_name;
                    END IF;

                   PERFORM a.attname::VARCHAR
                      FROM pg_attribute a
                INNER JOIN pg_class c
                        ON c.oid = a.attrelid
                       AND c.relkind = 'r'
                       AND c.relname::VARCHAR = 'tt_cache_table_test'
                     WHERE a.attnum > 0
                       AND a.attname = my_tl_record.aliased_column_name;

                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Output cache table does not have column % in output!', my_tl_record.aliased_column_name;
                    END IF;

                    IF( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ NEW.unique_expression ) THEN
                        IF( my_tl_record.column_name != my_tl_record.aliased_column_name ) THEN
                            DROP TABLE tt_cache_table_test;
                            RAISE EXCEPTION 'Column % is in the unique_expression and cannot be aliased!', my_tl_record.column_name;
                        END IF;

                        IF( NOT ( ARRAY[ my_tl_record.column_name ]::VARCHAR[] <@ my_validated_unique_expression ) ) THEN
                            my_validated_unique_expression := array_append(
                                my_validated_unique_expression,
                                my_tl_record.column_name
                            );
                        END IF;
                    END IF;
                END LOOP;
            END LOOP;
        END LOOP;
    END LOOP;

    IF( array_length( my_validated_unique_expression, 1 ) != array_length( NEW.unique_expression, 1 ) ) THEN
        RAISE EXCEPTION 'unique_expression failed validation. All unique_expression columns need to be in output.';
    END IF;

    DROP TABLE tt_cache_table_test;
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_validate_table_columns
    BEFORE INSERT OR UPDATE OF table_columns ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_validate_table_columns();

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_triggers()
RETURNS VOID AS
 $_$
DECLARE
    my_additional_keys  VARCHAR[]; -- Keeps track of the intersection of unique_expression and triggered columns
    my_func_args        VARCHAR[]; -- Argument list of trigger function
    my_func_arg         VARCHAR;
    my_ct_record        RECORD;
    my_tr_record        RECORD;
    my_query            TEXT; -- Used for trigger creation / debug
BEGIN
    -- This function builds up my_func_args for each trigger function it installs
    -- In normal mode, my_func_args looks like:
    -- ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1', 'unique_key_2' ]'::JSON::VARCHAR ]::VARCHAR[]
    -- This gives the trigger a shortcut for the primary key of TG_RELNAME once it executes,
    -- As well as provides any additional keys that are a member of any unique_expression of a cache table that are also present on the table
    -- In rollup mode, my_func_args loops like
    -- ARRAY[ 'pk_column'::VARCHAR, '[ 'unique_key_1' ]'::JSON::VARCHAR, '{ masquerades_for_table:table_name,fk_column_name:foreign_key_on_current_table_referencing_masquerades_for_table }'::JSON::VARCHAR ]::VARCHAR[]
    -- This provides the keys used in normal mode, along with the masquerading information,
    -- In this mode, the first two parameters are overwritten so that the trigger can masquerade update operations
    
    -- Loop over installed triggers and validate that they are correct
    FOR my_tr_record IN(
            WITH tt_installed_triggers AS
            (
                SELECT t.tgname::VARCHAR AS trigger_name,
                       unnest( @extschema@.fn_parse_tgargs( t.tgargs ) ) AS column_name,
                       c.relname::VARCHAR AS table_name,
                       n.nspname::VARCHAR AS schema_name
                  FROM pg_trigger t
            INNER JOIN pg_class c
                    ON c.oid = t.tgrelid
                   AND c.relkind = 'r'
                   AND c.relhastriggers IS TRUE
            INNER JOIN pg_namespace n
                    ON n.oid = c.relnamespace
                 WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request'
            ),
            tt_trigger_adjust_logic AS
            (
                SELECT COALESCE( tc.schema_name, tt.schema_name ) AS schema_name,
                       COALESCE( tc.table_name, tt.table_name ) AS table_name,
                       array_remove( array_agg( DISTINCT tc.column_name ORDER BY tc.column_name NULLS LAST ), NULL ) AS new_definition,
                       array_remove( array_agg( DISTINCT tt.column_name ORDER BY tt.column_name NULLS LAST ), NULL ) AS old_definition,
                       array_agg( DISTINCT tt.trigger_name ORDER BY tt.trigger_name NULLS LAST ) AS tgname
                  FROM tt_installed_triggers tt
            FULL OUTER JOIN @extschema@.tb_trigger_column tc
                    ON tc.table_name = tt.table_name
                   AND tc.column_name = tt.column_name
              GROUP BY COALESCE( tc.schema_name, tt.schema_name ),
                       COALESCE( tc.table_name, tt.table_name )
            )
                SELECT tgname[1] AS trigger_name,
                       new_definition,
                       schema_name,
                       table_name,
                       @extschema@.fn_get_table_pk_col( table_name ) AS primary_key,
                       CASE WHEN TRUE = ALL( SELECT unnest( tgname ) IS NULL )
                            THEN 'CREATE_TRIGGER'::VARCHAR
                            WHEN FALSE = ALL( SELECT unnest( tgname ) IS NULL ) AND new_definition != old_definition
                            THEN 'DROP_TRIGGER'::VARCHAR
                            ELSE 'UPDATE_TRIGGER'::VARCHAR
                             END AS operation
                  FROM tt_trigger_adjust_logic
                 WHERE NOT ( FALSE = ALL( SELECT unnest( tgname ) IS NULL ) AND new_definition = old_definition ) -- ignore triggers with no change
                            ) LOOP
        my_additional_keys := NULL::VARCHAR[]; -- Reset to avoid trigger bugs
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            EXECUTE 'DROP TRIGGER ' || my_tr_record.trigger_name || ' ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        -- Begin creating trigger definition
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            my_func_args[1] := my_tr_record.primary_key;
            
            -- Find all cache_tables this trigger will impact and determine the unique_expression overlap and rollup_info
            FOR my_ct_record IN(
                WITH tt_related_cache_tables AS
                (
                    SELECT DISTINCT ON( ct.cache_table )
                           ct.cache_table,
                           ct.unique_expression
                      FROM @extschema@.tb_cache_table ct
                INNER JOIN @extschema@.tb_cache_table_trigger_column cttc
                        ON cttc.cache_table = ct.cache_table
                INNER JOIN @extschema@.tb_trigger_column tc
                        ON tc.trigger_column = cttc.trigger_column
                       AND tc.table_name = my_tr_record.table_name
                )
                    SELECT ARRAY(
                                    SELECT unnest( unique_expression )
                                 INTERSECT
                                    SELECT unnest( my_tr_record.new_definition )
                                ) AS keys,
                           cache_table
                      FROM tt_related_cache_tables
                               ) LOOP
                IF( my_ct_record.keys IS NOT NULL ) THEN
                    my_additional_keys := my_additional_keys || my_ct_record.keys;
                END IF;
            END LOOP;
            
            -- Get distinct elements of my_additional_keys
            SELECT array_agg( DISTINCT a.element )::VARCHAR -- needs to be string for addition into my_func_args ( a 1-dim array )
              INTO my_func_arg
              FROM (
                        SELECT unnest( my_additional_keys ) AS element
                   ) a
             WHERE a.element IS NOT NULL;
             
             -- Use this roundabout method since selecting into an array element results in a null element (wtf tom lane)
             my_func_args[2] := my_func_arg;

             my_query := format(
                         'CREATE TRIGGER tr_queue_cache_update_request'
                      || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                      || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L )',
                         my_func_args[1],
                         my_func_args[2]
                     );
             EXECUTE my_query;
        END IF;
    END LOOP;

    -- TODO: Handle rollup triggers
    FOR my_tr_record IN(
            WITH tt_current_trigger_info AS
            (
                SELECT ct.cache_table,
                       bn.nspname::VARCHAR AS schema_name,
                       bcl.relname::VARCHAR AS table_name,
                       je.value AS value,
                       ba.attname AS foreign_key,
                       (
                            '{"masquerades_for_table":"'
                         || je.value
                         || '","fk_column_name":"'
                         || ba.attname
                         || '"}'
                       )::JSON AS rollup_info
                  FROM @extschema@.tb_cache_table ct
    INNER JOIN LATERAL (
                            SELECT ct.cache_table,
                                   value
                              FROM json_array_elements( ct.has_rollup_from_table )
                       ) jae
                    ON jae.cache_table = ct.cache_table
    INNER JOIN LATERAL (
                            SELECT jae.cache_table,
                                   key::VARCHAR,
                                   regexp_replace( value::VARCHAR, '"', '', 'g' ) AS value
                              FROM json_each( jae.value )
                       ) je
                    ON je.cache_table = ct.cache_table
            INNER JOIN pg_class bcl -- base table
                    ON bcl.relkind = 'r'
                   AND bcl.relname::VARCHAR = regexp_replace( je.key, '^.*\.', '' ) -- get table_name
            INNER JOIN pg_namespace bn
                    ON bn.oid = bcl.relnamespace
                   AND bn.nspname::VARCHAR = regexp_replace( je.key, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_class mcl -- masqueraded table
                    ON mcl.relkind = 'r'
                   AND mcl.relname::VARCHAR = regexp_replace( je.value, '^.*\.', '' ) -- get table_name
            INNER JOIN pg_namespace mn
                    ON mn.oid = mcl.relnamespace
                   AND mn.nspname::VARCHAR = regexp_replace( je.value, '\..*$', '' ) -- get schema qualifier
            INNER JOIN pg_attribute ba
                    ON ba.attrelid = bcl.oid
                   AND ba.attnum > 0
            INNER JOIN pg_attribute ma
                    ON ma.attrelid = mcl.oid
                   AND ma.attnum > 0
            INNER JOIN pg_constraint bc
                    ON bc.conrelid = bcl.oid
                   AND bc.conrelid = ba.attrelid
                   AND bc.contype = 'f'
                   AND ba.attnum = ANY( bc.conkey )
                   AND bc.confrelid = mcl.oid
            INNER JOIN pg_constraint mc
                    ON mc.conrelid = mcl.oid
                   AND mc.conrelid = ma.attrelid
                   AND mc.contype = 'p'
                   AND ma.attnum = ANY( mc.conkey )
                 WHERE has_rollup_from_table IS NOT NULL
            ),
            tt_installed_trigger_info AS
            (
                SELECT t.tgname::VARCHAR AS trigger_name,
                       t.tgargs,
                       n.nspname::VARCHAR AS schema_name,
                       c.relname::VARCHAR AS table_name
                  FROM pg_trigger t
            INNER JOIN pg_class c
                    ON c.oid = t.tgrelid
                   AND c.relkind = 'r'
                   AND c.relhastriggers IS TRUE
            INNER JOIN pg_namespace n
                    ON n.oid = c.relnamespace
                 WHERE t.tgname::VARCHAR = 'tr_queue_cache_update_request_from_rollup_table'
            ),
            tt_trigger_logic AS
            (
                SELECT COALESCE( ttct.schema_name, ttit.schema_name ) AS schema_name,
                       COALESCE( ttct.table_name, ttit.table_name ) AS table_name,
                       CASE WHEN ttct.table_name IS NULL
                             AND ttit.table_name IS NOT NULL
                            THEN 'DROP_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NULL
                            THEN 'CREATE_TRIGGER'
                            WHEN ttct.table_name IS NOT NULL
                             AND ttit.table_name IS NOT NULL
                             AND rollup_info::VARCHAR IS DISTINCT FROM ( ( @extschema@.fn_parse_tgargs( ttit.tgargs ) )[2] )::JSON::VARCHAR
                            THEN 'UPDATE_TRIGGER'
                            ELSE NULL
                             END AS logic_result,
                       ttct.rollup_info,
                       ttit.trigger_name
                  FROM tt_current_trigger_info ttct
       FULL OUTER JOIN tt_installed_trigger_info ttit
                    ON ttit.table_name = ttct.table_name
            )
                SELECT schema_name,
                       table_name,
                       logic_result AS operation,
                       rollup_info,
                       trigger_name
                  FROM tt_trigger_logic
                 WHERE logic_result IS NOT NULL
                       ) LOOP
        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'DROP_TRIGGER' ) THEN
            EXECUTE 'DROP TRIGGER ' || my_tr_record.trigger_name || ' ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name;
        END IF;

        IF( my_tr_record.operation = 'UPDATE_TRIGGER' OR my_tr_record.operation = 'CREATE_TRIGGER' ) THEN
            SELECT @extschema@.fn_get_table_pk_col( my_tr_record.table_name )::VARCHAR
              INTO my_func_arg;
        
            my_func_args[1] := my_func_arg;
            my_func_args[2] := 'null'::JSON::VARCHAR;
            my_func_args[3] := my_tr_record.rollup_info::VARCHAR;
            
            my_query := format(
                        'CREATE TRIGGER tr_queue_cache_update_request_from_rollup_table'
                     || '    AFTER INSERT OR DELETE OR UPDATE ON ' || my_tr_record.schema_name || '.' || my_tr_record.table_name
                     || '    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_queue_cache_update_request( %L, %L, %L )',
                         my_func_args[1],
                         my_func_args[2],
                         my_func_args[3]
                    ); 
            EXECUTE my_query;    
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_manage_indexes
(
    in_cache_table INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_index_name       VARCHAR;
    my_ct_record        RECORD;
    my_index_column     VARCHAR;
    my_index_type       VARCHAR;
    my_unique_columns   VARCHAR;
BEGIN
    FOR my_index_name IN(
            SELECT ci.relname::VARCHAR
              FROM pg_class c
        INNER JOIN pg_index i
                ON i.indrelid = c.oid
        INNER JOIN pg_class ci
                ON ci.oid = i.indexrelid
        INNER JOIN pg_namespace n
                ON n.oid = c.relnamespace
        INNER JOIN @extschema@.tb_cache_table ct
                ON ct.table_name = c.relname::VARCHAR
               AND ct.schema = n.nspname::VARCHAR
               AND ct.cache_table = in_cache_table
                        ) LOOP
        EXECUTE 'DROP INDEX ' || my_index_name;
    END LOOP;

    SELECT unique_expression,
           schema,
           table_name,
           index_columns
      INTO my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    FOR my_index_column, my_index_type IN(
            SELECT key::VARCHAR,
                   value::VARCHAR
              FROM json_each( my_ct_record.index_columns )
                                         ) LOOP
        my_index_name := 'ix_' || my_ct_record.table_name || '_' || my_index_column;

        IF( my_index_type ~* '^gin' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || 'USING ' || my_index_type;
        ELSIF( my_index_type ~* '^btree' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name;
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_analyze_table
(
    in_cache_table  INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_table_name   VARCHAR;
    my_schema_name  VARCHAR;
BEGIN
    SELECT table_name,
           schema
      INTO my_table_name,
           my_schema_name
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'ANALYZE ' || my_schema_name || '.' || my_table_name;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table
(
    in_cache_table_name    VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_unique_expression    VARCHAR[];
    my_query                TEXT;
    my_group_by             TEXT;
    my_schema               VARCHAR;
    my_duration             INTERVAL;
    my_start                TIMESTAMP;
    my_min_messages         VARCHAR;
    my_is_parent            BOOLEAN;
    my_cache_table          INTEGER;
    my_parent_table         INTEGER;
    my_unique               VARCHAR;
    my_child_table          RECORD;
    my_index_row            RECORD;
    my_rows                 INTEGER;
    my_index_start          TIMESTAMP;
    my_index_duration       INTERVAL;
BEGIN
    SELECT clock_timestamp()
      INTO my_start;

    IF NOT pg_try_advisory_xact_lock(
                    'tb_cache_table'::REGCLASS::INTEGER,
                    cache_table
                )
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
    THEN
        RAISE EXCEPTION 'Aborted attempt to refresh cache table, a refresh is already in progress';
    END IF;

    -- Attempt to lock tb_cache_update_request globally. This is a tblmgr-level signal
    -- that there is a refresh currently underway
    PERFORM pg_try_advisory_xact_lock(
                'tb_cache_update_request'::REGCLASS::INTEGER
            );
    
    my_index_duration := '0 seconds'::INTERVAL;

    SELECT unique_expression,
           generation_query,
           COALESCE( 'GROUP BY ' || array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR ),
           cache_table,
           inherits,
           schema
      INTO my_unique_expression,
           my_query,
           my_group_by,
           my_cache_table,
           my_parent_table,
           my_schema
      FROM @extschema@.tb_cache_table
     WHERE table_name = in_cache_table_name;

    IF( my_cache_table IS NULL ) THEN
        RAISE NOTICE 'TBLMGR: Table % is not managed by tblmgr.', in_cache_table_name;
        RETURN;
    END IF;

    IF( my_parent_table IS NOT NULL ) THEN
        RAISE NOTICE 'TBLMGR: Redirecting table refresh to parent table';
        PERFORM @extschema@.fn_refresh_cache_table( table_name )
           FROM @extschema@.tb_cache_table
          WHERE cache_table = my_parent_table;
        RETURN;
    END IF;

    --Handle the case where the target table is inherited
    my_is_parent := FALSE;

    SELECT CASE WHEN COUNT( cache_table ) > 0
                THEN TRUE
                ELSE FALSE
           END
      INTO my_is_parent
      FROM @extschema@.tb_cache_table
     WHERE inherits = my_cache_table;

    my_min_messages := current_setting( 'client_min_messages' );
    SET client_min_messages TO 'WARNING';

    EXECUTE 'CREATE TABLE ' || my_schema || '.' || in_cache_table_name || '_temp AS ( ' || my_query || my_group_by || ' )';
    EXECUTE 'DROP INDEX IF EXISTS ix_' || in_cache_table_name || '_temp';
    my_index_start := clock_timestamp();
    EXECUTE 'CREATE UNIQUE INDEX ix_' || in_cache_table_name || '_temp ON '
         || my_schema || '.' || in_cache_table_name || '_temp( ' || array_to_string( my_unique_expression, ',' ) || ')';
    my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start ); 
    EXECUTE 'SELECT COUNT(*) FROM ' || my_schema || '.' || in_cache_table_name || '_temp'
       INTO my_rows;
    
    IF( my_is_parent IS TRUE ) THEN
        FOR my_child_table IN( SELECT * FROM @extschema@.tb_cache_table WHERE inherits = my_cache_table ) LOOP
            EXECUTE 'CREATE TABLE ' || my_child_table.schema || '.' || my_child_table.table_name || '_temp () '
                 || ' INHERITS ( ' || my_schema || '.' || in_cache_table_name || '_temp )';
            RAISE NOTICE 'Populating %...', my_child_table.schema || '.' || my_child_table.table_name;
            EXECUTE 'INSERT INTO ' || my_child_table.table_name || '_temp '
                 || my_child_table.generation_query
                 || COALESCE( 'GROUP BY ' || array_to_string( my_child_table.group_by_expression, ',' ), '' );
            EXECUTE 'DROP INDEX IF EXISTS ix_' || my_child_table.table_name || '_temp';
            my_index_start := clock_timestamp();
            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_child_table.table_name || '_temp ON '
                 || my_child_table.schema || '.' || my_child_table.table_name || '_temp( '
                 || array_to_string( my_child_table.unique_expression, ',' ) || ')';
            my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start );
            EXECUTE 'SELECT COUNT(*) + ' || my_rows || '::INTEGER FROM ' || my_child_table.schema || '.' || my_child_table.table_name || '_TEMP'
               INTO my_rows; 
        END LOOP;
    END IF;

    IF( my_is_parent IS TRUE ) THEN
        -- Handle case where table inheritance is broken - manually remove the inherited table
        FOR my_child_table IN
            SELECT *
              FROM @extschema@.tb_cache_table
             WHERE inherits = my_cache_table
        LOOP
            EXECUTE 'DROP TABLE IF EXISTS ' || my_child_table.schema || '.' || my_child_table.table_name;
        END LOOP;

        EXECUTE 'DROP TABLE IF EXISTS ' || my_schema || '.' || in_cache_table_name || ' CASCADE';
    ELSE
        EXECUTE 'DROP TABLE IF EXISTS ' || my_schema || '.' || in_cache_table_name;
    END IF;

    EXECUTE 'ALTER TABLE ' || my_schema || '.' || in_cache_table_name || '_temp RENAME TO ' || in_cache_table_name;
    EXECUTE 'DROP INDEX IF EXISTS ix_' || in_cache_table_name;
    EXECUTE 'ALTER INDEX ix_' || in_cache_table_name || '_temp RENAME TO ix_' || in_cache_table_name;

    EXECUTE 'SET client_min_messages TO ' || my_min_messages;

    RAISE NOTICE 'TBLMGR: Analyzing table %.%...', my_schema, in_cache_table_name;
    PERFORM @extschema@.fn_analyze_table( my_cache_table );
    RAISE NOTICE 'TBLMGR: Adding indexes to table %.%...', my_schema, in_cache_table_name;
    my_index_start := clock_timestamp();
    PERFORM @extschema@.fn_manage_indexes( my_cache_table );
    my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start ); 

    IF( my_is_parent IS TRUE ) THEN
        FOR my_child_table IN
            SELECT *
              FROM @extschema@.tb_cache_table
             WHERE inherits = my_cache_table
        LOOP
            EXECUTE 'ALTER TABLE ' || my_child_table.schema || '.' || my_child_table.table_name || '_temp RENAME TO ' || my_child_table.table_name;
            EXECUTE 'DROP INDEX IF EXISTS ix_' || my_child_table.table_name;
            EXECUTE 'ALTER INDEX ix_' || my_child_table.table_name || '_temp RENAME TO ix_' || my_child_table.table_name;

            RAISE NOTICE 'TBLMGR: Analyzing table %.%...', my_child_table.schema, my_child_table.table_name;
            PERFORM @extschema@.fn_analyze_table( my_child_table.cache_table );
            RAISE NOTICE 'TBLMGR: Adding indexes to table %.%...', my_child_table.schema, my_child_table.table_name;
            my_index_start := clock_timestamp();
            PERFORM @extschema@.fn_manage_indexes( my_child_table.cache_table );
            my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start ); 
        END LOOP;
    END IF;

    INSERT INTO @extschema@.tb_cache_table_statistic
                (
                    cache_table,
                    full_refresh_duration,
                    full_refresh_rows,
                    additional_stats
                )
         SELECT cache_table,
                clock_timestamp() - my_start AS full_refresh_duration,
                my_rows AS full_refresh_rows,
                ( '{"index_creation":' || EXTRACT( epoch FROM my_index_duration )::FLOAT::VARCHAR || '}' )::JSON
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
            AND schema = my_schema;

    
    PERFORM *
       FROM @extschema@.tb_cache_update_request;

    IF FOUND THEN
        NOTIFY cache_update_request;
    END IF;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_drop_table()
RETURNS TRIGGER AS
 $_$
DECLARE
    my_trigger_record   RECORD;
BEGIN
    
    EXECUTE 'DROP TABLE IF EXISTS ' || OLD.schema || '.' || OLD.table_name || ' CASCADE';

    EXECUTE 'DELETE FROM @extschema@.tb_cache_table ctc '
         || '      USING @extschema@.tb_cache_table ct '
         || '      WHERE ctc.inherits = ' || OLD.cache_table;
    RETURN OLD;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_drop_table
    BEFORE DELETE ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_drop_table();

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table_trigger_wrapper()
RETURNS TRIGGER AS
 $_$
BEGIN
    PERFORM @extschema@.fn_refresh_cache_table( NEW.table_name );
    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE TRIGGER tr_handle_table_definition_change
    AFTER INSERT OR UPDATE OF generation_query, group_by_expression, unique_expression
    ON @extschema@.tb_cache_table
    FOR EACH ROW EXECUTE PROCEDURE @extschema@.fn_refresh_cache_table_trigger_wrapper();

CREATE OR REPLACE FUNCTION @extschema@.fn_validate_cache_table
(
    in_cache_table      INTEGER,
    in_scan_fraction    FLOAT DEFAULT 0.1
)
RETURNS BOOLEAN AS
 $_$
DECLARE
    my_ct_record    RECORD;
    my_limit        INTEGER;
    my_column_name  VARCHAR;
    my_check        BOOLEAN;
    my_es_proto     VARCHAR[]; -- arrays of columns used to make record object for set comparison
    my_cs_proto     VARCHAR[];
    my_query        TEXT;
BEGIN
    PERFORM *
       FROM tblmgr.tb_cache_update_request;

    IF FOUND THEN
        RAISE NOTICE 'Test aborted, unserviced cache_update_requests found';
        RETURN FALSE;
    END IF;

    SELECT table_name,
           schema,
           unique_expression,
           generation_query,
           COALESCE( 'GROUP BY ' || array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR ) AS group_by,
           table_columns
      INTO STRICT my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    EXECUTE 'SELECT COUNT(*) FROM ' || my_ct_record.schema || '.' || my_ct_record.table_name
       INTO my_limit;

    my_limit := ( my_limit * in_scan_fraction )::INTEGER;
    RAISE NOTICE 'Scanning in % rows from %.%', my_limit, my_ct_record.schema, my_ct_record.table_name;

    EXECUTE
    'CREATE TEMP TABLE tt_current_set AS '
 || '( '
 || my_ct_record.generation_query || ' '
 || my_ct_record.group_by || ' '
 || ' LIMIT ' || my_limit
 || ') ';
    EXECUTE 'CREATE INDEX ix_temp_current_unique ON tt_current_set( ' || array_to_string( my_ct_record.unique_expression, ',' ) || ' )';
    ANALYZE tt_current_set;

    PERFORM *
       FROM tt_current_set;
    
    IF NOT FOUND THEN
        DROP TABLE IF EXISTS tt_current_set;
        RETURN FALSE;
    END IF;
   
    my_query := 
    'CREATE TEMP TABLE tt_existing_set AS '
 || '( '
 || '    SELECT ct.* '
 || '      FROM ONLY ' || my_ct_record.schema || '.' || my_ct_record.table_name || ' ct ';

    IF( in_scan_fraction < 1.0 ) THEN
        RAISE NOTICE 'Performing partial scan, warning: missing or extra tuples cannot be validated.';
        my_query := my_query
     || 'INNER JOIN tt_current_set ttcs '
     || '     USING( ' || array_to_string( my_ct_record.unique_expression, ', ' ) || ' ) ';
    ELSE
        RAISE NOTICE 'Performing full set check';
    END IF;
    
    my_query := my_query || ') ';

    EXECUTE my_query;
    EXECUTE 'CREATE INDEX ix_temp_existing_unique ON tt_existing_set( ' || array_to_string( my_ct_record.unique_expression, ',' ) || ' )';
    ANALYZE tt_existing_set;
    
    PERFORM *
       FROM tt_existing_set;

    IF NOT FOUND THEN
        DROP TABLE IF EXISTS tt_current_set;
        DROP TABLE IF EXISTS tt_existing_set;
        RETURN FALSE;
    END IF;
    
    -- Build join clause for full set validation
    FOR my_column_name IN(
            SELECT a.attname::VARCHAR
              FROM pg_attribute a
        INNER JOIN pg_class c
                ON c.oid = a.attrelid
               AND c.relname::VARCHAR = my_ct_record.table_name
               AND c.relkind = 'r'
             WHERE a.attnum > 0
                         ) LOOP
        my_es_proto := array_append(
            my_es_proto,
            ( 'ttes.' || my_column_name )::VARCHAR
        );

        my_cs_proto := array_append(
            my_cs_proto,
            ( 'ttcs.' || my_column_name )::VARCHAR
        );
    END LOOP;

    -- Check for set disparity
    EXECUTE
    'CREATE TEMP TABLE tt_validation AS '
 || '( '
 || '          SELECT ttcs.* '
 || '            FROM tt_existing_set ttes '
 || ' FULL OUTER JOIN tt_current_set ttcs '
 || '              ON ( ' || array_to_string( my_cs_proto, ', ' ) || ' )::VARCHAR = ( ' || array_to_string( my_es_proto, ', ' ) || ' )::VARCHAR '
 || '           WHERE ttes.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttes.' ) || ' IS NULL '
 || '              OR ttcs.' || array_to_string( my_ct_record.unique_expression, ' IS NULL AND ttcs.' ) || ' IS NULL '
 || ') ';
    PERFORM *
       FROM tt_validation;
    
    IF FOUND THEN
        DROP TABLE IF EXISTS tt_validation;
        DROP TABLE IF EXISTS tt_existing_set;
        DROP TABLE IF EXISTS tt_current_set;
        RETURN FALSE;
    END IF;

    DROP TABLE IF EXISTS tt_validation;
    DROP TABLE IF EXISTS tt_existing_set;
    DROP TABLE IF EXISTS tt_current_set;
    
    RETURN TRUE;
END
 $_$
    LANGUAGE 'plpgsql';

GRANT USAGE ON SCHEMA @extschema@ TO PUBLIC;
-- TODO: Better permissions to avoid letting just anyone change tblmgr config
GRANT SELECT, UPDATE, DELETE, INSERT ON ALL TABLES IN SCHEMA @extschema@ TO PUBLIC;
GRANT USAGE ON ALL SEQUENCES IN SCHEMA @extschema@ TO PUBLIC;


SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_cache_table', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_trigger_column', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.tb_cache_table_trigger_column', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_cache_table', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_trigger_column', '' );
SELECT pg_catalog.pg_extension_config_dump( '@extschema@.sq_pk_cache_table_trigger_column', '' );
