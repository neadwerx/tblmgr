CREATE OR REPLACE FUNCTION @extschema@.fn_manage_indexes
(
    in_cache_table  INTEGER
)
RETURNS VOID AS
 $_$
DECLARE
    my_index_name       VARCHAR;
    my_ct_record        RECORD;
    my_index_column     VARCHAR;
    my_index_type       VARCHAR;
    my_unique_columns   VARCHAR;
BEGIN
    FOR my_index_name IN(
            SELECT ci.relname::VARCHAR
              FROM pg_class c
        INNER JOIN pg_index i
                ON i.indrelid = c.oid
               AND i.indisunique IS FALSE
        INNER JOIN pg_class ci
                ON ci.oid = i.indexrelid
        INNER JOIN pg_namespace n
                ON n.oid = c.relnamespace
        INNER JOIN @extschema@.tb_cache_table ct
                ON ct.table_name = c.relname::VARCHAR
               AND ct.schema = n.nspname::VARCHAR
               AND ct.cache_table = in_cache_table
                        ) LOOP
        EXECUTE 'DROP INDEX ' || my_index_name;
    END LOOP;

    SELECT unique_expression,
           schema,
           table_name,
           index_columns
      INTO my_ct_record
      FROM @extschema@.tb_cache_table
     WHERE cache_table = in_cache_table;

    FOR my_index_column, my_index_type IN(
            SELECT key::VARCHAR,
                   value::VARCHAR
              FROM json_each( my_ct_record.index_columns )
                                         ) LOOP
        my_index_name := 'ix_' || my_ct_record.table_name || '_' || my_index_column;
        IF( my_index_type ~* 'gin' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || 'USING ' || my_index_type;
        ELSIF( my_index_type ~* 'btree' ) THEN
            EXECUTE 'CREATE INDEX ' || my_index_name::VARCHAR(63) || ' ON ' || my_ct_record.schema || '.' || my_ct_record.table_name
                 || '( ' || my_index_column || ' )';
        END IF;
    END LOOP;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_disable_flush()
RETURNS TRIGGER AS
 $_$
BEGIN
    -- Check for in-progress cache table refreshes by looking for advisory locks held by fn_refresh_cache_table()
    PERFORM pid
       FROM pg_locks
      WHERE locktype = 'advisory'
        AND mode = 'ExclusiveLock'
        AND objid = '@extschema@.tb_cache_update_request'::REGCLASS;

    IF FOUND THEN
        NEW.disable_flush := TRUE;
    END IF;

    RETURN NEW;
END
 $_$
    LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION @extschema@.fn_refresh_cache_table
(
    in_cache_table_name    VARCHAR
)
RETURNS VOID AS
 $_$
DECLARE
    my_unique_expression    VARCHAR[];
    my_query                TEXT;
    my_group_by             TEXT;
    my_schema               VARCHAR;
    my_duration             INTERVAL;
    my_start                TIMESTAMP;
    my_min_messages         VARCHAR;
    my_is_parent            BOOLEAN;
    my_cache_table          INTEGER;
    my_parent_table         INTEGER;
    my_unique               VARCHAR;
    my_child_table          RECORD;
    my_index_row            RECORD;
    my_rows                 INTEGER;
    my_index_start          TIMESTAMP;
    my_index_duration       INTERVAL;
BEGIN
    SELECT clock_timestamp()
      INTO my_start;

    IF NOT pg_try_advisory_xact_lock(
                    '@extschema@.tb_cache_table'::REGCLASS::INTEGER,
                    cache_table
                )
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
    THEN
        RAISE EXCEPTION 'Aborted attempt to refresh cache table, a refresh is already in progress';
    END IF;

    -- Attempt to lock tb_cache_update_request globally. This is a tblmgr-level signal
    -- that there is a refresh currently underway
    PERFORM pg_try_advisory_xact_lock(
                '@extschema@.tb_cache_update_request'::REGCLASS::INTEGER
            );
    
    my_index_duration := '0 seconds'::INTERVAL;

    SELECT unique_expression,
           generation_query,
           COALESCE( 'GROUP BY ' || array_to_string( group_by_expression, ',' )::VARCHAR, ''::VARCHAR ),
           cache_table,
           inherits,
           schema
      INTO my_unique_expression,
           my_query,
           my_group_by,
           my_cache_table,
           my_parent_table,
           my_schema
      FROM @extschema@.tb_cache_table
     WHERE table_name = in_cache_table_name;

    IF( my_cache_table IS NULL ) THEN
        RAISE NOTICE 'TBLMGR: Table % is not managed by tblmgr.', in_cache_table_name;
        RETURN;
    END IF;

--    IF( my_parent_table IS NOT NULL ) THEN
--        RAISE NOTICE 'TBLMGR: Redirecting table refresh to parent table';
--        PERFORM @extschema@.fn_refresh_cache_table( table_name )
--           FROM @extschema@.tb_cache_table
--          WHERE cache_table = my_parent_table;
--        RETURN;
--    END IF;

    --Handle the case where the target table is inherited
    my_is_parent := FALSE;

    SELECT CASE WHEN COUNT( cache_table ) > 0
                THEN TRUE
                ELSE FALSE
           END
      INTO my_is_parent
      FROM @extschema@.tb_cache_table
     WHERE inherits = my_cache_table;

    my_min_messages := current_setting( 'client_min_messages' );
    SET client_min_messages TO 'WARNING';

    EXECUTE 'CREATE TABLE ' || my_schema || '.' || in_cache_table_name || '_temp AS ( ' || my_query || my_group_by || ' )';
    EXECUTE 'SELECT COUNT(*) FROM ' || my_schema || '.' || in_cache_table_name || '_temp'
       INTO my_rows;
    
    IF( my_is_parent IS TRUE ) THEN
        FOR my_child_table IN( SELECT * FROM @extschema@.tb_cache_table WHERE inherits = my_cache_table ) LOOP
            EXECUTE 'CREATE TABLE ' || my_child_table.schema || '.' || my_child_table.table_name || '_temp () '
                 || ' INHERITS ( ' || my_schema || '.' || in_cache_table_name || '_temp )';
            --RAISE NOTICE 'Populating %...', my_child_table.schema || '.' || my_child_table.table_name;
            EXECUTE 'INSERT INTO ' || my_child_table.table_name || '_temp '
                 || my_child_table.generation_query
                 || COALESCE( 'GROUP BY ' || array_to_string( my_child_table.group_by_expression, ',' ), '' );
            EXECUTE 'SELECT COUNT(*) + ' || my_rows || '::INTEGER FROM ' || my_child_table.schema || '.' || my_child_table.table_name || '_TEMP'
               INTO my_rows; 
        END LOOP;
    END IF;

    IF( my_is_parent IS TRUE ) THEN
        -- Handle case where table inheritance is broken - manually remove the inherited table
        FOR my_child_table IN
            SELECT *
              FROM @extschema@.tb_cache_table
             WHERE inherits = my_cache_table
        LOOP
            EXECUTE 'DROP TABLE IF EXISTS ' || my_child_table.schema || '.' || my_child_table.table_name;
        END LOOP;

        EXECUTE 'DROP TABLE IF EXISTS ' || my_schema || '.' || in_cache_table_name || ' CASCADE';
    ELSE
        EXECUTE 'DROP TABLE IF EXISTS ' || my_schema || '.' || in_cache_table_name;
    END IF;

    EXECUTE 'ALTER TABLE ' || my_schema || '.' || in_cache_table_name || '_temp RENAME TO ' || in_cache_table_name;
    EXECUTE 'DROP INDEX IF EXISTS ix_' || in_cache_table_name;
    EXECUTE 'CREATE UNIQUE INDEX ix_' || in_cache_table_name || ' ON '
         || my_schema || '.' || in_cache_table_name || '( ' || array_to_string( my_unique_expression, ',' ) || ')';

    EXECUTE 'SET client_min_messages TO ' || my_min_messages;

    --RAISE NOTICE 'TBLMGR: Analyzing table %.%...', my_schema, in_cache_table_name;
    PERFORM @extschema@.fn_analyze_table( my_cache_table );
    --RAISE NOTICE 'TBLMGR: Adding indexes to table %.%...', my_schema, in_cache_table_name;
    my_index_start := clock_timestamp();
    PERFORM @extschema@.fn_manage_indexes( my_cache_table );
    my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start ); 

    IF( my_is_parent IS TRUE ) THEN
        FOR my_child_table IN
            SELECT *
              FROM @extschema@.tb_cache_table
             WHERE inherits = my_cache_table
        LOOP
            EXECUTE 'ALTER TABLE ' || my_child_table.schema || '.' || my_child_table.table_name || '_temp RENAME TO ' || my_child_table.table_name;
            EXECUTE 'DROP INDEX IF EXISTS ix_' || my_child_table.table_name;
            EXECUTE 'CREATE UNIQUE INDEX ix_' || my_child_table.table_name || ' ON '
                 || my_child_table.schema || '.' || my_child_table.table_name || '( ' || array_to_string( my_child_table.unique_expression, ',' ) || ')';

            --RAISE NOTICE 'TBLMGR: Analyzing table %.%...', my_child_table.schema, my_child_table.table_name;
            PERFORM @extschema@.fn_analyze_table( my_child_table.cache_table );
            --RAISE NOTICE 'TBLMGR: Adding indexes to table %.%...', my_child_table.schema, my_child_table.table_name;
            my_index_start := clock_timestamp();
            PERFORM @extschema@.fn_manage_indexes( my_child_table.cache_table );
            my_index_duration := my_index_duration + ( clock_timestamp() - my_index_start ); 
        END LOOP;
    END IF;

    INSERT INTO @extschema@.tb_cache_table_statistic
                (
                    cache_table,
                    full_refresh_duration,
                    full_refresh_rows,
                    additional_stats
                )
         SELECT cache_table,
                clock_timestamp() - my_start AS full_refresh_duration,
                my_rows AS full_refresh_rows,
                ( '{"index_creation":' || EXTRACT( epoch FROM my_index_duration )::FLOAT::VARCHAR || '}' )::JSON
           FROM @extschema@.tb_cache_table
          WHERE table_name = in_cache_table_name
            AND schema = my_schema;

    
    PERFORM *
       FROM @extschema@.tb_cache_update_request;

    IF FOUND THEN
        NOTIFY cache_update_request;
    END IF;

    RETURN;
END
 $_$
    LANGUAGE 'plpgsql';

