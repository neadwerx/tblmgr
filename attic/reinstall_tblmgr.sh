#!/bin/bash

set -e

[ -n "$1" ] || {
    echo "Database must be specified"
    exit 1
}

{
    echo "DO \$\$"
    echo "DECLARE"
    echo "    my_table VARCHAR;"
    echo "BEGIN"
    echo "    FOR my_table IN( SELECT table_name FROM tblmgr.tb_cache_table ) LOOP "
    echo "        EXECUTE 'LOCK TABLE ' || my_table || ' IN ACCESS EXCLUSIVE MODE NOWAIT';"
    echo "    END LOOP;"
    echo "END"
    echo " \$\$ LANGUAGE plpgsql;"
    echo "drop extension tblmgr cascade;" 
    echo "create extension tblmgr schema tblmgr;"
    echo "ALTER TABLE tblmgr.tb_cache_table DISABLE TRIGGER tr_catch_trigger_changes;"
    echo "ALTER TABLE tblmgr.tb_cache_table DISABLE TRIGGER tr_handle_table_definition_change;"
    echo "ALTER TABLE tblmgr.tb_cache_table DISABLE TRIGGER tr_validate_table_columns;"
    pg_dump -U postgres --data-only -t tblmgr.tb_cache_table "$1"
    pg_dump -U postgres --data-only -t tblmgr.tb_trigger_column "$1"
    pg_dump -U postgres --data-only -t tblmgr.tb_cache_table_trigger_column "$1"
    echo "ALTER TABLE tblmgr.tb_cache_table ENABLE TRIGGER tr_catch_trigger_changes;"
    echo "ALTER TABLE tblmgr.tb_cache_table ENABLE TRIGGER tr_handle_table_definition_change;"
    echo "ALTER TABLE tblmgr.tb_cache_table ENABLE TRIGGER tr_validate_table_columns;"
    echo "SET search_path = public, tblmgr, pg_catalog;"
    echo "SELECT tblmgr.fn_manage_triggers();"
} > '/tmp/tblmgr_reinstall.sql'


make install

psql -U postgres -d "$1" -1 -f /tmp/tblmgr_reinstall.sql

psql -U postgres -d "$1" -1 -c "SELECT tblmgr.fn_refresh_cache_table( table_name ) FROM tblmgr.tb_cache_table;"
