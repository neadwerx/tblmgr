#!/usr/bin/perl

$| = 1;

use utf8;
use strict;
use warnings;

use DBI;
use JSON;
use IO::Select;
use IO::Handle;
use Sys::Syslog;
use Getopt::Std;
use Time::HiRes qw( gettimeofday tv_interval );
use POSIX qw( strftime setsid :sys_wait_h );
use Data::Dumper;
use Readonly;
use Carp;

Readonly::Scalar my $LISTEN_CHANNEL => 'cache_update_request';
Readonly::Scalar my $DEBUG          => 0;

my( $conn_map, $sel, $file_descriptor );

my $service_threads = { }; # hash used to track threads that service cache tables.
my $db_handles      = { };

sub update($)
{
    my( $msg ) = @_;
    syslog( 'info', "$msg" ) if( $msg );
    print "$msg\n" if( $msg and $DEBUG );
    return;
}

sub setup_status_file()
{
    openlog( 'tblmgr', '', 'user' );
    return;
}

sub daemonize()
{
    $SIG{__DIE__} = sub
    {
        update( "ERROR: " . $_[0] );
        exit 1;
    };

    $SIG{__WARN__} = sub
    {
        update( "WARNING: " . $_[0] );
    };

    chdir '/' or croak "Can't chdir to /: $!";
    open STDIN, '/dev/null'     or croak( "Can't read /dev/null: $!" );
    open STDOUT, '>/dev/null'   or croak( "Can't write to /dev/null: $!" );
    defined( my $pid = fork() ) or croak( "Can't fork: $!" );
    exit 0 if $pid;
    setsid()                    or croat( "Can't start a new session: $!" );
}

sub manage_pid_file($$)
{
    my( $pid, $pid_file ) = @_;

    if( -e $pid_file )
    {
        croak( "Lock file '$pid_file' exists, but is not a regular file!" ) unless( -f $pid_file );
        croak( "Cannot read lock file '$pid_file'" ) unless( -r $pid_file );

        open( PID, "<$pid_file" ) or croak( "Could not open existing PID file '$pid_file': $!" );
        chomp( my $line = <PID> );
        close( PID );

        my $existing_pid;

        ( $existing_pid ) = ( $line =~ /^(\d+)$/ );

        if(
                defined $existing_pid
            and ( $existing_pid =~ /^\d+$/ )
            and $existing_pid > 0
          )
        {
            my $process_found = 0;
            my $ps_command    = '/bin/ps -e |';

            unless( open( PS, $ps_command ) )
            {
                croak( "cannot check for existing processes: $!" );
            }

            while( <PS> )
            {
                my( $check_pid ) = ( $_ =~ /^\D+(\d+)\D.*$/ );

                next unless( defined $check_pid and $check_pid =~ /^\d+$/ and $check_pid > 0 );

                if( int( $existing_pid ) == int( $check_pid ) )
                {
                    $process_found = 1;
                    last;
                }
            }

            close PS;

            if( $process_found )
            {
                croak( "Service already running! PID ($existing_pid)" );
            }
            else
            {
                unlink( $pid_file ) or croak( "Could not remove stale PID file: $!" );
                open( PID, ">$pid_file" ) or croak( "Could not open PID file: $!" );
                print( PID $pid );
                close( PID );
            }
        }
    }
    else
    {
        open( PID, ">$pid_file" ) or croak( "Could not open new PID file '$pid_file': $!" );
        print( PID $pid );
        close( PID );
    }

    return;
}

sub try_query($;$)
{
    my( $query, $pid ) = @_;
    my $sth;
    my $local_handle;
    
    if( $pid )
    {
        $local_handle = $db_handles->{$pid};
    }
    else
    {
        #Child exit causes parent handle to die too :*( RIP
        if( $db_handles->{0}->ping() )
        {
            $local_handle = $db_handles->{0};
        }
        else
        {
            $local_handle  = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef );
            $db_handles->{0} = $local_handle;
        }
    }

    until( ( $sth = $local_handle->prepare( $query ) ) and $sth->execute() )
    {
        return $sth if( $local_handle->ping() > 0 );

        until( $local_handle and $local_handle->ping() > 0 )
        {
            update( 'DB connection is down!' );
            sleep 1;
            $local_handle = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef );
            print "Reconnected\n"; 
            if( $pid )
            {
                $db_handles->{$pid} = $local_handle;
            }
            else
            {
                $db_handles->{0} = $local_handle;
            }
        }
        
        unless( $pid )
        {
            $local_handle->do( "LISTEN $LISTEN_CHANNEL" );
            $file_descriptor = $local_handle->func( 'getfd' );
            $sel             = IO::Select->new( $file_descriptor );
        }
    }

    return $sth;
}

sub get_pk_column_name($)
{
    my( $table_name ) = @_;

    my $pk_col;

    my $q = <<SQL;
    SELECT pga.attname AS pk_col
      FROM pg_index pgi
INNER JOIN pg_class pgc
        ON pgc.oid = pgi.indrelid
       AND pgc.oid = '${table_name}'::REGCLASS
INNER JOIN pg_attribute pga
        ON pga.attrelid = pgc.oid
       AND pga.attnum = ANY( pgi.indkey )
     WHERE pgi.indisprimary
SQL
    my $sth = try_query( $q );
    my $row = $sth->fetchrow_hashref();
       $pk_col = $row->{'pk_col'};

    return $pk_col;
}

sub get_table_columns($;$$)
{
    my( $table_name, $exclude_columns, $pid ) = @_;
    
    my $exclude = '';

    if(
            defined $exclude_columns
        and ref( $exclude_columns ) eq 'ARRAY'
        and scalar( @$exclude_columns ) > 0
      )
    {
        $exclude = "AND pga.attname NOT IN( '" . join( "','", @$exclude_columns ) . "' )";
    }
    
    my @return_columns;

    my $query = <<SQL;
    SELECT pga.attname
      FROM pg_attribute pga
INNER JOIN pg_class pgc
        ON pgc.oid = pga.attrelid
       AND pgc.relname = '$table_name'
     WHERE pga.attnum > 0
           $exclude
  ORDER BY pga.attnum
SQL

    update( $query ) if( $DEBUG );
    my $sth = try_query( $query, $pid );

    while( my $row = $sth->fetchrow_hashref() )
    {
        push( @return_columns, $row->{attname} );
    }

    return @return_columns;
}

sub child_thread($$)
{
    my( $cache_table_info, $table_keys ) = @_;
    $db_handles->{0} = undef; 
    $db_handles->{$$} = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef )
        or croak( 'Could not connect to database' );
    
    my $unique_expression = $cache_table_info->{'unique_expression'  };
    my $generation_query  = $cache_table_info->{'generation_query'   };
    my $cache_table       = $cache_table_info->{'cache_table'        };
    my $group_by          = $cache_table_info->{'group_by_expression'};
    my $table_columns     = $cache_table_info->{'table_columns'      };
       $table_columns     = decode_json( $table_columns );
    
    my @generation_where_entries;
    my @ct_where_entries;

    while( my ($table_name, $pk_hash) = each %$table_keys )
    {
        my $pk_column = $pk_hash->{pk_col};
        my $pk_values = $pk_hash->{pk_vals};
        my $key_infos = $pk_hash->{keys};

        while( my ($table_alias, $colname_mapping) = each %{$table_columns->{$table_name}} )
        {
            update( "$table_alias, " . Dumper( $colname_mapping ) ) if( $DEBUG );
            my $pk_col_alias = $colname_mapping->{$pk_column};
            push( @generation_where_entries, "$table_alias.$pk_column IN( " . join( ',', @$pk_values ) . ' ) '   );

            if( $pk_col_alias )
            {
                push( @ct_where_entries,  "vw.$pk_col_alias IN( " . join( ',', @$pk_values ) . ' ) ' );
            }
            else
            {
                # Use key_infos as a backup. This is generated as a fallback by the fn_queue_cache_update_request functions
                foreach my $key_info( @$key_infos )
                {
                    my $key_info_json = decode_json( $key_info );
                    my @ct_where_sub_entries;

                    while( my ($unique_column,$value ) = each %$key_info_json )
                    {
                        push( @ct_where_sub_entries, "vw.$unique_column = $value" );
                    }

                    push( @ct_where_entries, join( ' AND ', @ct_where_sub_entries ) );
                }
            }
        }
    }

    # Handle the case where:
    #  - we update raw tuple values
    #  - Update of tuple causes exclusion from set
    #      ( filter by unqiue expression and key updated )
    #  - update causes tuple to be included in set

    $generation_query .= ' AND ( ' . join( ' ) or ( ', @generation_where_entries ) . ' ) ' . $group_by;
    my $tt_query  = "CREATE TEMP TABLE tt_${cache_table}_compare AS ( $generation_query )";
    
    update( $tt_query ) if( $DEBUG );
    try_query( $tt_query, $$ );
    undef( $tt_query );
    undef( @generation_where_entries );

    try_query( "CREATE INDEX ix_${cache_table} ON tt_${cache_table}_compare( " . join( ',', @$unique_expression ) . ' )', $$ );
    
    my @columns = &get_table_columns( $cache_table, $unique_expression, $$ );

    my $join              = join( ' AND ',    map { "tt.$_ = vw.$_" } @$unique_expression );
    my $table_unique      = join( ', ',       map { "vw.$_"         } @$unique_expression );
    my $left_join_where_t = join( ' AND ',    map { "tt.$_ IS NULL" } @$unique_expression );
    my $left_join_where_v = join( ' AND ',    map { "vw.$_ IS NULL" } @$unique_expression );
    my $update            = join( ', ',       map {    "$_ = tt.$_" } @columns            );
    my $where             = join( ' ) OR ( ', @ct_where_entries );
     
    my $update_query = <<SQL;
        UPDATE ${cache_table} vw
           SET $update
          FROM tt_${cache_table}_compare tt
         WHERE $join
SQL

    update( $update_query ) if( $DEBUG );
    try_query( $update_query, $$ );

    my $remove_query = <<SQL;
        WITH tt_rows_removed AS
        (
            SELECT $table_unique
              FROM ${cache_table} vw
         LEFT JOIN tt_${cache_table}_compare tt
                ON $join
             WHERE ( $where )
               AND $left_join_where_t
        )
        DELETE FROM ${cache_table} vw
         USING tt_rows_removed tt
         WHERE $join
SQL

    update( $remove_query ) if( $DEBUG );
    try_query( $remove_query, $$ );

    my $insert_query = <<SQL;
    WITH tt_rows_insert AS
    (
        SELECT tt.*
          FROM tt_${cache_table}_compare tt
     LEFT JOIN ${cache_table} vw
            ON $join
         WHERE $left_join_where_v
    )
    INSERT INTO ${cache_table}
         SELECT *
           FROM tt_rows_insert
SQL

    update( $insert_query ) if( $DEBUG );
    try_query( $insert_query, $$ );
    try_query( "DROP TABLE IF EXISTS tt_${cache_table}_compare", $$ );
    
    $db_handles->{$$}->disconnect();

    exit 0;
}

sub flush_cache_requests_by_table_keys($)
{
    my( $table_keys ) = @_;
    my $flush_entry_q = <<SQL;
    DELETE FROM tblmgr.tb_cache_update_request
          WHERE
SQL

    my @table_entries;
    foreach my $table_name( keys %$table_keys )
    {
        my $primary_keys      = $table_keys->{$table_name}->{pk_vals};
        my $created           = $table_keys->{$table_name}->{created};
        my $table_flush_entry = " ( table_name = '$table_name' ";

        if( scalar( @$primary_keys ) > 0 )
        {
            $table_flush_entry .= 'AND primary_key IN( ' . join( ',', @$primary_keys ) . ' )';
        }

        $table_flush_entry .= " AND created <= '$created'::TIMESTAMP ";
        $table_flush_entry .= ' )';

        push( @table_entries, $table_flush_entry );
    }
   
    $flush_entry_q .= join( ' OR ', @table_entries );
#    update( $flush_entry_q ) if( $DEBUG ); 
#    try_query( $flush_entry_q );
    return;
}

sub process_update_requests()
{
    # Get a list of cache tables and all the updates that correspond to them.
    #  The first two CTEs union the arrays of key_info's keys and fn_get_table_pk_col( table_name )
    #  from tb_cache_update_request
    #
    #  The last query joins that with the trigger_columns on column and table to determine what
    #  cache_tables are impacted by this update.
    #
    #  the created field is maxed and carried through so that we can accurately clear requests
    #  that have been serviced. This query should execute in ~700ms
    my $requests_q = <<SQL;
    WITH tt_latest_requests AS
    (
          SELECT DISTINCT ON( tt.table_name, tt.primary_key )
                 tt.table_name,
                 tt.primary_key,
                 ( array_agg( tt.key_info ORDER BY tt.created DESC ) )[1] AS key_info,
                 MAX( tt.created ) AS created,
                 array_agg( DISTINCT jok.key )::VARCHAR[] AS keys
            FROM tblmgr.tb_cache_update_request tt
    JOIN LATERAL (
                     SELECT key,
                            tt.table_name,
                            tt.primary_key
                       FROM json_object_keys( tt.key_info ) key
                 ) jok
              ON jok.table_name = tt.table_name
             AND jok.primary_key = tt.primary_key
        GROUP BY tt.table_name,
                 tt.primary_key
    ),
    tt_distinct_column_names AS
    (
          SELECT tt.table_name,
                 tt.primary_key,
                 tt.key_info,
                 unnest( CASE WHEN ARRAY[ a.attname::VARCHAR ]::VARCHAR[] <@ tt.keys
                      THEN tt.keys
                      ELSE array_append( tt.keys, a.attname::VARCHAR )
                       END
                 ) AS column_name,
                 tt.created
            FROM tt_latest_requests tt
      INNER JOIN pg_class c
              ON c.relname::VARCHAR = tt.table_name
             AND c.relkind = 'r'
      INNER JOIN pg_attribute a
              ON a.attrelid = c.oid
             AND a.attnum > 0
      INNER JOIN pg_constraint cn
              ON cn.contype = 'p'
             AND cn.conrelid = c.oid
             AND cn.conkey[1] = a.attnum
    ),
    tt_cache_table_updates AS
    (
        SELECT ct.table_name AS cache_table,
               ct.unique_expression,
               ct.generation_query,
               ct.table_columns,
               COALESCE( ' GROUP BY ' || array_to_string( ct.group_by_expression, ', ' ), '' ) AS group_by_expression,
               tt.table_name,
               array_agg( tt.primary_key ORDER BY tt.primary_key ) AS primary_keys,
               array_agg( tt.key_info    ORDER BY tt.primary_key ) AS key_infos,
               MAX( tt.created ) AS created
          FROM tblmgr.tb_cache_table ct
    INNER JOIN tblmgr.tb_cache_table_trigger_column cttc
            ON cttc.cache_table = ct.cache_table
    INNER JOIN tblmgr.tb_trigger_column tc
            ON tc.trigger_column = cttc.trigger_column
    INNER JOIN tt_distinct_column_names tt
            ON tt.table_name = tc.table_name
           AND tt.column_name = tc.column_name
      GROUP BY ct.cache_table,
               tt.table_name
    )
        SELECT cache_table,
               unique_expression,
               generation_query,
               group_by_expression,
               table_columns,
               ( '{"' || table_name || '":{'
               || '"pk_col":"'   || tblmgr.fn_get_table_pk_col( table_name ) || '",'
               || '"pk_vals":['  || array_to_string( primary_keys, ','  ) || '],'
               || '"keys":['     || array_to_string( key_infos,    ','  ) || '],'
               || '"created":"' || created || '"}}' )::JSON AS table_keys
          FROM tt_cache_table_updates
SQL

    my $requests_sth = try_query( $requests_q );
    my $requests     = $requests_sth->fetchall_hashref( 'cache_table' );
    my $cache_tables = scalar( keys %$requests );
    
    # Thread here
    foreach my $cache_table( keys %$requests )
    {
        my $table_keys    = decode_json( $requests->{$cache_table}->{table_keys} );

        unless( $table_keys )
        {
            update( "TBLMGR ERROR: Failed to decode json for cache table $cache_table" );
            next;
        }

        my $cache_table_info = {
            cache_table         => $cache_table,
            group_by_expression => $requests->{$cache_table}->{group_by_expression},
            generation_query    => $requests->{$cache_table}->{generation_query},
            unique_expression   => $requests->{$cache_table}->{unique_expression},
            table_columns       => $requests->{$cache_table}->{table_columns},
        };
        
        # Process entries here
        if( defined $service_threads->{$cache_table} and $service_threads->{$cache_table}->{pid} > 0 )
        {
            if( waitpid( $service_threads->{$cache_table}->{pid}, WNOHANG ) )
            {
                delete( $service_threads->{$cache_table} );
            }
            else
            {
                update( "TBLMGR WARNING: PID for cache_table $cache_table ( " . $service_threads->{$cache_table}->{pid} . ' ) hasnt completed since last NOTIFY' );
            }
        }
        else
        {
            my $pid = fork();

            if( defined $pid and $pid == 0 )
            {
                &child_thread( $cache_table_info, $table_keys );
            }
            elsif( defined $pid and $pid > 0 )
            {
                $service_threads->{$cache_table}->{pid}        = $pid;
                $service_threads->{$cache_table}->{table_keys} = $table_keys;
            }
            else
            {
                update( "TBLMGR ERROR: Failed to fork() for cache_table $cache_table!" );
            }
        }

        # Assume the child thread will finish its work
        &flush_cache_requests_by_table_keys( $table_keys );
    }
    
    return;
}

sub usage($)
{
    my( $msg ) = @_;
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0\n";
    print " -d dbname\n";
    print " -U username\n";
    print " -h host\n";
    print " [ -p port ]\n";
    print " [ -P pid_file ]\n";
    print " [ -D do not daemonize ]\n";
    exit 1;
}

sub main()
{
    our( $opt_D, $opt_d, $opt_U, $opt_h, $opt_p, $opt_P );

    usage( 'Invalid arguments' ) unless( getopts( 'd:U:h:p:P:D' ) );

    my $dbname       = $opt_d;
    my $host         = $opt_h;
    my $user         = $opt_U;
    my $port         = $opt_p;
    my $no_daemonize = $opt_D;
    my $pid_file     = $opt_P;

    $port = 5432 unless( defined $port );
    usage( 'Invalid port' ) if( defined $port and ($port !~ /^\d+$/ or $port < 1 or $port > 65536) );
    usage( 'Invalid dbname'   ) unless( defined $dbname and length( $dbname ) > 0 );
    usage( 'Invalid username' ) unless( defined $user   and length( $user   ) > 0 );
    usage( 'Invalid hostname' ) unless( defined $host   and length( $host   ) > 0 );

    unless( defined $pid_file and length( $pid_file ) > 0 )
    {
        $pid_file = "/var/run/tblmgr.$dbname.pid";
    }

    daemonize() unless( $no_daemonize );
    setup_status_file();

    my $pid = $$;

    manage_pid_file( $pid, $pid_file );

    my $conn_string = "dbi:Pg:dbname=${dbname};host=${host};port=${port}";
    $conn_map->{conn_string} = $conn_string;
    $conn_map->{user}        = $user;

    $db_handles->{0} = DBI->connect( $conn_string, $user, undef ) or croak( 'Could not connect to database' );

    process_update_requests();
    
    $db_handles->{0}->do( "LISTEN $LISTEN_CHANNEL" );
    $file_descriptor = $db_handles->{0}->func( 'getfd' );
    $sel             = IO::Select->new( $file_descriptor );
    
    while(1)
    {
        my $local_handle = $db_handles->{0};
        update( 'Pre WAIT for NOTIFY' ) if( $DEBUG );
        $sel->can_read; # Blocks and waits
        my $notify = $local_handle->func( 'pg_notifies' );
        update( 'Checking notify' ) if( $DEBUG );

        if( $notify )
        {
            my( $relname, $pid ) = @$notify;

            if( $relname eq $LISTEN_CHANNEL )
            {
                process_update_requests();
            }
        }
        else
        {
            try_query( 'SELECT 1' );
        }

        sleep( 0.25 );
    }

    return;
}

main();
exit 0;
