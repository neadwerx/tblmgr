SELECT pg_catalog.pg_extension_config_dump( 'tb_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( 'tb_managed_table_trigger', '' );
SELECT pg_catalog.pg_extension_config_dump( 'tb_managed_table', '' );

CREATE OR REPLACE FUNCTION @extschema@.fn_queue_cache_update_request_from_rollup_table()
RETURNS TRIGGER AS
 $function$
DECLARE
    my_record               RECORD;
    my_table_name           VARCHAR;
    my_fk_colname           VARCHAR;
    my_target_table_name    VARCHAR;
    my_pk_value             INTEGER;
BEGIN
    --Masquerades a modification to the table the trigger is
    -- present on as an update to the table that the table has an FK to.

    my_table_name := TG_RELNAME::VARCHAR;

    IF( TG_OP = 'INSERT' ) THEN
        my_record := NEW;
    ELSIF( TG_OP = 'UPDATE' ) THEN
        IF( NEW::VARCHAR IS DISTINCT FROM OLD::VARCHAR ) THEN
            my_record := NEW;
        ELSE
            RETURN NEW;
        END IF;
    ELSIF( TG_OP = 'DELETE' ) THEN
        my_record := OLD;
    END IF;

    SELECT masquerades_for_table
      INTO my_target_table_name
      FROM @extschema@.tb_table_trigger
     WHERE table_name = my_table_name
       AND masquerades_for_table IS NOT NULL;

    IF( my_target_table_name IS NULL ) THEN
        RAISE NOTICE 'TBLMGR: Orphaned table trigger located on %, cannot determine the table for which it is masquerading.', TG_RELNAME;
        RETURN my_record;
    END IF;

    SELECT ca.attname
      INTO my_fk_colname
      FROM pg_constraint pco
INNER JOIN pg_class pcl
        ON pcl.oid = pco.confrelid
       AND pcl.relname = my_target_table_name
INNER JOIN pg_class ccl
        ON ccl.oid = pco.conrelid
       AND ccl.relname IS DISTINCT FROM my_target_table_name
       AND ccl.relname = my_table_name
INNER JOIN pg_attribute ca
        ON ca.attrelid = pco.conrelid
       AND ca.attnum = ANY( pco.conkey )
     WHERE pco.contype = 'f'; -- FK

    EXECUTE 'SELECT $1.' || my_fk_colname
       INTO my_pk_value
      USING my_record;

    INSERT INTO @extschema@.tb_cache_update_request
                (
                    table_name,
                    primary_key,
                    op
                )
        VALUES
                (
                    my_target_table_name,
                    my_pk_value,
                    'U'
                );

    RETURN my_record;
END
 $function$
    LANGUAGE 'plpgsql';
