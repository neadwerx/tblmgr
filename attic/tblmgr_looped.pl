#!/usr/bin/perl

$| = 1;

use utf8;
use strict;
use warnings;

use DBI;
use JSON;
use IO::Select;
use IO::Handle;
use Sys::Syslog;
use Getopt::Std;
use Time::HiRes qw( gettimeofday tv_interval );
use POSIX qw( strftime setsid :sys_wait_h );
use Data::Dumper;
use Readonly;
use Carp;

Readonly::Scalar my $LISTEN_CHANNEL      => 'cache_update_request';
Readonly::Scalar my $DEBUG               => 0;

my( $conn_map, $handle, $sel, $file_descriptor );
my $_verbose = 0;
my $_timing  = 0;
my $_schema  = 'tblmgr';

sub update($)
{
    my( $msg ) = @_;
    syslog( 'info', "$msg" ) if( $msg );
    print "$msg\n" if( $msg and -t STDIN );
    return;
}

sub setup_status_file()
{
    openlog( 'tblmgr', '', 'user' );
    return;
}

sub daemonize()
{
    $SIG{__DIE__} = sub
    {
        update( "ERROR: " . $_[0] );
        exit 1;
    };

    $SIG{__WARN__} = sub
    {
        update( "WARNING: " . $_[0] );
    };

    chdir '/' or croak "Can't chdir to /: $!";
    open STDIN, '/dev/null'     or croak( "Can't read /dev/null: $!" );
    open STDOUT, '>/dev/null'   or croak( "Can't write to /dev/null: $!" );
    defined( my $pid = fork() ) or croak( "Can't fork: $!" );
    exit 0 if $pid;
    setsid()                    or croak( "Can't start a new session: $!" );
}

sub manage_pid_file($$)
{
    my( $pid, $pid_file ) = @_;

    if( -e $pid_file )
    {
        croak( "Lock file '$pid_file' exists, but is not a regular file!" ) unless( -f $pid_file );
        croak( "Cannot read lock file '$pid_file'" ) unless( -r $pid_file );

        open( PID, "<$pid_file" ) or croak( "Could not open existing PID file '$pid_file': $!" );
        chomp( my $line = <PID> );
        close( PID );

        my $existing_pid;

        ( $existing_pid ) = ( $line =~ /^(\d+)$/ );

        if(
                defined $existing_pid
            and ( $existing_pid =~ /^\d+$/ )
            and $existing_pid > 0
          )
        {
            my $process_found = 0;
            my $ps_command    = '/bin/ps -e |';

            unless( open( PS, $ps_command ) )
            {
                croak( "cannot check for existing processes: $!" );
            }

            while( <PS> )
            {
                my( $check_pid ) = ( $_ =~ /^\D+(\d+)\D.*$/ );

                next unless( defined $check_pid and $check_pid =~ /^\d+$/ and $check_pid > 0 );

                if( int( $existing_pid ) == int( $check_pid ) )
                {
                    $process_found = 1;
                    last;
                }
            }

            close PS;

            if( $process_found )
            {
                croak( "Service already running! PID ($existing_pid)" );
            }
            else
            {
                unlink( $pid_file ) or croak( "Could not remove stale PID file: $!" );
                open( PID, ">$pid_file" ) or croak( "Could not open PID file: $!" );
                print( PID $pid );
                close( PID );
            }
        }
    }
    else
    {
        open( PID, ">$pid_file" ) or croak( "Could not open new PID file '$pid_file': $!" );
        print( PID $pid );
        close( PID );
    }

    return;
}

sub try_query($)
{
    my( $query ) = @_;
    my $sth;

    until( ( $sth = $handle->prepare( $query ) ) and $sth->execute() )
    {
        return $sth if( $handle->ping() > 0 );

        until( $handle and $handle->ping() > 0 )
        {
            update( 'DB connection is down!' ) if( $_verbose or $DEBUG );
            sleep 0.5;
            $handle = DBI->connect( $conn_map->{conn_string}, $conn_map->{user}, undef );
        }

        update( 'Reconnected to database' ) if( $_verbose or $DEBUG );
        $handle->do( "LISTEN $LISTEN_CHANNEL" );
        $file_descriptor = $handle->func( 'getfd' );
        $sel             = IO::Select->new( $file_descriptor );
    }

    return $sth;
}

sub get_pk_column_name($)
{
    my( $table_name ) = @_;

    my $pk_col;

    my $q = <<SQL;
    SELECT pga.attname AS pk_col
      FROM pg_index pgi
INNER JOIN pg_class pgc
        ON pgc.oid = pgi.indrelid
       AND pgc.oid = '${table_name}'::REGCLASS
INNER JOIN pg_attribute pga
        ON pga.attrelid = pgc.oid
       AND pga.attnum = ANY( pgi.indkey )
     WHERE pgi.indisprimary
SQL
    my $sth = try_query( $q );
    my $row = $sth->fetchrow_hashref();
       $pk_col = $row->{'pk_col'};

    return $pk_col;
}

sub get_table_columns($;$)
{
    my( $table_name, $exclude_columns ) = @_;

    my $exclude = '';

    if(
            defined $exclude_columns
        and ref( $exclude_columns ) eq 'ARRAY'
        and scalar( @$exclude_columns ) > 0
      )
    {
        $exclude = "AND pga.attname NOT IN( '" . join( "','", @$exclude_columns ) . "' )";
    }

    my @return_columns;

    my $query = <<SQL;
    SELECT pga.attname
      FROM pg_attribute pga
INNER JOIN pg_class pgc
        ON pgc.oid = pga.attrelid
       AND pgc.relname = '$table_name'
     WHERE pga.attnum > 0
           $exclude
  ORDER BY pga.attnum
SQL

    update( $query ) if( $DEBUG );
    my $sth = try_query( $query );

    while( my $row = $sth->fetchrow_hashref() )
    {
        push( @return_columns, $row->{attname} );
    }

    return @return_columns;
}

sub process_ct_updates($$)
{
    my( $cache_table_info, $table_keys ) = @_;

    my $update_start      = [gettimeofday];
    my $pk_cache_table    = $cache_table_info->{'pk_cache_table'     };
    my $unique_expression = $cache_table_info->{'unique_expression'  };
    my $generation_query  = $cache_table_info->{'generation_query'   };
    my $cache_table       = $cache_table_info->{'cache_table'        };
    my $schema            = $cache_table_info->{'schema'             };
    my $group_by          = $cache_table_info->{'group_by_expression'};
    my $table_columns     = $cache_table_info->{'table_columns'      };
       $table_columns     = decode_json( $table_columns );
    
    update( "Processing updates for $cache_table" ) if( $_verbose );
    update( "Cache table $cache_table service timings:" ) if( $_timing );

    my @generation_where_entries;
    my @ct_where_entries;

    my $additional_key_count = 0;
    my $primary_key_count    = 0;
    my $table_count          = 0;

    while( my ($table_name, $pk_hash) = each %$table_keys )
    {
        my $pk_column = $pk_hash->{pk_col};
        my $pk_values = $pk_hash->{pk_vals};
        my $key_infos = $pk_hash->{keys};
        $table_count++;

        foreach my $schema_name( keys %$table_columns )
        {
            my $tables = $table_columns->{$schema_name};
            while( my ($table_alias, $colname_mapping) = each %{$tables->{$table_name}} )
            {
                my $pk_col_alias = $colname_mapping->{$pk_column};
                push( @generation_where_entries, "$table_alias.$pk_column IN( " . join( ',', @$pk_values ) . ' ) '   );

                if( $pk_col_alias )
                {
                    push( @ct_where_entries,  "vw.$pk_col_alias IN( " . join( ',', @$pk_values ) . ' ) ' );
                    $primary_key_count += scalar( @$pk_values );
                }
                else
                {
                    # Use key_infos as a backup. This is generated as a fallback by the fn_queue_cache_update_request functions
                    foreach my $key_info( @$key_infos )
                    {
                        my @ct_where_sub_entries;

                        while( my ($unique_column,$value ) = each %$key_info )
                        {
                            push( @ct_where_sub_entries, "vw.$unique_column = $value" );
                        }

                        push( @ct_where_entries, join( ' AND ', @ct_where_sub_entries ) );
                        $additional_key_count ++;
                    }
                }
            }
        }
    }

    my @columns = &get_table_columns( $cache_table, $unique_expression );
    
    my $json_processed      = [gettimeofday];
    my $json_parse_overhead = tv_interval( $update_start, $json_processed );
    update( " - JSON Parse: $json_parse_overhead" ) if( $_timing );

    my $join                = join( ' AND ',    map { "tt.$_ = vw.$_" } @$unique_expression );
    my $table_unique        = join( ', ',       map { "vw.$_"         } @$unique_expression );
    my $bindable_unique     = join( ' AND ',    map { "vw.$_ = ?"     } @$unique_expression );
    my $left_join_where_t   = join( ' AND ',    map { "tt.$_ IS NULL" } @$unique_expression );
    my $left_join_where_v   = join( ' AND ',    map { "vw.$_ IS NULL" } @$unique_expression );
    my $update              = join( ', ',       map {    "$_ = tt.$_" } @columns            );
    my $bindable_update     = join( ', ',       map {    "$_ = ?"     } @columns            );
    my $where               = join( ' ) OR ( ', @ct_where_entries );
    # Handle the case where:
    #  - we update raw tuple values
    #  - Update of tuple causes exclusion from set
    #      ( filter by unqiue expression and key updated )
    #  - update causes tuple to be included in set

    $generation_query .= ' AND ( ' . join( ' ) or ( ', @generation_where_entries ) . ' ) ' . $group_by;
    my $tt_query  = "CREATE UNLOGGED TABLE tt_${cache_table}_compare AS ( $generation_query )";

    update( $tt_query ) if( $DEBUG );

    try_query( $tt_query );
    try_query( "CREATE INDEX ix_${cache_table}_compare ON tt_${cache_table}_compare( " . join( ',', @$unique_expression ) . ' )' );

    my $temp_table_finished = [gettimeofday];
    my $temp_table_overhead = tv_interval( $json_processed, $temp_table_finished );
    
    update( " - Temp table: $temp_table_overhead" ) if( $_timing );
    update( "  UPDATE: Temp table created for $cache_table" ) if( $_verbose );

    my $update_query = <<SQL;
        UPDATE ${schema}.${cache_table} vw
           SET $update
          FROM tt_${cache_table}_compare tt
         WHERE $join
SQL

    update( $update_query ) if( $DEBUG );
    try_query( $update_query );

    my $update_finished = [gettimeofday];
    my $update_duration = tv_interval( $temp_table_finished, $update_finished );
    
    update( " - UPDATE:     $update_duration" ) if( $_timing );
    update( "  UPDATE: Existing rows updated for $cache_table" ) if( $_verbose );

    my $remove_query    = <<SQL;
    WITH tt_rows_removed AS
    (
        SELECT $table_unique
          FROM ${cache_table} vw
     LEFT JOIN tt_${cache_table}_compare tt
            ON $join
         WHERE ( $where )
           AND $left_join_where_t
    )
        SELECT $table_unique
          FROM tt_rows_removed vw
SQL

    update( $remove_query ) if( $DEBUG );
    my $remove_sth = try_query( $remove_query );
    my $delete_q   = <<SQL;
    DELETE FROM ${schema}.${cache_table} vw
          WHERE $bindable_unique
SQL
    my $delete_sth = $handle->prepare( $delete_q );
    while( my $remove_row = $remove_sth->fetchrow_hashref() )
    {
        my $bind_index = 1;
        foreach my $uniq( @$unique_expression )
        {
            my $value = $remove_row->{$uniq};
            $delete_sth->bind_param( $bind_index, $value );
            $bind_index++;
        }
        $delete_sth->execute() or update( "Failed to remove row" );
    }

    my $delete_finished = [gettimeofday];
    my $delete_duration = tv_interval( $update_finished, $delete_finished );
    
    update( " - DELETE:     $delete_duration" ) if( $_timing );
    update( "  UPDATE: Old rows removed for $cache_table" ) if( $_verbose );

    my $insert_query = <<SQL;
    WITH tt_rows_insert AS
    (
        SELECT tt.*
          FROM tt_${cache_table}_compare tt
     LEFT JOIN ${cache_table} vw
            ON $join
         WHERE $left_join_where_v
    )
        SELECT *
          FROM tt_rows_insert
SQL

    update( $insert_query ) if( $DEBUG );
    my $create_sth  = try_query( $insert_query );
    my $insert_cols = join( ', ', @columns );
    my $insert_bind = '?, ' x scalar @columns;
       $insert_bind =~ s/,\s$//;
    my $insert_q    = <<SQL;
    INSERT INTO ${schema}.${cache_table}
                (
                    $insert_cols
                )
         VALUES
                (
                    $insert_bind
                )
SQL
    my $insert_sth = $handle->prepare( $insert_q );

    while( my $create_row = $create_sth->fetchrow_hashref() )
    {
        my $bind_index = 1;

        foreach my $column( @columns )
        {
            my $value = $create_row->{$column};
            $insert_sth->bind_param( $bind_index, $value );
            $bind_index++;
        }

        $insert_sth->execute() or update( "Failed to create row" );
    }

    my $insert_finished = [gettimeofday];
    my $insert_duration = tv_interval( $delete_finished, $insert_finished );

    update( " - INSERT:     $insert_duration"     ) if( $_timing );
    update( "  UPDATE: New rows added for $cache_table" ) if( $_verbose );

    try_query( "DROP TABLE IF EXISTS tt_${cache_table}_compare" );

    # Create detailed stats entry
    my $stats_q = <<SQL;
    INSERT INTO $_schema.tb_cache_table_statistic
                (
                    cache_table,
                    additional_stats
                )
         VALUES
                (
                    $pk_cache_table,
                    '{"json_parse":$json_parse_overhead,"temp_table":$temp_table_overhead,"update":$update_duration,"delete":$delete_duration,"insert":$insert_duration,"additional_key_count":$additional_key_count,"primary_key_count":$primary_key_count,"table_count":$table_count}'::JSON
                );
SQL

    try_query( $stats_q );
    try_query( "DROP TABLE IF EXISTS tt_${cache_table}_compare" );
    return;
}

sub flush_cache_requests_by_table_keys($)
{
    my( $table_keys ) = @_;
    my $table_flush_start = [gettimeofday];
    # Check for advisory locks held by refreshes
    my $lock_check_q = <<SQL;
    SELECT pid
      FROM pg_locks
     WHERE locktype = 'advisory'
       AND mode = 'ExclusiveLock'
       AND objid = 'tb_cache_update_request'::REGCLASS::OID
SQL

    my $sth = try_query( $lock_check_q );
    my $flush_entry_q = <<SQL;
    DELETE FROM $_schema.tb_cache_update_request
SQL

    if( $sth->rows() > 0 )
    {
        $flush_entry_q .= <<SQL;
         WHERE disable_flush IS FALSE
           AND
SQL
        update( 'fn_refresh_cache_table is running, some cache_update_requests will not be flushed' ) if( $_verbose );
    }
    else
    {
        $flush_entry_q .= <<SQL;
         WHERE
SQL
    }

    my @table_entries;
    my $key_count = 0;

    foreach my $table_name( keys %$table_keys )
    {
        my $primary_keys      = $table_keys->{$table_name}->{pk_vals};
        my $created           = $table_keys->{$table_name}->{created};
        my $table_flush_entry = " ( table_name = '$table_name' ";

        if( scalar( @$primary_keys ) > 0 )
        {
            $table_flush_entry .= 'AND primary_key IN( ' . join( ',', @$primary_keys ) . ' )';
            $key_count += scalar( @$primary_keys );
        }

        $table_flush_entry .= " AND created <= '$created'::TIMESTAMP ";
        $table_flush_entry .= ' )';

        push( @table_entries, $table_flush_entry );
    }

    $flush_entry_q .= join( ' OR ', @table_entries );
    update( $flush_entry_q ) if( $DEBUG );
    try_query( $flush_entry_q );
    my $table_flush_duration = tv_interval( $table_flush_start, [gettimeofday] );
    update( "$key_count keys flushed in $table_flush_duration seconds" ) if( $_timing or $_verbose );

    return;
}

sub process_update_requests()
{
    # Get a list of cache tables and all the updates that correspond to them.
    #  The first two CTEs union the arrays of key_info's keys and fn_get_table_pk_col( table_name )
    #  from tb_cache_update_request
    #
    #  The last query joins that with the trigger_columns on column and table to determine what
    #  cache_tables are impacted by this update.
    #
    #  the created field is maxed and carried through so that we can accurately clear requests
    #  that have been serviced. This query should execute in ~700ms
    my $queue_peek_start = [gettimeofday];
    my $requests_q = <<SQL;
    WITH tt_latest_requests AS
    (
           SELECT DISTINCT ON( tt.table_name, tt.primary_key )
                  tt.table_name,
                  tt.primary_key,
                  ( array_agg( tt.key_info ORDER BY tt.created DESC ) )[1] AS key_info,
                  MAX( tt.created ) AS created,
                  MIN( tt.created ) AS oldest_created,
                  array_agg( DISTINCT jok.key )::VARCHAR[] AS keys,
                  COUNT( DISTINCT cache_update_request ) AS keynum
             FROM $_schema.tb_cache_update_request tt
LEFT JOIN LATERAL (
                      SELECT key,
                             tt.table_name,
                             tt.primary_key
                        FROM json_object_keys( tt.key_info ) key
                  ) jok
               ON jok.table_name = tt.table_name
              AND jok.primary_key = tt.primary_key
         GROUP BY tt.table_name,
                  tt.primary_key
    ),
    tt_distinct_column_names AS
    (
           SELECT tt.table_name,
                  tt.primary_key,
                  tt.key_info,
                  unnest( CASE WHEN ARRAY[ a.attname::VARCHAR ]::VARCHAR[] <@ tt.keys
                       THEN tt.keys
                       ELSE array_append( tt.keys, a.attname::VARCHAR )
                        END
                  ) AS column_name,
                  tt.keynum,
                  tt.created,
                  tt.oldest_created
             FROM tt_latest_requests tt
       INNER JOIN pg_class c
               ON c.relname::VARCHAR = tt.table_name
              AND c.relkind = 'r'
       INNER JOIN pg_attribute a
               ON a.attrelid = c.oid
              AND a.attnum > 0
       INNER JOIN pg_constraint cn
               ON cn.contype = 'p'
              AND cn.conrelid = c.oid
              AND cn.conkey[1] = a.attnum
     ),
     tt_cache_table_updates AS
     (
         SELECT ct.table_name AS cache_table,
                ct.cache_table AS pk_cache_table,
                ct.unique_expression,
                ct.generation_query,
                ct.table_columns,
                ct.schema,
                COALESCE( ' GROUP BY ' || array_to_string( ct.group_by_expression, ', ' ), '' ) AS group_by_expression,
                tt.table_name,
                SUM( tt.keynum ) AS keynum,
                array_agg( tt.primary_key ORDER BY tt.primary_key ) AS primary_keys,
                array_agg( tt.key_info    ORDER BY tt.primary_key ) AS key_infos,
                MAX( tt.created ) AS created,
                MIN( tt.oldest_created ) AS oldest_created
           FROM $_schema.tb_cache_table ct
     INNER JOIN $_schema.tb_cache_table_trigger_column cttc
             ON cttc.cache_table = ct.cache_table
     INNER JOIN $_schema.tb_trigger_column tc
             ON tc.trigger_column = cttc.trigger_column
     INNER JOIN tt_distinct_column_names tt
             ON tt.table_name = tc.table_name
            AND tt.column_name = tc.column_name
       GROUP BY ct.cache_table,
                tt.table_name
    )
         SELECT cache_table,
                pk_cache_table,
                schema,
                unique_expression,
                generation_query,
                group_by_expression,
                table_columns,
                keynum,
                oldest_created,
                ( '{"' || table_name || '":{'
                || '"pk_col":"'   || $_schema.fn_get_table_pk_col( table_name ) || '",'
                || '"pk_vals":['  || array_to_string( primary_keys, ','  ) || '],'
                || '"keys":['     || array_to_string( key_infos,    ','  ) || '],'
                || '"created":"' || created || '"}}' )::JSON AS table_keys
           FROM tt_cache_table_updates
SQL

    my $requests_sth     = try_query( $requests_q );
    my $requests         = $requests_sth->fetchall_hashref( 'cache_table' );
    my $cache_tables     = scalar( keys %$requests );
    my $queue_peek_time  = tv_interval( $queue_peek_start, [gettimeofday] );
    update( "queue peek time: $queue_peek_time" ) if( $_timing );

    # Thread here
    foreach my $cache_table( keys %$requests )
    {
        my $ct_service_start = [gettimeofday];
        my $table_keys       = decode_json( $requests->{$cache_table}->{table_keys} );

        unless( $table_keys )
        {
            update( "TBLMGR ERROR: Failed to decode json for cache table $cache_table" );
            next;
        }

        my $table_count = scalar keys %$table_keys;
        update( "Processing updates to $cache_table from $table_count base tables." ) if( $_verbose );

        my $cache_table_info = {
            cache_table         => $cache_table,
            pk_cache_table      => $requests->{$cache_table}->{pk_cache_table},
            group_by_expression => $requests->{$cache_table}->{group_by_expression},
            generation_query    => $requests->{$cache_table}->{generation_query},
            unique_expression   => $requests->{$cache_table}->{unique_expression},
            table_columns       => $requests->{$cache_table}->{table_columns},
            schema              => $requests->{$cache_table}->{schema},
        };

        &process_ct_updates( $cache_table_info, $table_keys );

        update( 'Flushing serviced cache update requests' ) if( $_verbose );
        # Assume the child thread will finish its work
        &flush_cache_requests_by_table_keys( $table_keys );

        # Store stats
        my $key_count       = $requests->{$cache_table}->{keynum};
        my $oldest_created  = $requests->{$cache_table}->{oldest_created};
        my $duration        = tv_interval( $ct_service_start, [gettimeofday] );
        my @table_stats;

        update( "$cache_table service time: $duration" ) if( $_timing );

        foreach my $table_name( keys %$table_keys )
        {
            push( @table_stats, "\"$table_name\":" . scalar( @{$table_keys->{$table_name}->{pk_vals}} ) );
        }

        my $tables_array = join( ',', @table_stats );
        my $stat_query = <<SQL;
        INSERT INTO $_schema.tb_cache_table_statistic
                    (
                        cache_table,
                        partial_refresh_keys,
                        partial_refresh_duration,
                        additional_stats
                    )
             SELECT cache_table,
                    '$key_count'::INTEGER AS partial_refresh_keys,
                    ( $duration * '1 second'::INTERVAL ) AS partial_refresh_duration,
                    '{"tables":{$tables_array},"oldest_request":"$oldest_created","peek_time":$queue_peek_time}'::JSON AS additional_stats
               FROM $_schema.tb_cache_table
              WHERE table_name = '$cache_table'
SQL
        try_query( $stat_query );
        update( "Service statistics and timing updated for $cache_table" ) if( $_verbose );
    }

    return;
}

sub usage($)
{
    my( $msg ) = @_;
    print "$msg\n" if( $msg );
    print "Usage:\n";
    print " $0\n";
    print " -d dbname\n";
    print " -U username\n";
    print " -h host\n";
    print " [ -p port ]\n";
    print " [ -P pid_file ]\n";
    print " [ -s tblmgr_schema (default: tblmgr)\n";
    print " [ -D ( do not daemonize ) ]\n";
    print " [ -V ( verbose ) ]\n";
    print " [ -T ( show timings ) ]\n";
    exit 1;
}

sub main()
{
    our( $opt_D, $opt_d, $opt_U, $opt_h, $opt_p, $opt_P, $opt_V, $opt_T, $opt_s );

    usage( 'Invalid arguments' ) unless( getopts( 'd:U:h:p:P:s:DVT' ) );

    my $dbname       = $opt_d;
    my $host         = $opt_h;
    my $user         = $opt_U;
    my $port         = $opt_p;
    my $no_daemonize = $opt_D;
    my $pid_file     = $opt_P;
       $_schema      = $opt_s if( $opt_s );
       $_verbose     = $opt_V if( $opt_V );
       $_timing      = $opt_T if( $opt_T );

    $port = 5432 unless( defined $port );
    usage( 'Invalid port' ) if( defined $port and ($port !~ /^\d+$/ or $port < 1 or $port > 65536) );
    usage( 'Invalid dbname'   ) unless( defined $dbname and length( $dbname ) > 0 );
    usage( 'Invalid username' ) unless( defined $user   and length( $user   ) > 0 );
    usage( 'Invalid hostname' ) unless( defined $host   and length( $host   ) > 0 );

    unless( defined $pid_file and length( $pid_file ) > 0 )
    {
        $pid_file = "/var/run/tblmgr.$dbname.pid";
    }

    daemonize() unless( $no_daemonize );
    setup_status_file();

    my $pid = $$;

    manage_pid_file( $pid, $pid_file );

    my $conn_string = "dbi:Pg:dbname=${dbname};host=${host};port=${port}";
    $conn_map->{conn_string} = $conn_string;
    $conn_map->{user}        = $user;

    $handle = DBI->connect( $conn_string, $user, undef ) or croak( 'Could not connect to database' );

    update( 'STARTUP: Checking for cache update requests' ) if( $_verbose );
    process_update_requests();

    update( "STARTUP: Listening on channel '$LISTEN_CHANNEL'" ) if( $_verbose );
    $handle->do( "LISTEN $LISTEN_CHANNEL" );
    $file_descriptor = $handle->func( 'getfd' );
    $sel             = IO::Select->new( $file_descriptor );

    my( $update_process_start, $update_finished, $update_duration );
    my( $time_since_last_notify, $last_notify_time );

    while(1)
    {
        update( 'Pre WAIT for NOTIFY' ) if( $DEBUG );

        $last_notify_time = [gettimeofday];

        update( "Waiting for NOTIFY on '$LISTEN_CHANNEL'" ) if( $_verbose );
        
        $sel->can_read; # Blocks and waits
        my $notify = $handle->func( 'pg_notifies' );
        update( 'Checking notify' ) if( $DEBUG );
        update( 'Processing notify...' ) if( $_verbose );
        $time_since_last_notify = tv_interval( $last_notify_time, [gettimeofday] );
        update( "Time since last notify: $time_since_last_notify" ) if( $_timing );

        if( $notify )
        {
            my( $relname, $pid ) = @$notify;

            if( $relname eq $LISTEN_CHANNEL )
            {
                $update_process_start = [gettimeofday];
                process_update_requests();

                if( $_timing or $_verbose )
                {
                    $update_finished      = [gettimeofday];
                    $update_duration      = tv_interval( $update_process_start, $update_finished );
                    update( "Time to process last NOTIFY: $update_duration seconds" );
                }
            }
        }
        else
        {
            try_query( 'SELECT 1' );
        }

        sleep( 0.25 );
    }

    return;
}

main();
exit 0;
